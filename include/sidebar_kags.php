    <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <?PHP include 'logo.php';?>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="./">
                <i class="fas fa-fw fa-home"></i>
                <span>Home</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-users"></i>
                <span>Residents </span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Residents</h6>
                    <a class="collapse-item" href="../../pages/kagawad/residence_r.php">Resident list</a>
                    <a class="collapse-item" href="../../pages/kagawad/residence_a.php">Approval of residents</a>
                    <a class="collapse-item" href="../../pages/kagawad/residence_d.php">Declined residents</a>

                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/functionaries_c.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Functionaries</span></a>
        </li>

        <!-- Nav Item - Ecoboy -->
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/schedule.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Ecoboy Schedule</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/ecoboy_collected.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Ecoboy Collect</span></a>
        </li>
        <!--li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/rate.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Ecoboy Rate</span></a>
        </li-->
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/reports.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Accomplishment report</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/dump_kagawad.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Receipt</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/position.php">
                <i class="fas fa-fw fa-chair"></i>
                <span>Position</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/garbage_truck.php">
                <i class="fas fa-fw fa-truck"></i>
                <span>Garbage Truck</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->