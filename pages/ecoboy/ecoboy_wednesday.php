<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Garbage Collection Schedule</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item border-bottom-info">
                    <a class="nav-link active" href="ecoboy_wednesday.php">WEDNESDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ecoboy_saturday.php">SATURDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ecoboy_sunday.php">SUNDAY</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <form action="" method="GET">
                    <div class="form-group">
                        <label>Week</label>
                        <select class="form-control" name="week_number" required>
                            <option />
                            <option value="week1">
                                WEEK 1
                            </option>
                            <option value="week2">
                                WEEK 2
                            </option>
                            <option value="week3">
                                WEEK 3
                            </option>
                            <option value="week4">
                                WEEK 4
                            </option>
                        </select>
                    </div>
            </div>
            <div class="col-xl-3 col-lg-8">
                <div class="form-group" style="margin-top:12%;">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                </form>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Collection Date </th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>For pick up</th>
                                <th>Load to CENRO</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Collection Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>For pick up</th>
                                <th>Load to CENRO</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php
                                if(isset($_GET['week_number']))
                                {
                                    $weekno=$_GET['week_number'];
 $result = mysqli_query($db, "SELECT * FROM schedule where ecoboy_id=".$_SESSION['session_user']." and week_no='$weekno' and days='wednesday';");
             
                                }
                                 else
                                 {
            $result = mysqli_query($db, "SELECT * FROM schedule where ecoboy_id=".$_SESSION['session_user']." and days='wednesday';");
                                }
            while ($row = mysqli_fetch_array($result)) {
              ?>
                                <td><?php echo date("F j, Y  "."("."l".")",strtotime($row['CollDate'])); ?></td>
                                <td><?php echo $row['StartTime']; ?></td>
                                <td><?php echo $row['EndTime']; ?></td>
                                <?php
              if ($row['status'] == 'inactive') {
                ?>
                                <td style="color:blue;">Not yet reviewed</td>
                                <?php } else if ($row['status'] == 'denied') { ?>
                                <td style="color:red;">Denied </td>
                                <?php } else if ($row['status'] == 'Approve') { ?>
                                <td style="color:green;"> Approved</td>
                                <?php } ?>

                                <td>
                                    <a href="../../php_function/function_for_ecoboy.php?accept=<?php echo $row['Sched_No']; ?>&day=<?php echo $row['days'];?>&click-okay=okay"
                                        onclick="return confirm('Are you sure you want to accept this schedule?');"
                                        class="btn btn-success">
                                        <i class="fas fa-check"> </i>
                                    </a>
                                    <button class="btn btn-danger" data-toggle="modal"
                                        data-target="#openModal<?php echo $row['Sched_No']; ?>">
                                        <i class="fas fa-times"> </i>
                                    </button>

                                </td>
                                <td>
                                    <form action="responded_resident.php" method="POST">
                                        <input type="hidden" name="skidid" value="<?php echo $row['Sched_No']; ?>" />
                                        <button class="btn btn-primary" name="btn-res">
                                            <i class="icon-for-pick-up"><img
                                                    src='../../components/img/images/pickup.png'
                                                    style="width: 20px; height: 20px;" /> </i>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <button class="btn btn-success" data-toggle="modal"
                                        data-target="#opendriver<?php echo $row['Sched_No']; ?>">
                                        <i class="fas fa-dumpster"> </i>
                                    </button>
                                </td>
                            </tr>

                            <?php
        }
        ?>
                        </tbody>
                    </table>
                </div>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
  ?>
<div class="modal" id="openModal<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Do you want to decline this schedule please state the reason.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <textarea style="width: 100%; height: 100%;" placeholder="Enter here" name="reason"></textarea>
                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                        <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="decline-sked" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
  ?>
<div class="modal" id="opendriver<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Please put the weight/Net/kgs, plate number and the name of driver.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label>Weight / Net / Kgs.</label>
                        <input type="number" value="<?php echo $row['weight_net_kgs']; ?>" class=" form-control"
                            name="weyt" />
                    </div>
                    <div class="form-group">
                        <label>Plate Number</label>

                        <select class="form-control select2" name="pletno" required>
                            <option value="<?php echo $row['garbage_truck_id']; ?>"><?php echo $row['plate_number']; ?>
                            </option>
                            <?php      	 
                            $query = mysqli_query($db, "Select garbage_truck_id,Plate_number from garbagetruck;") or die(mysqli_error());
                            while($fetch = mysqli_fetch_array($query))
                            {   

                            echo "<option value=".$fetch['garbage_truck_id'].">".$fetch['Plate_number']."</option>";

                            } ?>
                        </select>

                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                        <input type="hidden" value="<?php echo $row['days']; ?>" name="days" />
                    </div>
                    <div class="form-group">
                        <label>Driver</label>
                        <input type="text" value="<?php echo $row['driver']; ?>" class=" form-control" name="driver" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-plet" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
  ?>
<div class="modal" id="opendriver<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Please put the plate number and the name of driver.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label>Plate Number</label>
                        <input type="text" value="<?php echo $row['plate_number']; ?>" class=" form-control"
                            name="pletno" required="required" />
                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                        <input type="hidden" value="<?php echo $row['days']; ?>" name="days" />
                    </div>
                    <div class="form-group">
                        <label>Driver</label>
                        <input type="text" value="<?php echo $row['driver']; ?>" class=" form-control" name="driver"
                            required="required" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-plet" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>