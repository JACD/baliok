<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-success">Add Driver</h6>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-8 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Contact #</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Contact #</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php
					$cap = mysqli_query($db,"SELECT * FROM brgydriver where deleted='0';");
                    while($row = mysqli_fetch_array($cap))
                    {   
                      ?>
                                <td>
                                    <?php echo $row['fname'].' '.$row['mname'].' '.$row['lname'];?>
                                </td>
                                <td>
                                    <?php echo $row['contact'];?>
                                </td>
                                <td>
                                    <button class="btn btn-success btn-circle" data-toggle="modal"
                                        data-target="#editModal<?php echo $row['driver_id'];?>">
                                        <i class="fas fa-edit"> </i>
                                    </button>
                                    <a href="../../php_function/adddriver.php?aydes=<?php echo $row['driver_id'];?>&click=remove"
                                        onclick="return confirm('Are you sure you want to remove this driver?');"
                                        class="btn btn-danger btn-circle">
                                        <i class="fas fa-sync"> </i>
                                    </a>

                                </td>
                            </tr>

                            <?php
									}
									?>
                        </tbody>
                    </table>

                </div>
            </div>

            <!-- Donut Chart -->
            <div class="col-xl-4 col-lg-3">
                <div class="">
                    <!-- Card Header - Dropdown -->
                    <div class="">
                        <h6 class="m-0 font-weight-bold text-primary">Insert Here </h6>
                    </div>
                    <!-- Card Body -->
                    <div class="">

                        <div class="">
                            <form action="../../php_function/adddriver.php" method="POST">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="fname" maxlength="25">

                                </div>
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control" name="mname" maxlength="25">

                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="lname" maxlength="25">

                                </div>
                                <div class="form-group">
                                    <label>Contact #</label>
                                    <input type="number" class="form-control" name="cp" max="99999999999">

                                </div>
                                <div>
                                    <button type="submit" onclick="return confirm('Do you want to submit?');"
                                        name="btn-driver" class="btn btn-success btn-user btn-block">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- /.container-fluid -->
<?php
			$cap = mysqli_query($db,"SELECT * FROM brgydriver;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<!-- update Modal-->
<div class="modal fade" id="editModal<?php echo $row['driver_id'];?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update record</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../../php_function/adddriver.php" method="POST">

                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" name="fname" maxlength="25"
                            value="<?php echo $row['fname'];?>">

                    </div>
                    <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" class="form-control" name="mname" maxlength="25"
                            value="<?php echo $row['mname'];?>">

                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="lname" maxlength="25"
                            value="<?php echo $row['lname'];?>">

                    </div>
                    <div class="form-group">
                        <label>Contact #</label>
                        <input type="number" class="form-control" name="cp" max="99999999999"
                            value="<?php echo $row['contact'];?>">

                    </div>
                    <input type="hidden" class="form-control" name="idfor" value="<?php echo $row['driver_id'];?>"
                        required>

                    <div>
                    </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" onclick="return confirm('Do you want to update?');" name="btn-driver-up"
                    class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>