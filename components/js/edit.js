   jQuery(document).ready(function() {

    $('.registration-form fieldset:first-child').fadeIn('slow'); 

    
    // next step

        $('.registration-form .btn-next').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).next().fadeIn();
    	});
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });

    
});


      function selectPurok(){

        switch($("#purokno").val()){
          
          case "1":
            document.getElementById("purok").value = "Gemma Ronquillo";
            break;
          case "2":
            document.getElementById("purok").value = "Jessa Mutia";
            break;
          case "3":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
          case "4":
            document.getElementById("purok").value = "Fortune Agustin II";
            break;
          case "5":
            document.getElementById("purok").value = "Edwin Gallera";
            break;
          case "6":
            document.getElementById("purok").value = "Ludivina Olaquer";
            break;
          case "RV3 7-A":
            document.getElementById("purok").value = "Flordelina Bermudez";
            break;
          case "RV3 7-B":
            document.getElementById("purok").value = "Marlyn Duran";
            break;
          case "RV3 8-A":
            document.getElementById("purok").value = "Emma Castillano";
            break;
          case "RV3 8-B":
            document.getElementById("purok").value = "Virginia Avelino";
            break;
          case "9":
            document.getElementById("purok").value = "Thelma Aninon";
            break;
          case "10":
            document.getElementById("purok").value = "Vicentita Ocay";
            break;
          case "11-A":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
          case "11-B":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
        }
      }

 

      
    $('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
   $(document).ready(function(){
     $('.radiobtn').click(function(){
       var demovalue = $(this).val(); 
         $("div.myDiv").hide();
         $("#show"+demovalue).show();
      });
   });
  
$(document).ready(function(){
 
 $('#familyheads').typeahead({
  source: function(query, result)
  {
   $.ajax({
    url:"fetch_family_head_only.php",
    method:"POST",
    data:{query:query},
    dataType:"json",
    success:function(data)
    {
     result($.map(data, function(item){
      return item;
     }));
    }
   })
  }
 });
 
}); 

var xmlHttp;
function familyhead()
{
var str = familyheads.value;
if(str == "Select State")
alert("");
xmlHttp=GetXmlHttpObject();
if (xmlHttp==null)
{
alert ("Your browser does not support AJAX!");
return;
}
var url="familyhead_id.php"; 
url=url+"?faquerysecs="+str;
url=url+"&sid="+Math.random();
xmlHttp.onreadystatechange=secChanged;
xmlHttp.open("GET",url,true);
xmlHttp.send(null);
}
function secChanged()
{
if (xmlHttp.readyState==4)
{
document.getElementById("fheads").innerHTML=xmlHttp.responseText;
}
}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
{
// Firefox, Opera 8.0+, Safari
xmlHttp=new XMLHttpRequest();
}
catch (e)
{
// Internet Explorer
try
{
xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e)
{
xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp;
}