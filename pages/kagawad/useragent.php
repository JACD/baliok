<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm text-left" data-toggle="modal"
            data-target="#addagent"><i class="fas fa-plus"></i> Add User Agent</a>

    </div>
    <div class="card-body">



        <!-- DREA E SULOD ANG CONTENT -->


        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Date Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Date Created</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php
					$cap = mysqli_query($db,"SELECT * FROM brgyuseragent;");
                    while($row = mysqli_fetch_array($cap))
                    {   
                      ?>
                                <td>
                                    <?php echo $row['firstname'].' '.$row['lastname'];?>
                                </td>
                                <td>
                                    <?php echo $row['auth'];?>
                                </td>
                                <td>
                                    <?php echo $row['date_created'];?>
                                </td>
                                <td>
                                    <button class="btn btn-success btn-circle" data-toggle="modal"
                                        data-target="#editModal<?php echo $row['useragent_id'];?>">
                                        <i class="fas fa-edit"> </i>
                                    </button>
                                    <a href="../../php_function/useragent.php?idremove=<?php echo $row['useragent_id'];?>&click=remove"
                                        onclick="return confirm('Are you sure you want to remove this user?');"
                                        class="btn btn-danger btn-circle">
                                        <i class="fas fa-times"> </i>
                                    </a>

                                </td>
                            </tr>

                            <?php
									}
									?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
</div>

<!-- /.container-fluid -->
<div class="modal fade" id="addagent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add User Agent</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../../php_function/useragent.php" method="POST">
                    <div class="form-group">
                        <label>Firstname *</label>
                        <input type="text" class="form-control" name="fname" required>
                    </div>
                    <div class="form-group">
                        <label>Lastname *</label>
                        <input type="text" class="form-control" name="lname" required>
                    </div>
                    <div class="form-group">
                        <label>Username / Email*</label>
                        <input type="email" class="form-control" name="uname" required>
                    </div>
                    <div class="form-group">
                        <label>Password *</label>
                        <input type="password" class="form-control" minlength="8" name="pwd" required>
                    </div>
                    <div class="form-group">
                        <label>Position *</label>
                        <select class="form-control" name="post" required>
                            <option></option>
                            <option value="landfill">landfill </option>
                            <option value="CENRO">CENRO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" value="<?php echo date('m-d-Y');?>" name="yearcreated"
                            required>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" onclick="return confirm('Do you want to save this user?');" name="btn-user-agent"
                    class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
			$cap = mysqli_query($db,"SELECT * FROM brgyuseragent;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<!-- update Modal-->
<div class="modal fade" id="editModal<?php echo $row['useragent_id'];?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update record</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../../php_function/useragent.php" method="POST">
                    <div class="form-group">
                        <label>Firstname *</label>
                        <input type="text" class="form-control" name="fname" value="<?php echo $row['firstname'];?>"
                            value="<?php echo $row['firstname'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Lastname *</label>
                        <input type="text" class="form-control" name="lname" value="<?php echo $row['lastname'];?>"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Username / Email*</label>
                        <input type="email" class="form-control" name="uname" value="<?php echo $row['username'];?>"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Password *</label>
                        <input type="password" class="form-control" minlength="8" name="pwd"
                            value="<?php echo $row['password'];?>" required>
                    </div>
                    <div class="form-group">
                        <label>Position *</label>
                        <select class="form-control" name="post" required>
                            <option value="<?php echo $row['auth'];?>"><?php echo $row['auth'];?></option>
                            <option value="landfill">landfill </option>
                            <option value="CENRO">CENRO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="useragentid"
                            value="<?php echo $row['useragent_id'];?>" required>
                        <input type="hidden" class="form-control" name="yearcreated"
                            value="<?php echo $row['date_created'];?>" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" onclick="return confirm('Do you want to update?');" name="btn-useragent-up"
                    class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>