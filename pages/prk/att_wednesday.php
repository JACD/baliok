<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Ecoboy Attendance</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="att_wednesday.php">WEDNESDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="att_saturday.php">SATURDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="att_sunday.php">SUNDAY</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <form action="" method="GET">
                    <div class="form-group">
                        <label>Week</label>
                        <select class="form-control" name="week_number" required>
                            <option />
                            <option value="week1">
                                WEEK 1
                            </option>
                            <option value="week2">
                                WEEK 2
                            </option>
                            <option value="week3">
                                WEEK 3
                            </option>
                            <option value="week4">
                                WEEK 4
                            </option>
                        </select>
                    </div>
            </div>
            <div class="col-xl-3 col-lg-8">
                <div class="form-group" style="margin-top:12%;">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                </form>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Collection Date</th>
                                <th>Ecoboy Name</th>
                                <th>Attendance</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Collection Date</th>
                                <th>Ecoboy Name</th>
                                <th>Attendance</th>
                            </tr>
                        </tfoot>
                        <tbody>

                            <?php 
                                 
                                    if(isset($_GET['week_number']))
                                    {
                                    $week=$_GET['week_number'];
                                 
                                    $result = mysqli_query($db,"SELECT * FROM `schedule` WHERE `week_no`='$week' AND `days`='wednesday';");

                                    }
                                    else 
                                    {
                                        
                                    $result = mysqli_query($db,"SELECT * from schedule where days='wednesday';");
                                    }
                                    while($row = mysqli_fetch_array($result))
                                    { 
                                          if(isset($row['CollDate'])){ 
                                    ?>
                            <tr>
                                <td> <?php echo  $dates= date("F j, Y  "."("."l".")",strtotime($row['CollDate']));?>
                                </td>

                                <td><?php echo $row['EcoboyName'];?></td>
                                <?php if($row['status']=='Approve') { ?>
                                <td> <span style="color:green;">Present</span></td>
                                <?php } else if($row['status']=='denied') { ?>
                                <td><span style="color:red;">Present</span></td>
                                <?php } ?>
                                <?php
                                 }
                                else
                                 { 
                                }
                                ?>
                            </tr>

                            <?php  
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>