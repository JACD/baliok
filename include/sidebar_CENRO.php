    <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <?PHP include 'logo.php';?>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="./">
                <i class="fas fa-fw fa-home"></i>
                <span>Home</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">


        <!-- Nav Item - Ecoboy -->
        <li class="nav-item">
            <a class="nav-link" href="../../pages/useragent/cenro.php">
                <i class="fas fa-fw fa-table"></i>
                <span>CENRO</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/kagawad/reports.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Accomplishment Report</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/useragent/garbage_request.php">
                <i class="fas fa-fw fa-trash"></i>
                <span>Garbage Request</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->