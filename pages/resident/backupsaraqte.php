


                <?php include '../../include/mainincludetop.php';?>
<style>
/* HIDE RADIO */

[type=radio] {
    position: absolute;
    opacity: 0;
    width: 0;
    height: 0;
}

/* IMAGE STYLES */

[type=radio]+img {
    cursor: pointer;
}

/* CHECKED STYLES */

[type=radio]:checked+img {
    outline: 2px solid green;
}

.checked {
    color: orange;
}

.select {
    color: orange !important;
}

.clr {
    color: orange !important;
}
</style>
<?php include '../../php_function/session_name.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Ecoboy</h6>
        </center>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <!--button class="btn btn-primary" data-toggle="modal" data-target="#openAllModal">
                    Donation
                </button-->

                <br /><br />
            </div>
            <div class="col-xl-3 col-lg-8">

            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 col-lg-8">

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Nickname</th>
                            <th>Ecoboy Name</th>
                            <th>Rate/Donate</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Image</th>
                            <th>Nickname</th>
                            <th>Ecoboy Name</th>
                            <th>Rate/Donate</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <?php  
					$result = mysqli_query($db,"SELECT * FROM resident where position='Ecoboy';"); 
                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                            <td><img class="rounded-circle" width="30%" height="50%"
                                    src="../../components/img/profile/<?php echo $row['photo'];?>"></td>
                            <td>
                                <?php echo $row['nickname']; ?>
                            </td>
                            <td>
                                <?php echo $row['fname'].' '.$row['lname']; ?>
                            </td>
                            <td>
                                <div id="album_rating" class="rating" onmouseout="removehover();">
                                    <span class="fa fa-star star1" data-id="<?php echo $row['id'];?>"
                                        onmouseover="hovericonone();" id="rating_star1"></span>
                                    <span class="fa fa-star star2" data-id="<?php echo $row['id'];?>"
                                        onmouseover="hovericontwo();" id="rating_star2"></span>
                                    <span class="fa fa-star star3" data-id="<?php echo $row['id'];?>"
                                        onmouseover="hovericonthree();" id="rating_star3"></span>
                                    <span class="fa fa-star star4" data-id="<?php echo $row['id'];?>"
                                        onmouseover="hovericonfour();" id="rating_star4"></span>
                                    <span class="fa fa-star star5" data-id="<?php echo $row['id'];?>"
                                        onmouseover="hovericonfive();" id="rating_star5" value="5"></span>
                                </div>
                                <div id="show" style="display: none;" class="rating">
                                    <span class="fa fa-star clr"></span>
                                    <span class="fa fa-star clr"></span>
                                    <span class="fa fa-star clr"></span>
                                    <span class="fa fa-star clr"></span>
                                    <span class="fa fa-star clr"></span>
                                </div>

                                <!-- <button class="btn btn-primary" data-toggle="modal"
                                    data-target="#openModal<?php echo $row['id'];?>">
                                    <i class="fas fa-eye"> </i>
                                </button>-->
                            </td>
                        </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<?php
			$cap = mysqli_query($db,"SELECT * FROM resident;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal" id="openModal<?php echo $row['id'];?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Rate</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">


                <form action="../../php_function/function_resident_rate.php" method="POST">
                    <div class="form-group row"
                        style="border:1px solid #b3bdc7; text-align:center; padding: 10px 10px 10px 50px;">
                        <div class="col-sm-2 mb-12 mb-sm-0">
                            <label class="radio-inline">
                                <input type="radio" name="star" value="1" />
                                <img class="rounded-circle" width="100%" height="100%"
                                    src="../../components/img/images/star.png"><br />
                            </label>
                        </div>
                        <div class="col-sm-2 mb-12 mb-sm-0">
                            <label class="radio-inline">

                                <input type="radio" name="star" value="2" />
                                <img class="rounded-circle" width="100%" height="100%"
                                    src="../../components/img/images/star.png"><br />
                            </label>
                        </div>
                        <div class="col-sm-2 mb-12 mb-sm-0">
                            <label class="radio-inline">
                                <input type="radio" name="star" value="3" />
                                <img class="rounded-circle" width="100%" height="100%"
                                    src="../../components/img/images/star.png"><br />
                            </label>
                        </div>
                        <div class="col-sm-2 mb-12 mb-sm-0">
                            <label class="radio-inline">
                                <input type="radio" name="star" value="4" />
                                <img class="rounded-circle" width="100%" height="100%"
                                    src="../../components/img/images/star.png"><br />
                            </label>
                        </div>
                        <div class="col-sm-2 mb-12 mb-sm-0">
                            <label class="radio-inline">
                                <input type="radio" name="star" value="5" />
                                <img class="rounded-circle" width="100%" height="100%"
                                    src="../../components/img/images/star.png"><br />
                            </label>
                        </div>

                    </div>
                    <div class="form-group">
                        <h4>Donation</h4>
                        <input type="number" class="form-control" placeholder="optional" name="tip" />
                    </div>
                    <input type="hidden" value="<?php echo $row['id'];?>" name="ecoboyid" />


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button type="submit" name="btn-plert" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
    $idhere=$row['Sched_No'];
  ?>
<div class="modal" id="openModal<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Giving a donation are not mandatory.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/send_pledge.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <?php 
                $pledges = mysqli_query($db, "SELECT * FROM pledge where sked_id='$idhere';");
                while ($rows = mysqli_fetch_array($pledges)) {
                ?>
                    <span style="color:red;">NOTE: <br /> You are already give a donation with the amount of PHP
                        <?php echo $rows['amount']; ?> </span>
                    <?php } ?>
                    <br />
                    <br />
                    <div class="form-group">
                        <input type="number" class="form-control" placeholder="optional" name="tip" />
                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="pledge" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>


<div class="modal" id="openAllModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Give donation to all Ecoboy.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/send_pledge.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label>How much per Ecoboy?</label>
                        <input type="number" name="tip" class="form-control" placeholder="Enter Here" />
                        <input type="hidden" value="<?php echo $idres; ?>" name="idres" />
                        <input type="hidden" value="<?php echo $fullname; ?>" name="nameres" />

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="pledge" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>




<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>