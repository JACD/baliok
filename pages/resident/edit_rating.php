<?php include '../../include/mainincludetop.php';?>
<style>
/* HIDE RADIO */

[type=radio] {
    position: absolute;
    opacity: 0;
    width: 0;
    height: 0;
}

/* IMAGE STYLES */

[type=radio]+img {
    cursor: pointer;
}

/* CHECKED STYLES */

[type=radio]:checked+img {
    outline: 2px solid green;
}

.checked {
    color: orange;
}

.select {
    color: orange !important;
}

.clr {
    color: orange !important;
}
</style>
<?php include '../../php_function/session_name.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Rate Ecoboy</h6>
        </center>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-xl-4 col-lg-4">
            </div>
            <div class="col-xl-4 col-lg-4">
                <?php  
                 $rateid=$_GET['rate_id'];
                 $ayde=$_GET['taoid'];
					$result = mysqli_query($db,"SELECT * FROM resident where position='Ecoboy' AND id='$ayde';"); 
                    while($row = mysqli_fetch_array($result))
                    {   
                        $ecoid=$row['id'];
                      ?>
                <center>
                    <img class="rounded-circle" width="30%" height="40%"
                        src="../../components/img/profile/<?php echo $row['photo'];?>">

                </center>
                <div class="form-group">
                    <label><strong>Nick name: </strong></label><br />
                    <?php echo $row['nickname']; ?>
                </div>
                <div class="form-group">
                    <label><strong>Complete name: </strong></label><br />
                    <?php echo $row['fname'].' '.$row['lname']; ?>
                </div>
                <form action="../../php_function/function_resident_rate.php" method="POST">

                    <div class="form-group">

                        <label><strong>Rating </strong></label><br />
                        <div id="album_rating" class="rating" onmouseout="removehover();">
                            <span class="fa fa-star star1" onmouseover="hovericonone();" id="rating_star1"></span>
                            <span class="fa fa-star star2" onmouseover="hovericontwo();" id="rating_star2"></span>
                            <span class="fa fa-star star3" onmouseover="hovericonthree();" id="rating_star3"></span>
                            <span class="fa fa-star star4" onmouseover="hovericonfour();" id="rating_star4"></span>
                            <span class="fa fa-star star5" onmouseover="hovericonfive();" id="rating_star5"></span>
                        </div>
                    </div>
                    <div class="form-group" id="stargaze">

                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" value="<?php echo $ecoid;?>" name="ecoboyid">
                        <input type="hidden" class="form-control" value="<?php echo $rateid;?>" name="rateid">
                    </div>
                    <button class="btn btn-success" name="btn-update-plert"
                        onclick="return confirm('Are you sure you want to update?');"> Update </button>
                </form>
                <?php } ?>
            </div>
            <div class="col-xl-4 col-lg-4">
            </div>
        </div>
    </div>
</div>

<!-- /.container-fluid -->


<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>