<body id="container page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
        <?php
	 if(isset($_SESSION['session_caps']) OR isset($_SESSION['session_admin']))
	{
		include 'sidebar_caps.php';
	}
	else if(isset($_SESSION['session_kags']) OR isset($_SESSION['session_admin']))
	{
		include 'sidebar_kags.php';
	}
	else if(isset($_SESSION['session_eco']) OR isset($_SESSION['session_admin']))
	{
		include 'sidebar_eco.php';
	}
	else if(isset($_SESSION['session_res']) OR isset($_SESSION['session_admin']))
	{
		include 'sidebar_res.php';
	}
	else if(isset($_SESSION['session_prkleader']) OR isset($_SESSION['session_admin']))
	{
		include 'sidebar_prkleader.php';
	}
	else if(isset($_SESSION['session_user_agent']) OR isset($_SESSION['session_admin']))
	{
		if($_SESSION['session_user_agent']=='CENRO')
		{ 
			include 'sidebar_CENRO.php'; 
		}
		else if($_SESSION['session_user_agent']=='landfill')
		{ 
			include 'sidebar_landfill.php';
		}
	}
	?>
        <!-- Sidebar -->

        <!-- Sidebar -->


        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include 'navbar.php';?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">