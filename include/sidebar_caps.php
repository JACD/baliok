    <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <?PHP include 'logo.php';?>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="./">
                <i class="fas fa-fw fa-home"></i>
                <span>Home</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">


        <!-- Nav Item - Ecoboy -->
        <li class="nav-item">
            <a class="nav-link" href="../../pages/barangay_captain/schedule.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Schedule</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/barangay_captain/reports.php">
                <i class="fas fa-fw fa-file"></i>
                <span>Accomplishment Report</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/barangay_captain/report-bin.php">
                <i class="fas fa-fw fa-trash"></i>
                <span>Garbage Bin Report</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->