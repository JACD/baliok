<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Generate report</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Collection Date</th>
                                <th>Ecoboy</th>
                                <th>Plate number</th>
                                <th>Driver</th>
                                <th>Weight/net/kgs</th>
                                <th>Validate</th>
                                <th>Preview</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Collection Date</th>
                                <th>Ecoboy</th>
                                <th>Plate number</th>
                                <th>Driver</th>
                                <th>Weight/net/kgs</th>
                                <th>Validate</th>
                                <th>Preview</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php 
					$result = mysqli_query($db,"SELECT * FROM schedule where status='Approve';");
                         
                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                                <td>
                                    <?php echo date("F j, Y",strtotime($row['CollDateStart'])); ?>
                                </td>
                                <td>
                                    <?php echo $row['EcoboyName'];?>
                                </td>
                                <td>
                                    <?php echo $row['plate_number'];?>
                                </td>
                                <td>
                                    <?php echo $row['driver'];?>
                                </td>
                                <td>
                                    <?php echo $row['weight_net_kgs'];?>
                                </td>
                                <?php
                      if($row['plate_number']!='' && $row['driver']!='')
                      { 
                      ?>
                                <?php if($row['accomplish']=='ok') { ?>
                                <td style="color:green;"> Validated </td>
                                <?php } else { ?>
                                <td> </td>
                                <?php }?>
                                <?php } else { ?>
                                <td style="color:red;">Ecoboy not reported the plate number or the driver </td>
                                <?php } ?>
                                <?php
                      if($row['accomplish']=='ok')
                      { 
                      ?>
                                <td>
                                    <a href="../../php_function/report.php" target="_blank"
                                        class="btn btn-primary btn-circle">
                                        <i class="fas fa-file"> </i>
                                    </a>
                                </td>
                                <?php } else { ?>
                                <td> </td>
                                <?php } ?>

                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>