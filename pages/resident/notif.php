<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Alert Garbage Collected</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="col-lg-8">
                    <?php   

                    $notifier = mysqli_query($db, "SELECT * FROM `ecoboy_claimed_garbage` WHERE `resid`='".$_SESSION['session_user']."' AND `claim`='Yes';");

                    while ($notif = mysqli_fetch_array($notifier)) {
                     
                     ?>
                    <div class="card mb-4 py-3 border-left-info">
                        <div class="card-body">
                            <?php echo 'Ecoboy '. ucfirst($notif['Ecoboy_nym']);?>
                            collected the garbage on
                            <span class="text-primary">
                                <?php echo date("F j, Y" ,strtotime($notif['date_collected']));?>
                        </div>
                    </div>
                    <?php } ?>

                </div>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->
<?php include '../../include/mainincludebottom.php';?>a