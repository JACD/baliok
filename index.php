<?php
 error_reporting(0);
 	session_start();
 include('php_function/config.php');
 include('php_function/countuserreg.php');
 
    ?>
<!DOCTYPE html>
<html>

<head>
    <title>ECOLLECT</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="components/css/uikit.min.css" />
    <script src="components/js/uikit.min.js"></script>
    <script src="components/js/uikit-icons.min.js"></script>
    <link rel="icon" type="image/png" sizes="16x16" href="components/img/logo/favicon-16x16.png">

    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="components/css/sb-admin-2.min.css" rel="stylesheet">
    <script src="components/js/checkemail.js"></script>
    <style>
    @import url('https://fonts.googleapis.com/css?family=Poppins|Roboto&display=swap');

    html,
    h2,
    h3,
    h4,
    h5,
    h1 {
        font-family: 'Poppins' !important;
    }

    .uk-navbar-container:not(.uk-navbar-transparent) {
        background: #1cc88a !important;
    }

    .navbar-style {
        position: relative;
        z-index: 980;
        padding: -2% 8% 0% 8%;
    }

    .nav-link:hover {
        transition: 0.3s ease;
        border-bottom: 2px solid #ff9501;
        color: #fff !important;
    }

    .nav-link {
        border-bottom: 2px solid #1cc88a;
    }

    div.uk-navbar-right ul li a {
        color: #fff;
        font-size: 16px;
        font-family: 'Poppins';
        font-weight: bold;
    }

    div#mob-nav-style ul li a {
        color: #1cc88a;
        font-size: 16px;
        font-family: 'Poppins';
        font-weight: bold;
    }

    .uk-height-large {
        height: 100vh !important;
    }

    article#article-style {
        margin: 0% 15% 0% 15%;
        font-family: 'Poppins';
    }

    article#article-style h1 {
        color: #ffffff;
    }

    article#article-style p {
        color: #ffffff;
        font-size: 16px;
    }

    #btn-more-l {
        color: #1cc88a;
        background: #fff;
        border-color: #1cc88a;
        font-size: 18px;
        font-family: 'Poppins';
        border-radius: 30px;
    }

    .btn-login {
        border-radius: 30px;
        font-size: 15px;
        font-family: 'Poppins';
    }

    .btn-login-md {
        border-radius: 30px;
        font-size: 15px;
        font-family: 'Poppins';
    }

    .heartclr {
        color: #1cc88a;
    }

    .clr {
        color: #fff !important;
    }

    .uk-bg-green {
        background-color: #1cc88a !important;
    }

    .uk-button-meduim:hover {
        background-color: #1cc88a !important;
        color: #fff !important;
        border-color: #fff !important;
    }

    .uk-button-large:hover {
        background-color: #1cc88a !important;
        color: #fff !important;
        border-color: #fff !important;
    }

    #main-footer-fr {
        background-color: #fff;
        color: #1cc88a;
        padding: 20px 20px;
    }

    body::-webkit-scrollbar {
        width: 5px;
    }

    body::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }

    body::-webkit-scrollbar-thumb {
        background-color: #1cc88a;
        outline: 1px solid slategrey;
    }

    @media (max-width: 768px) {
        svg:not(:root) {
            color: #fff;
        }

        .btn-login {
            background-color: #0f7ae5 !important;
            color: #fff !important;
            border-color: #fff !important;
        }

        #btn-more-lg {
            margin: -5% 0% 0% 0% !important;
            color: #ffffff;
            background: #1cc88a;
            border-color: #ffffff;
            font-size: 18px;
            font-family: 'Poppins';
            border-radius: 30px;
        }

        article#article-style h1 {
            font-size: 30px;
            margin: -20% 0% -5% 0% !important;
        }

        article#article-style p {
            font-size: 18px;
            margin: 20% -35% 10% -30% !important;
        }

        button#btn-more-sm {
            margin: -35% 0% -5% 0% !important;
        }
    }
    </style>
</head>

<body>
    <div id="page" class="site">
        <div class="site-branding">
            <div class="uk-visible@m"
                uk-sticky="animation: uk-animation-slide-top sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; bottom: #transparent-sticky-navbar">
                <nav class="uk-navbar-container navbar-style" uk-navbar>
                    <div class="uk-navbar-left uk-animation-toggle">
                        <a class="uk-navbar-item uk-logo uk-animation-scale-up" href="index.html"><img
                                src="components/img/images/logoblank.png" width="120" alt=""></a>
                    </div>
                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav">
                            <li><a class="nav-link" href="#about-us" uk-scroll>ABOUT US</a></li>
                            <li><a class="nav-link" href="#brgyoff" uk-scroll>BARANGAY OFFICIALS </a></li>
                            <li><a class="nav-link" href="#hist" uk-scroll>HISTORY </a></li>
                            <li><a class="nav-link" href="#bc" uk-scroll>BARANGAY CAPTAIN</a></li>
                            <li><a class="nav-link" href="#popu" uk-scroll>POPULATION</a></li>
                        </ul>
                    </div>
                    <div class="uk-navbar-item">

                        <a class="uk-button uk-button-primary btn-login" href="#modal-group-1" uk-toggle>Login</a>

                    </div>
                </nav>
            </div>

            <div class="uk-hidden@m">
                <div uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky">
                    <nav class="uk-navbar uk-navbar-container navbar-style">
                        <div class="uk-navbar-left">
                            <a class="uk-navbar-toggle" uk-toggle="target: #offcanvas-nav-primary" href="#">
                                <span uk-navbar-toggle-icon></span> <span class="uk-margin-small-left"></span>
                            </a>
                        </div>
                    </nav>
                </div>
            </div>
            <!-------modal-------->

            <div id="modal-group-1" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-margin-auto-vertical">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <div class="uk-modal-header">
                        <h2 class="uk-modal-title">Login</h2>
                    </div>
                    <div class="uk-modal-body">
                        <div uk-grid>
                            <div class="uk-width-auto@m"></div>
                            <div class="uk-width-expand@m">
                                <form class="user" method="POST" action="php_function/login.php">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            placeholder="Enter Email Address" name=" email" required="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user"
                                            id="exampleInputPassword" placeholder="Password" name="password"
                                            required="">
                                    </div>
                                    <!-- <button type="submit" name="btn-login" class="btn btn-primary btn-user btn-lg">
                                                    Login
                                                </button> -->
                                    <div class="btn-login-md" style="text-align:center;">
                                        <button class="uk-button uk-button-large uk-button-primary btn-login"
                                            name="btn-login">Login</button>
                                    </div>
                                </form>
                            </div>
                            <div class="uk-width-auto@m"></div>
                        </div>

                    </div>
                    <div class="uk-modal-footer">
                        <div class="text-center">
                            <a href="#modal-group-2" class="" uk-toggle>Create an Account!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-group-2" class="uk-flex-top" uk-modal>
            <div class="uk-modal-dialog uk-margin-auto-vertical">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                <div class="uk-modal-header">
                    <h2 class="uk-modal-title">Sign up</h2>
                </div>
                <div class="uk-modal-body">
                    <div uk-grid>
                        <div class="uk-width-auto@m"></div>
                        <div class="uk-width-expand@m">
                            <form class="user" action="mail/sendmail.php" role="form" method="POST" data-ajax="false">
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" onkeyup="checkemail();"
                                        id="email" autocomplete="off" placeholder="Enter Email Address" name="email"
                                        required="required">
                                </div>
                                <span id="name_status"></span>
                                <div class="btn-login-md" style="text-align:center;">
                                    <button
                                        class="uk-button uk-button-large uk-button-primary btn-login">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div class="uk-width-auto@m"></div>
                    </div>
                </div>
                <div class="uk-modal-footer">
                    <div class="text-center">
                        <a href="#modal-group-1" class="" uk-toggle>Already Registered?</a>
                    </div>
                </div>
            </div>
        </div>
        <!--------------->

        <div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
            <div class="uk-offcanvas-bar uk-flex uk-flex-column uk-background-default" id="mob-nav-style">
                <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical uk-text-justify">
                    <li class="uk-active">
                        <a href="#"><img src="components/img/logo/logolanding.png" width="120" alt=""></a>
                    </li>
                    <li><a class="navlink" href="#about-us">ABOUT US</a></li>
                    <li><a class="navllink" href="#brgyoff">BARANGAY OFFICIALS </a></li>
                    <li><a class="nav-link" href="#hist" uk-scroll>HISTORY </a></li>
                    <li><a class="navlink" href="#bc">BARANGAY CAPTAIN</a></li>
                    <li><a class="navlink" href="#popu">POPULATION</a></li>
                    <div class="uk-navbar-item">
                        <form action="login.php">
                            <input class="uk-input uk-form-width-small" type="text" placeholder="Input">
                            <a class="uk-button uk-button-primary btn-login" href="#modal-group-1" uk-toggle>Login</a>

                        </form>
                    </div>
                </ul>

            </div>
        </div>
    </div>
    <!---- Navbar ---->
    <div class="uk-padding-left uk-padding-right">
        <div class="uk-cover-container uk-height-large">
            <img src="components/img/images/bgren.jpg" alt="" uk-cover>
            <div class="uk-position-center uk-text-center">
                <article class="uk-article uk-padding" id="article-style">
                    <h1 class="uk-article-title uk-text-bolder">WELCOME BARANGAY BALIOK</h1>
                    <p class="uk-article-meta uk-text-small">Visit the Barangay Baliok for more information just contact
                        Mrs. Lolita L. Marin.</p>
                </article>
                <a href="#about-us" uk-scroll> <button
                        class="uk-button uk-button-default uk-button-large uk-margin uk-visible@s uk-animation-scale-up uk-transform-origin-bottom-right"
                        id="btn-more-l"><span>MORE INFO</span></button></a>
                <a href="#about-us" uk-scroll> <button
                        class="uk-button uk-button-default uk-button-meduim uk-margin uk-hidden@s uk-animation-scale-up uk-transform-origin-bottom-right"
                        id="btn-more-lg"><span>MORE INFO</span></button></a>
            </div>
        </div>
        <div class="uk-grid-divider uk-child-width-expand@s uk-section-muted uk-padding-large" id="about-us" uk-grid>
            <div class="uk-text-center">
                <span uk-icon="icon: heart; ratio: 2" class="heartclr"></span>
                <h3 class="uk-text-bolder">VISION</h3>
                <p>The Barangay stands as a beacon to lift its constituency in an improved and dignified quality of
                    life.
                </p>
            </div>
            <div class="uk-text-center">
                <span uk-icon="icon: heart; ratio: 2" class="heartclr"></span>
                <h3 class="uk-text-bolder">MISSION</h3>
                <p>The Barangay, as a conduit of the government in nation building, inspired by Filipino patriotism, is
                    committed to fully develop the human resource of its well-balanced ecology and appropriate
                    technological complementing facilities.
                    In attaining such endeavor, its network of livelihood and revitalized economic enterprise, through
                    free access to faith maintenance of enhanced vital social services. In this light, it promotes an
                    empirical linkage of a partnership
                    between the barangay and the international communities towards the propagation of ideal and
                    progressive habitat.</p>
            </div>
        </div>

        <div class="uk-container uk-padding" id="brgyoff">
            <div class="uk-text-center">
                <h2 class="uk-text-bolder">BARANGAY OFFICIALS</h2>
                <span uk-icon="icon: chevron-down; ratio: 2" class="heartclr uk-padding uk-padding-remove-top"></span>
            </div>
            <div class="uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-text-center uk-margin-left uk-margin-right"
                uk-grid>
                <?php
					$kag = mysqli_query($db,"SELECT fname,lname,mname,photo,rank,yeardeclared FROM resident INNER JOIN brgykagawad ON resident.id = brgykagawad.res_id where resident.position='Kagawad';");
                    while($row = mysqli_fetch_array($kag))
                    {    
                      ?>
                <div>
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="components/img/profile/<?php echo $row['photo'];?>" alt="Profile image">
                        </div>
                        <div class="uk-card-body">
                            <span uk-icon="icon: user; ratio: 1.5" class="heartclr"></span>
                            <h3 class="uk-card-title uk-text-bolder">
                                <?php echo $row['fname'].' '.$row['mname'].' '.$row['lname'];?>
                            </h3>
                            <h4>Rank:
                                <?php echo $row['rank'];?>
                            </h4>
                            <h5>Year Declared:
                                <?php echo $row['yeardeclared'];?>
                            </h5>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="uk-section uk-section-muted" id="hist">
            <div class="uk-container">

                <article class="uk-article">
                    <span uk-icon="icon: history; ratio: 2" class="heartclr"></span>
                    <h1 class="uk-article-title"><a class="uk-link-reset" href="">History/Background</a></h1>

                    <p>Barangay Baliok was created under a Commonwealth Act of 1937 and was inaugurated, March 1, 1951.
                        They elected their first Tenyente Del Barrio, the late Eugenio Naraval, and the Vice Tenyente
                        del Barrio, Mr. Floro C. Parba. By June
                        19, 1965, by virtue of RA No. 4354, otherwise known as the Revised Charter of Davao City,
                        Barangay Baliok was again recreated. It was named after a big tree that grows within the
                        vicinity having a common name – Bayok, with
                        a specific name “Pterospermum Diversifolium Blume.” The Bagobos, the dominant tribe during that
                        time, uses the bark of this tree for coloring. Later on, from the word Bayok, the Bagobos called
                        it Baliok.</p>

                    <p>On March 19, 2003, the Barangay Council of Baliok, under the leadership of our present Barangay
                        Rudolfo M. Bagajo. Sr., passed a Resolution No. 31 series of 2003, requesting the Honorable
                        Mayor Rodrigo Roa Duterte to issue a Proclamation
                        commemorating June 19 as Araw ng Barangay Baliok.
                    </p>

                    <p>On April 14, 2003, the Hon. City Mayor Rodrigo Roa Duterte issued a Proclamation No. 6 Declaring
                        June 19 and every year after that as Araw ng Barangay Baliok. We celebrated our First Araw last
                        June 19, 2003, commemorating the 38th
                        years of existence of Barangay Baliok. On October 29, 2007, Barangay Election, Antonio A.
                        Tinasas won as a Barangay Captain, which he was a Barangay Kagawad of Baliok for Twenty Five
                        (25) Years.</p>

                    <p>Barangay Baliok has a total land area of 306 hectares. Bounded on the North by Barangay Bago
                        Gallera: on the South by Barangay Lubogan: On the West by Barangay Bangkas Heights and on the
                        East by Barangay Dumoy.</p>

                    <p>Barangay Baliok, celebrated the annual Fiesta, every 29th day of August with a Patron Saint, St.
                        Agustine.</p>

                </article>
            </div>
        </div>

        <div class="uk-container uk-padding" id="bc">
            <div class="uk-text-center">
                <h2 class="uk-text-bolder">BARANGAY CAPTAIN</h2>
                <span uk-icon="icon: chevron-down; ratio: 2" class="heartclr uk-padding uk-padding-remove-top"></span>
            </div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s" uk-grid>
                <?php
					$cap = mysqli_query($db,"SELECT fname,lname,mname,photo,yeardeclared,current FROM resident INNER JOIN brgycaptain ON resident.id = brgycaptain.res_id where resident.position='Captain' and brgycaptain.current='Current Captain';");
                    while($row = mysqli_fetch_array($cap))
                    {    
                      ?>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="components/img/profile/<?php echo $row['photo'];?>" alt="Profile photo" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title uk-text-bolder"><span uk-icon="icon: user; ratio: 1.5"
                                class="heartclr">
                                <?php echo 'Hon. '.$row['fname'].' '.$row['mname'].'. '.$row['lname'];?></h3>

                        <h5>Year Declared: <?php echo $row['yeardeclared'];?>
                        </h5>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="uk-bg-green uk-text-center" id="popu">
            <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match uk-padding-large" uk-grid>
                <div>
                    <div class="uk-card uk-card-body" width="200" height="200">
                        <h2 class="clr">All residents</h2>
                        <hr class="uk-divider-small">
                        <h3 class="clr"> <?php alluser(); ?></h3>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-body clr" width="200" height="200">
                        <h2 class="clr">Male</h2>
                        <hr class="uk-divider-small">
                        <h3 class="clr"> <?php maleuser(); ?></h3>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-body clr" width="200" height="200">
                        <h2 class="clr">Female</h2>
                        <hr class="uk-divider-small">
                        <h3 class="clr"> <?php femaleuser(); ?></h3>
                    </div>
                </div>
            </div>
        </div>

        <!--class section five-->
    </div>
    </div>
    <footer id="main-footer">
        <div id="main-footer-fr">
            <div class="uk-text-center">
                Copyright 2017 | ECOLLECT | All Rights Reserved
            </div>
        </div>
    </footer>
</body>

<script src="regex/jquery.min.js"></script>
<script src="vendor/jquery/jquery.min.js"></script>

</html>