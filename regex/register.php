<?php include '../php_function/save.php';?>
<?php
error_reporting(1);
include '../fetch_include/fetch_session.php'; 
session_start(); 
$param ="https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$link = 'https://ecollect.ml/regex/register.php?key='.$_GET['key'].'&email='.$_GET['email'];
if ($param != $link)
{
 header ('Location: ../index.php'); 
}  
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registration</title>

    <!-- Custom fonts for this template-->

    <link rel="icon" type="image/png" sizes="16x16" href="/components/img/logo/favicon-16x16.png">
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../components/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="components/img/logo/favicon-32x32.png">

    <style>
    .myDiv {
        display: none;
    }

    form.registration-form fieldset {
        display: none;
    }

    .redcolor {
        color: red;
    }
    </style>
</head>

<body class="bg-gradient-success">
    <div>
        <form class="registration-form" role="form" onsubmit="" method="POST">
            <!--===First ni siya===-->
            <fieldset>

                <!-- Start of Personal Information -->
                <div class="container" id="registerForm">
                    <div class="card o-hidden border-0 shadow-lg my-5">

                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-5 d-none d-lg-block">
                                    <div class="row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-8">
                                            <img style=" margin: 0% 0% 0% -40%; width: 77vh; height: 136vh;"
                                                src="../components/img/images/bg_sign_up1st.jpg">
                                        </div>
                                        <div class="col-lg-2">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">

                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                        </div>
                                        <div class="form-group row">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">Personal Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Residence</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Citizenship</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Other Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Credentials</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h6>Red asterisk (*) are required fields</h6>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Barangay ID <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="Brgy_ID"
                                                    placeholder="19-0000" name="Brgy_ID" required="required">
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Precinct No</label>
                                                <input type="text" class="form-control" id="Precinct_No"
                                                    placeholder="0000-0" name="Precinct_No">
                                            </div>
                                        </div>
                                        <hr>
                                        <label class="control-label">Name</label>
                                        <div class="form-group row">
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Last <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="lasst"
                                                    placeholder="Last name" name="lname" required="required">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">First <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="First"
                                                    placeholder="First name" name="fname" required="required">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Middle</label>
                                                <input type="text" class="form-control" id="Middlenym"
                                                    placeholder="Middle name" name="mname">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Suffix</label>
                                                <select class="form-control" name="suffix">
                                                    <option></option>
                                                    <option>Jr.</option>
                                                    <option>Sr.</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Nickname</label>
                                                <input type="text" class="form-control" id="Nickname"
                                                    placeholder="Nickname" name="nickname">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="radio-inline">
                                                    <input type="radio" name="capability" id="capabilitye"
                                                        value="Illiterate">
                                                    Illiterate
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="capability" id="capability"
                                                        value="Person with Disabilty">
                                                    Person with Disabilty
                                                </label>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Assisted by:</label>
                                                <input type="text" class="form-control" id="assistor"
                                                    placeholder="Please fill-up Assistor's Oath" name="assistedby">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Gender<span
                                                        class="redcolor">*</span></label>
                                                <select class="form-control" name="gender" required="required">
                                                    <option></option>
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                    <option>LGBT</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Height</label>
                                                <input type="text" class="form-control" id="height" placeholder="Height"
                                                    name="height">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Weight</label>
                                                <input type="text" class="form-control" id="weight" placeholder="Weight"
                                                    name="weight">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-next">Next<span
                                                    aria-hidden="true">&rarr;</span></button>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Personal Information -->
            </fieldset>
            <!--===Second and so on===-->
            <fieldset>
                <!-- Start of Residence -->
                <div class="container">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-5 d-none d-lg-block">
                                    <div class="row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-8">
                                            <img style=" margin: 0% 0% 0% -40%; width: 77vh; height: 116vh;"
                                                src="../components/img/images/bg_sign_up1st.jpg">
                                        </div>
                                        <div class="col-lg-2">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                        </div>
                                        <div class="form-group row">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link " href="#">Personal Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">Residence</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link " href="#">Citizenship</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Other Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Credentials</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Province <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="Province"
                                                    placeholder="Province" name="province" required="required">
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">City/Municipality <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="City/Municipalities"
                                                    placeholder="City/Municipality" name="citymuni" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Barangay <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="Barangay"
                                                    placeholder="Barangay" name="barangay" required="required">
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">House No./Street</label>
                                                <input type="text" class="form-control" id="House No./Street"
                                                    placeholder="House No./Street" name="street">
                                            </div>
                                        </div>
                                        <hr>
                                        <label class="control-label">PERIOD OF RESIDENCE</label>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">In the City / Municipality</label>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">In the Philippines</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-3 mb-3 mb-sm-0">
                                                <label class="control-label">No. of Years </label>
                                                <input type="text" class="form-control" id="noofyears" name="noofyears">
                                            </div>
                                            <div class="col-sm-3 mb-3 mb-sm-0">
                                                <label class="control-label">No. of Months</label>
                                                <input type="text" class="form-control" id="noofmonths"
                                                    name="noofmonths">
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">No. of Years</label>
                                                <input type="text" class="form-control" id="numofyears"
                                                    name="numofyears">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-previous"><span
                                                    aria-hidden="true">&larr;</span>Previous</button>

                                        </div><br>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-next">Next<span
                                                    aria-hidden="true">&rarr;</span></button>

                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Residence -->
            </fieldset>

            <fieldset>
                <!-- Start of Citizenship -->
                <div class="container">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-5 d-none d-lg-block">
                                    <div class="row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-8">
                                            <img style=" margin: 0% 0% 0% -40%; width: 77vh; height: 118vh;"
                                                src="../components/img/images/bg_sign_up1st.jpg">
                                        </div>
                                        <div class="col-lg-2">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                        </div>
                                        <div class="form-group row">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link " href="#">Personal Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link " href="#">Residence</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">Citizenship</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Other Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Credentials</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Citizenship <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="citizenship"
                                                    placeholder="Citizenship" name="citizenship" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                <label class="radio-inline">
                                                    <input type="radio" name="citizenshipby" id="ctn" class="radiobtn"
                                                        value="ByBirth" />
                                                    By Birth
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="citizenshipby" id="cti" class="radiobtn"
                                                        value="Naturalized" />
                                                    Naturalized
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="citizenshipby" id="citi" class="radiobtn"
                                                        value="Reacquired" />
                                                    Reacquired
                                                </label>
                                            </div>
                                        </div>
                                        <!--Display Citizenship By-->
                                        <div class="form-group row">
                                            <div id="showByBirth" class="myDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                        <label class="control-label">DATE OF BIRTH</label>
                                                        <input type="date" class="form-control" id="DOB" name="dob">
                                                    </div>
                                                </div>
                                                <label class="control-label">PLACE OF BIRTH</label>
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                        <label class="control-label">City/Municipality</label>
                                                        <input type="text" class="form-control" id="City/Municipality"
                                                            placeholder="City/Municipality" name="cmuni">
                                                    </div>
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                        <label class="control-label">Province</label>
                                                        <input type="text" class="form-control" id="CProvince"
                                                            placeholder="Province" name="cprovince">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="showNaturalized" class="myDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-5 mb-3 mb-sm-0">
                                                        <label class="control-label">Date of Naturalization</label>
                                                        <input type="date" class="form-control" id="DON" name="don">
                                                    </div>
                                                    <div class="col-sm-7 mb-3 mb-sm-0">
                                                        <label class="control-label">Certificate No./Order of
                                                            Approval</label>
                                                        <input type="text" class="form-control" id="CNOA" name="cnoa">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="showReacquired" class="myDiv">
                                                <div class="form-group row">
                                                    <div class="col-sm-5 mb-3 mb-sm-0">
                                                        <label class="control-label">Date of Naturalization</label>
                                                        <input type="date" class="form-control" id="RDON" name="rdon">
                                                    </div>
                                                    <div class="col-sm-7 mb-3 mb-sm-0">
                                                        <label class="control-label">Certificate No./Order of
                                                            Approval</label>
                                                        <input type="text" class="form-control" id="CNOOA" name="cnooa">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">PROFESSION / OCCUPATION</label>
                                                <input type="text" class="form-control" id="Occupation"
                                                    name="occupation">
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">TIN</label>
                                                <input type="text" class="form-control" id="TIN"
                                                    placeholder="000-000-000" name="tin">
                                            </div>
                                        </div>
                                        <hr>
                                        <label class="control-label">CIVIL STATUS</label>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                <label class="radio-inline">
                                                    <input type="radio" name="civilstatus" id="a" value=" Single">
                                                    Single
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="civilstatus" id="b" value="Married">
                                                    Married
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="civilstatus" id="c" value="Widow/er">
                                                    Widow/er
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="civilstatus" id="Civilstatus"
                                                        value="Legally Separated">
                                                    Legally Separated
                                                </label>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <input type="text" class="form-control" id="Spouse"
                                                    placeholder="Name of Spouse, if Married" name="spouse">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-previous"><span
                                                    aria-hidden="true">&larr;</span>Previous</button>
                                        </div><br>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-next">Next<span
                                                    aria-hidden="true">&rarr;</span></button>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Citizenship-->
            </fieldset>

            <fieldset>
                <!-- Start of Other Information -->
                <div class="container">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-5 d-none d-lg-block">
                                    <div class="row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-8">
                                            <img style=" margin: 0% 0% 0% -40%; width: 77vh; height: 130vh;"
                                                src="../components/img/images/bg_sign_up1st.jpg">
                                        </div>
                                        <div class="col-lg-2">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                        </div>
                                        <div class="form-group row">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Personal Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Residence</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Citizenship</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">Other Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Credentials</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <hr>
                                        <label class="control-label">NAME OF FATHER</label>
                                        <div class="form-group row">
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Last</label>
                                                <input type="text" class="form-control" id="luhst"
                                                    placeholder="Last name" name="flname" required="required">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">First</label>
                                                <input type="text" class="form-control" id="Firsts"
                                                    placeholder="First name" name="ffname" required="required">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Middle</label>
                                                <input type="text" class="form-control" id="Middle"
                                                    placeholder="Middle name" name="fmname">
                                            </div>
                                        </div>
                                        <label class="control-label">NAME OF MOTHER</label>
                                        <div class="form-group row">
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Last</label>
                                                <input type="text" class="form-control" id="mlast"
                                                    placeholder="Last name" name="mlname" required="required">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">First</label>
                                                <input type="text" class="form-control" id="mfname"
                                                    placeholder="First name" name="mfname" required="required">
                                            </div>
                                            <div class="col-sm-4 mb-3 mb-sm-0">
                                                <label class="control-label">Middle</label>
                                                <input type="text" class="form-control" id="mmiddle"
                                                    placeholder="Middle name" name="mmname">
                                            </div>
                                        </div>
                                        <hr>
                                        <h6>ARE YOU HEAD OF THE FAMILY? <span class="redcolor">*</span></h6>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                <label class="radio-inline">
                                                    <input type="radio" name="hotf" id="yes" class="radiobtn"
                                                        value="Yes" />
                                                    Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="hotf" id="no" class="radiobtn"
                                                        value="No" />
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div id="showYes" class="myDiv">
                                                <div class="multi-field-wrapper" style="display: inline !important;">
                                                    <div class="multi-fields">
                                                        <div class="multi-field">
                                                            <input type="text " class="form-control" id="names"
                                                                name="stuff[]">
                                                            <button type="button"
                                                                class="btn btn-primary remove-field">Remove</button>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <button type="button" class="btn btn-primary add-field">Add
                                                        field</button>
                                                </div>
                                            </div>

                                            <div id="showNo" class="myDiv">
                                                <label class="control-label text-success">Enter name of the Family
                                                    Head <span class="redcolor">*</span></label>

                                                <input class="form-control" type="text" id="familyheads"
                                                    onchange="familyhead();" placeholder="Lastname, Firstname">
                                            </div>

                                            <div id="fheads"></div>


                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label>Purok Number: <span class="redcolor">*</span></label>
                                                <select class="form-control select2" id="purokno" name="Purok_No"
                                                    onchange="selectPurok()" required>
                                                    <option></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="RV3 7-A">RV3 7-A</option>
                                                    <option value="RV3 7-B">RV3 7-B</option>
                                                    <option value="RV3 8-A">RV3 8-A</option>
                                                    <option value="RV3 8-B">RV3 8-B</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11-A">11-A</option>
                                                    <option value="11-B">11-B</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                                <label class="control-label">Purok Leader <span
                                                        class="redcolor">*</span></label>
                                                <input type="text" class="form-control" id="purok" name="purokleader">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-previous"><span
                                                    aria-hidden="true">&larr;</span>Previous</button>
                                        </div><br>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-next">Next<span
                                                    aria-hidden="true">&rarr;</span></button>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Other Information -->
            </fieldset>

            <fieldset>
                <!-- Start of Credentials -->
                <div class="container">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-5 d-none d-lg-block">
                                    <div class="row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-8">
                                            <img style=" margin: 0% 0% 0% -40%; width: 77vh; height: 110vh;"
                                                src="../components/img/images/bg_sign_up1st.jpg">
                                        </div>
                                        <div class="col-lg-2">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                        </div>

                                        <div class="form-group row">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Personal Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Residence</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Citizenship</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link " href="#">Other Information</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">Credentials</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <hr>
                                        <label class="control-label">Email Address <span
                                                class="redcolor">*</span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                <input type="text" class="form-control"
                                                    value="<?php echo $_GET['email'];?>" placeholder="Email Address"
                                                    disable>

                                                <input type="hidden" value="<?php echo $_GET['email'];?>" name="email"
                                                    required>
                                            </div>
                                        </div>
                                        <label class="control-label">Password <span class="redcolor">*</span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                <input type="password" class="form-control" id="password"
                                                    placeholder="Password" name="password" pattern=".{8,12}"
                                                    title="8 to 12 characters" required>
                                            </div>
                                        </div>
                                        <label class="control-label">Repeat Password <span
                                                class="redcolor">*</span></label>
                                        <div class="form-group row">
                                            <div class="col-sm-12 mb-3 mb-sm-0">
                                                <input type="password" class="form-control" id="re_pass"
                                                    placeholder="Repeat Password" name="re_pass" pattern=".{8,12}"
                                                    title="8 to 12 characters" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-block btn-info btn-previous"><span
                                                    aria-hidden="true">&larr;</span>Previous</button>
                                        </div><br>
                                        <hr>
                                        <button type="submit" name="btn-reg" class="btn btn-primary btn-user btn-block">
                                            Register Account
                                        </button>
                                        <hr>

                                        <div class="text-center">
                                            <a class="small" href="../index.php">Already have an account? Login!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Credentials -->
            </fieldset>

        </form>



    </div>

    <!-- jQuery 3 -->
    <script src="jquery.min.js"></script>
    <script src="typeahead.min.js"></script>
    <!-------=========select JS =======---->
    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../components/js/sb-admin-2.min.js"></script>

    <!-- JavaScript for Previous and Next-->

    <script type="text/Javascript">
        jQuery(document).ready(function() {

    $('.registration-form fieldset:first-child').fadeIn('slow'); 

    
    // next step

        $('.registration-form .btn-next').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).next().fadeIn();
    	});
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });

    
});


      function selectPurok(){

        switch($("#purokno").val()){
          
          case "1":
            document.getElementById("purok").value = "Gemma Ronquillo";
            break;
          case "2":
            document.getElementById("purok").value = "Jessa Mutia";
            break;
          case "3":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
          case "4":
            document.getElementById("purok").value = "Fortune Agustin II";
            break;
          case "5":
            document.getElementById("purok").value = "Edwin Gallera";
            break;
          case "6":
            document.getElementById("purok").value = "Ludivina Olaquer";
            break;
          case "RV3 7-A":
            document.getElementById("purok").value = "Flordelina Bermudez";
            break;
          case "RV3 7-B":
            document.getElementById("purok").value = "Marlyn Duran";
            break;
          case "RV3 8-A":
            document.getElementById("purok").value = "Emma Castillano";
            break;
          case "RV3 8-B":
            document.getElementById("purok").value = "Virginia Avelino";
            break;
          case "9":
            document.getElementById("purok").value = "Thelma Aninon";
            break;
          case "10":
            document.getElementById("purok").value = "Vicentita Ocay";
            break;
          case "11-A":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
          case "11-B":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
        }
      }

 

      
    $('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
   $(document).ready(function(){
     $('.radiobtn').click(function(){
       var demovalue = $(this).val(); 
         $("div.myDiv").hide();
         $("#show"+demovalue).show();
      });
   });
  
$(document).ready(function(){
 
 $('#familyheads').typeahead({
  source: function(query, result)
  {
   $.ajax({
    url:"fetch_family_head_only.php",
    method:"POST",
    data:{query:query},
    dataType:"json",
    success:function(data)
    {
     result($.map(data, function(item){
      return item;
     }));
    }
   })
  }
 });
 
}); 

var xmlHttp;
function familyhead()
{
var str = familyheads.value;
if(str == "Select State")
alert("");
xmlHttp=GetXmlHttpObject();
if (xmlHttp==null)
{
alert ("Your browser does not support AJAX!");
return;
}
var url="familyhead_id.php"; 
url=url+"?faquerysecs="+str;
url=url+"&sid="+Math.random();
xmlHttp.onreadystatechange=secChanged;
xmlHttp.open("GET",url,true);
xmlHttp.send(null);
}
function secChanged()
{
if (xmlHttp.readyState==4)
{
document.getElementById("fheads").innerHTML=xmlHttp.responseText;
}
}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
{
// Firefox, Opera 8.0+, Safari
xmlHttp=new XMLHttpRequest();
}
catch (e)
{
// Internet Explorer
try
{
xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e)
{
xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp;
}
</script>

</body>

</html>