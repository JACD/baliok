<?php

//load.php
include 'dbconfig.php';

$data = array();

$query = "SELECT * FROM schedule ORDER BY Sched_No";

$statement = $connect->prepare($query);

$statement->execute();

$result = $statement->fetchAll();

foreach($result as $row)
{
 $data[] = array(
  'id'   => $row["Sched_No"],
  'title'   => $row["title"],
  'start'   => $row["CollDateStart"],
  'end'   => $row["CollDateEnd"],
  'status'   => $row["status"]
 );
}

echo json_encode($data);

?>