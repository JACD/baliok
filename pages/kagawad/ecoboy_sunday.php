<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Garbage Collection Schedule</h6>
        </center>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" data-toggle="modal"
            data-target="#myModal"><i class="fas fa-plus"></i> Create Schedule</a>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="ecoboy_wednesday.php">WEDNESDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ecoboy_saturday.php">SATURDAY</a>
                </li>
                <li class="nav-item border-bottom-info">
                    <a class="nav-link active" href="ecoboy_sunday.php">SUNDAY</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <form action="" method="GET">
                    <div class="form-group">
                        <label>Week</label>
                        <select class="form-control" name="week_number" required>
                            <option />
                            <option value="week1">
                                WEEK 1
                            </option>
                            <option value="week2">
                                WEEK 2
                            </option>
                            <option value="week3">
                                WEEK 3
                            </option>
                            <option value="week4">
                                WEEK 4
                            </option>
                        </select>
                    </div>
            </div>
            <div class="col-xl-3 col-lg-8">
                <div class="form-group" style="margin-top:12%;">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                </form>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Collection Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Purok No.</th>
                            <th>Ecoboy Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Collection Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Purok No.</th>
                            <th>Ecoboy Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <?php 
                            if(isset($_GET['week_number']))
                            { 
                                $cweek = $_GET['week_number'];
					$result = mysqli_query($db,"SELECT * FROM schedule where week_no='$cweek' and days='sunday';");
                            }
                            else
                             {
					$result = mysqli_query($db,"SELECT * FROM schedule where days='sunday';");
                             }
                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                            <td><?php echo date("F j, Y  "."("."l".")",strtotime($row['CollDate'])); ?></td>
                            <td><?php echo $row['StartTime'];?></td>
                            <td><?php echo $row['EndTime'];?></td>
                            <td><?php echo $row['Purok_No'];?></td>
                            <td><?php echo $row['EcoboyName'];?></td>
                            <?php
                      if($row['status']=='inactive')
                      { 
                      ?>
                            <td style="color:blue;">Not yet reviewed</td>
                            <?php } else if($row['status']=='denied') { ?>
                            <td style="color:red;">Not available </td>
                            <?php } else if ($row['status'] == 'Approve') { ?>
                            <td style="color:green;"> Approved</td>
                            <?php } ?>
                            <td>
                                <button class="btn btn-success btn-circle" data-toggle="modal"
                                    data-target="#editModal<?php echo $row['Sched_No'];?>">
                                    <i class="fas fa-edit"> </i>
                                </button>
                                <a href="../../php_function/function_for_ecoboy.php?delete=<?php echo $row['Sched_No'];?>&click-delete=delete"
                                    onclick="return confirm('Are you sure you want to remove this schedule?');"
                                    class="btn btn-danger btn-circle">
                                    <i class="fas fa-times"> </i>
                                </a>
                                <button class="btn btn-primary btn-circle" data-toggle="modal"
                                    data-target="#openModal<?php echo $row['Sched_No'];?>">
                                    <i class="fas fa-eye"> </i>
                                </button>
                            </td>
                        </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->
<!-- update Modal-->
<form action="../../php_function/add_schedule.php" role="form" method="POST">
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title">Garbage Collection Schedule</h4>
                    </center>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="hidden" value="sunday" name="day" required>
                                <input type="date" class="form-control" id="date" name="CollDate" required>
                            </div>
                            <div class="form-group">
                                <label>Start Time</label>
                                <input type="time" class="form-control" id="start" name="StartTime" required>
                            </div>
                            <div class="form-group">
                                <label>End Time</label>
                                <input type="time" class="form-control" id="end" name="EndTime" required>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <label>Purok No.:</label>
                                <select class="form-control select2" id="purok" name=" Purok_No" required>
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="RV3 7-A">RV3 7-A</option>
                                    <option value="RV3 7-B">RV3 7-B</option>
                                    <option value="RV3 8-A">RV3 8-A</option>
                                    <option value="RV3 8-B">RV3 8-B</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11-A">11-A</option>
                                    <option value="11-B">11-B</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ecoboy Name:</label>
                                <select class="form-control select2 form-control" id="ecoboy" name="EcoboyName"
                                    required>
                                    <option></option>
                                    <?php 
				       	$query = mysqli_query($db, "Select * from resident WHERE position='Ecoboy';") or die(mysqli_error());
                 while($fetch = mysqli_fetch_array($query))
                 {
                   $fullname=$fetch['fname'].' '.$fetch['lname'];
                      ?>
                                    <option value="<?php echo $fetch['id']; ?>"><?php echo $fullname; ?></option>
                                    <?php
                    }
                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Week</label>
                                <select class="form-control" name="weekno" required>
                                    <option />
                                    <option value="week1">
                                        WEEK 1
                                    </option>
                                    <option value="week2">
                                        WEEK 2
                                    </option>
                                    <option value="week3">
                                        WEEK 3
                                    </option>
                                    <option value="week4">
                                        WEEK 4
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-sked" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal" id="openModal<?php echo $row['Sched_No'];?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Reason</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <p><?php echo $row['reason'];?></p>


                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
    <div class="modal" id="editModal<?php echo $row['Sched_No'];?>">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title">Update Schedule</h4>
                    </center>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="date" class="form-control" id="date" value="<?php echo $row['CollDate'];?>"
                                    name="CollDate" required>
                                <input type="hidden" class="form-control" id="" value="<?php echo $row['Sched_No'];?>"
                                    name="idhere" required>
                                <input type="hidden" value="sunday" name="day" required>

                            </div>
                            <div class="form-group">
                                <label>Start Time</label>
                                <input type="time" class="form-control" id="start"
                                    value="<?php echo $row['StartTime'];?>" name="StartTime" required>
                            </div>
                            <div class="form-group">
                                <label>End Time</label>
                                <input type="time" class="form-control" id="end" value="<?php echo $row['EndTime'];?>"
                                    name="EndTime" required>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <label>Purok No.:</label>
                                <select class="form-control select2" id="purok" name="Purok_No" required>
                                    <option value="<?php echo $row['Purok_No'];?>"><?php echo $row['Purok_No'];?>
                                    </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="RV3 7-A">RV3 7-A</option>
                                    <option value="RV3 7-B">RV3 7-B</option>
                                    <option value="RV3 8-A">RV3 8-A</option>
                                    <option value="RV3 8-B">RV3 8-B</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11-A">11-A</option>
                                    <option value="11-B">11-B</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ecoboy Name:</label>
                                <select class="form-control select2 form-control" id="ecoboy" name="EcoboyName"
                                    required>
                                    <option value="<?php echo $row['ecoboy_id'];?>"><?php echo $row['EcoboyName'];?>
                                    </option>
                                    <?php 
				       	$query = mysqli_query($db, "Select * from resident WHERE position='Ecoboy';") or die(mysqli_error());
                 while($fetch = mysqli_fetch_array($query))
                 {
                   $fullname=$fetch['fname'].' '.$fetch['lname'];
                      ?>
                                    <option value="<?php echo $fetch['id']; ?>"><?php echo $fullname; ?></option>
                                    <?php
                    }
                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Week</label>
                                <select class="form-control" name="weekno" required>
                                    <option value="<?php echo $row['week_no'];?>"><?php echo $row['week_no'];?></option>
                                    <option value="week1">
                                        WEEK 1
                                    </option>
                                    <option value="week2">
                                        WEEK 2
                                    </option>
                                    <option value="week3">
                                        WEEK 3
                                    </option>
                                    <option value="week4">
                                        WEEK 4
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="upt-sked" onclick="return confirm('Do you want to save?');"
                            class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->
<script>
var date = document.querySelector("[type=date]");

function noMondays(e) {
    var day = new Date(e.target.value).getUTCDay();

    // Days in JS range from 0-6 where 0 is Sunday and 6 is Saturday

    if (day == 1 || day == 2 || day == 3 || day == 4 || day == 5 || day == 6) {
        e.target.setCustomValidity("Please select sunday.");
    } else {
        e.target.setCustomValidity("");
    }
}

date.addEventListener("input", noMondays);
</script>


<?php include '../../include/mainincludebottom.php';?>