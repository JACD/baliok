-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: barangaybaliok
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accomplishment_report`
--

DROP TABLE IF EXISTS `accomplishment_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accomplishment_report` (
  `Accomp_No` int(11) NOT NULL AUTO_INCREMENT,
  `Date` varchar(20) NOT NULL,
  `Plate_No` varchar(50) NOT NULL,
  `Destination` varchar(50) NOT NULL,
  `Commodity` varchar(50) NOT NULL,
  `weight_net_kgs` varchar(20) NOT NULL,
  `cur_price` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`Accomp_No`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accomplishment_report`
--

LOCK TABLES `accomplishment_report` WRITE;
/*!40000 ALTER TABLE `accomplishment_report` DISABLE KEYS */;
INSERT INTO `accomplishment_report` VALUES (1,'2020-02-05','P#1001/Ryan Llamido Cabahog','CENR SANITARY LANDFILL D.C','SOLID WASTE','895',3.00),(2,'2020-02-07','P#1001/Ryan Llamido Cabahog','CENR SANITARY LANDFILL D.C','SOLID WASTE','900',3.00),(3,'2020-02-21','P#1001/Ryan Llamido Cabahog','CENR SANITARY LANDFILL D.C','SOLID WASTE','850',3.00);
/*!40000 ALTER TABLE `accomplishment_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `ses_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(10) NOT NULL,
  `auth` varchar(10) NOT NULL,
  PRIMARY KEY (`ses_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,'jie@jie','jie','Jie','Jie','user.png','admin');
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgycaptain`
--

DROP TABLE IF EXISTS `brgycaptain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgycaptain` (
  `brgycaptain_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` varchar(50) NOT NULL,
  `yearend` varchar(50) DEFAULT NULL,
  `current` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brgycaptain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgycaptain`
--

LOCK TABLES `brgycaptain` WRITE;
/*!40000 ALTER TABLE `brgycaptain` DISABLE KEYS */;
INSERT INTO `brgycaptain` VALUES (16,'11','sd asd','2018-03-05','2019-10-08','former barangay captain','2019-10-08 09:29:22'),(17,'19','jomari duran','2019-10-08','2019-10-29','former barangay captain','2019-10-29 10:35:42'),(18,'11','sd asd','2020-02-05','2020-02-05','Current Captain','2020-02-05 09:25:59');
/*!40000 ALTER TABLE `brgycaptain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgydriver`
--

DROP TABLE IF EXISTS `brgydriver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgydriver` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(25) NOT NULL,
  `mname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `contact` varchar(11) NOT NULL,
  `date_created` varchar(30) NOT NULL,
  `deleted` enum('0','1') NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgydriver`
--

LOCK TABLES `brgydriver` WRITE;
/*!40000 ALTER TABLE `brgydriver` DISABLE KEYS */;
INSERT INTO `brgydriver` VALUES (1,'Ryan','Llamido','Cabahog','09265541320','2019-12-10 13:11:17','0');
/*!40000 ALTER TABLE `brgydriver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgykagawad`
--

DROP TABLE IF EXISTS `brgykagawad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgykagawad` (
  `brgykagawad_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` varchar(5) NOT NULL,
  `rank` varchar(50) NOT NULL,
  `cat` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` varchar(50) DEFAULT NULL,
  `yearend` varchar(50) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brgykagawad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgykagawad`
--

LOCK TABLES `brgykagawad` WRITE;
/*!40000 ALTER TABLE `brgykagawad` DISABLE KEYS */;
INSERT INTO `brgykagawad` VALUES (8,'9','Rank 1','info','firstname lastname','2019-05-01',NULL,'2020-02-19 16:52:59'),(9,'23','Past Kagawad','Agriculture','Gaudiosa Bajala','2020-02-20','2020-02-20','2020-02-20 07:15:16'),(10,'28','Rank 2','Agriculture','Ramon Parba','2020-02-20',NULL,'2020-02-20 07:15:45'),(11,'40','Rank 3','Agriculture','Presencia Bandal','2020-02-20',NULL,'2020-02-20 07:19:40');
/*!40000 ALTER TABLE `brgykagawad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgypersonnel`
--

DROP TABLE IF EXISTS `brgypersonnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgypersonnel` (
  `personnel_id` int(11) NOT NULL AUTO_INCREMENT,
  `resident_id` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `brgyposition` varchar(50) NOT NULL,
  `yeardeclared` varchar(15) NOT NULL,
  `yearend` varchar(15) NOT NULL,
  PRIMARY KEY (`personnel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgypersonnel`
--

LOCK TABLES `brgypersonnel` WRITE;
/*!40000 ALTER TABLE `brgypersonnel` DISABLE KEYS */;
INSERT INTO `brgypersonnel` VALUES (5,'12','good Jv','Ecoboy','2019-05-13',''),(8,'14','HHHH wkol','Purok Leader','2018-10-18',''),(9,'18','sdf sdf','Past Ecoboy','2019-10-08','2020-02-22'),(10,'20','derick dayrit','Past Ecoboy','2019-10-30','2020-02-13');
/*!40000 ALTER TABLE `brgypersonnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgyposition`
--

DROP TABLE IF EXISTS `brgyposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgyposition` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `position` varchar(50) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgyposition`
--

LOCK TABLES `brgyposition` WRITE;
/*!40000 ALTER TABLE `brgyposition` DISABLE KEYS */;
INSERT INTO `brgyposition` VALUES (2,'This is the description of the purok leader','Purok Leader','2018-03-07'),(4,'This is the description of the ecoboy','Ecoboy','07-13-2019');
/*!40000 ALTER TABLE `brgyposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgyreceipt`
--

DROP TABLE IF EXISTS `brgyreceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgyreceipt` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `idsked` varchar(5) DEFAULT NULL,
  `weigh_in` varchar(20) NOT NULL,
  `plate_no` varchar(20) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `driver` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL,
  `gross_tare` varchar(20) NOT NULL,
  `total` varchar(20) NOT NULL,
  `commodity` varchar(20) NOT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgyreceipt`
--

LOCK TABLES `brgyreceipt` WRITE;
/*!40000 ALTER TABLE `brgyreceipt` DISABLE KEYS */;
INSERT INTO `brgyreceipt` VALUES (1,'12','02:10 pm, 08/10/2019','P#1000','joy','Jomari','toril','100','300','Solid Waste'),(2,'14','06:10 pm, 09/10/2019','P#10010','Jomari','ryan','Toril','150','450','Solid waste'),(3,'16','03:10 pm, 28/10/2019','P#1001','Cherry','Jomari','Toril','1000','3000','Solid waste'),(4,'18','02:10 pm, 30/10/2019','P#1001','Cherry','Jason','Daliao','3000','12000','Solid waste'),(5,'40','09:02 am, 01/02/2020','P#1003','Ryan A. Duran','Ryan Llamido Cabahog','Barangay Baliok','50/1900','200','Solid waste'),(6,'32','09:02 am, 06/02/2020','P#1002','Jimmy Mantos','Ryan Llamido Cabahog','Barangay Baliok','150/2050','600','Solid waste');
/*!40000 ALTER TABLE `brgyreceipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgyuseragent`
--

DROP TABLE IF EXISTS `brgyuseragent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgyuseragent` (
  `useragent_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  `auth` varchar(20) NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`useragent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgyuseragent`
--

LOCK TABLES `brgyuseragent` WRITE;
/*!40000 ALTER TABLE `brgyuseragent` DISABLE KEYS */;
INSERT INTO `brgyuseragent` VALUES (2,'Landfill','Clerk','user1@gmail.com','012345678','09-02-2019','landfill','download.jpg'),(3,'CENRO','Enforcer','user2@gmail.com','012345678','09-03-2019','CENRO',NULL);
/*!40000 ALTER TABLE `brgyuseragent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation`
--

DROP TABLE IF EXISTS `donation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation` (
  `pledge_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `res_id` varchar(5) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `ecoboyname` varchar(50) NOT NULL,
  `econickname` varchar(20) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `confirmation` varchar(10) DEFAULT NULL,
  `new` varchar(5) NOT NULL,
  `date` varchar(20) DEFAULT NULL,
  `reset` varchar(10) NOT NULL,
  PRIMARY KEY (`pledge_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation`
--

LOCK TABLES `donation` WRITE;
/*!40000 ALTER TABLE `donation` DISABLE KEYS */;
INSERT INTO `donation` VALUES (22,'ACT PBA','13','12','good Jv','Boyet',25.00,'Recieved','old','10-04-2019','reset'),(23,'ACT PBA','13','18','sdf sdf','juan',20.00,'Recieved','old','10-08-2019','reset'),(24,'ACT PBA','13','18','sdf sdf','juan',25.00,'Recieved','old','10-28-2019',''),(25,'ACT PBA','13','20','derick dayrit','asd',30.00,'Recieved','old','10-30-2019',''),(26,'ACT PBA','13','18','sdf sdf','juan',60.00,'Recieved','old','10-30-2019',''),(27,'rrr rrr','21','18','sdf sdf','juan',35.00,'Recieved','old','10-30-2019',''),(28,'rrr rrr','21','12','good Jv','Boyet',100.00,'Recieved','old','10-30-2019','reset'),(29,'ACT PBA','13','12','good Jv','Boyet',50.00,'Recieved','old','10-30-2019','reset'),(30,'Malou Balaod','35','12','Rogelio Ompod','Elio',10.00,'Recieved','old','02-21-2020','reset'),(31,'Gaudiosa Bajala','23','12','Rogelio Ompod','Elio',10.00,'Recieved','old','02-21-2020','reset'),(32,'Gaudiosa Bajala','23','12','Rogelio Ompod','Elio',20.00,'','','02-21-2020','reset'),(33,'Alma Cabal','31','12','Rogelio Ompod','Elio',20.00,'','','02-21-2020','reset'),(34,'Ryan Duran','13','12','Rogelio Ompod','Elio',10.00,'Recieved','old','02-21-2020','reset');
/*!40000 ALTER TABLE `donation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation_history`
--

DROP TABLE IF EXISTS `donation_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `eco_id` varchar(5) NOT NULL,
  `eco_name` varchar(50) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation_history`
--

LOCK TABLES `donation_history` WRITE;
/*!40000 ALTER TABLE `donation_history` DISABLE KEYS */;
INSERT INTO `donation_history` VALUES (3,'12','good Jv','5000.00','30-09-2019'),(4,'12','good Jv','25.00','10-04-2019'),(5,'18','sdf sdf','20.00','10-08-2019'),(6,'12','good Jv','150.00','02-04-2020'),(7,'12','Rogelio Ompod','10.00','02-21-2020'),(8,'12','Rogelio Ompod','10.00','02-21-2020'),(9,'12','Rogelio Ompod','10.00','02-21-2020');
/*!40000 ALTER TABLE `donation_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecoboy_claimed_garbage`
--

DROP TABLE IF EXISTS `ecoboy_claimed_garbage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecoboy_claimed_garbage` (
  `garbage_id` int(5) NOT NULL AUTO_INCREMENT,
  `Ecoboy_nym` varchar(50) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `resid` varchar(5) NOT NULL,
  `resname` varchar(50) NOT NULL,
  `claim` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `skid` varchar(5) NOT NULL,
  `cdate` varchar(50) DEFAULT NULL,
  `date_collected` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`garbage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecoboy_claimed_garbage`
--

LOCK TABLES `ecoboy_claimed_garbage` WRITE;
/*!40000 ALTER TABLE `ecoboy_claimed_garbage` DISABLE KEYS */;
INSERT INTO `ecoboy_claimed_garbage` VALUES (10,'good Jv','12','','','Yes','','8','',NULL),(7,'good Jv','12','13','ACT PBA','No','asdfl','8','wednesday',NULL),(9,'good Jv','12','','','Yes','','','',NULL),(11,'good Jv','12','134','ACT PBA','No','','8','wednesday',NULL),(12,'good Jv','12','','','Yes','','11','',NULL),(13,'good Jv','12','','','Yes','','11','',NULL),(14,'sdf sdf','18','','','Yes','','12','',NULL),(15,'good Jv','12','','','Yes','','14','',NULL),(16,'good Jv','12','','','Yes','','14','',NULL),(17,'good Jv','12','','','Yes','','8','',NULL),(18,'good Jv','12','','','Yes','','11','',NULL),(19,'good Jv','12','','','Yes','','14','',NULL),(20,'good Jv','12','13','ACT PBA','Yes','','16','wednesday','28-10-2019'),(21,'good Jv','12','13','ACT PBA','Yes','','8','wednesday','28-10-2019'),(22,'good Jv','12','13','ACT PBA','Yes','','11','saturday','28-10-2019'),(23,'good Jv','12','13','ACT PBA','Yes','','14','sunday','28-10-2019'),(24,'good Jv','12','13','ACT PBA','Yes','','18','wednesday','30-10-2019'),(25,'good Jv','12','13','ACT PBA','Yes','','26','','10-12-2019'),(26,'good Jv','12','13','ACT PBA','Yes','','29','','10-12-2019'),(27,'good Jv','12','13','ACT PBA','Yes','','32','','18-12-2019'),(28,'good Jv','12','22','Jomari Dela Cruz','Yes','','38','','28-12-2019'),(29,'good Jv','12','13','ACT PBA','Yes','','40','','07-01-2020'),(30,'good Jv','12','22','Jomari Dela Cruz','Yes','','40','','07-01-2020'),(31,'good Jv','12','13','ACT PBA','Yes','','47','','24-01-2020'),(32,'good Jv','12','13','ACT PBA','Yes','','50','','27-01-2020'),(33,'Rogelio Ompod','12','22','Jomari Dela Cruz','Yes','','49','','06-02-2020'),(34,'Rogelio Ompod','12','13','Ryan Duran','Yes','','49','','06-02-2020'),(35,'Rogelio Ompod','12','13','Ryan Duran','Yes','','51','','07-02-2020'),(36,'Rogelio Ompod','12','31','Alma Cabal','Yes','','52','','08-02-2020'),(37,'Rogelio Ompod','12','36','Dolley Casas','Yes','','52','','08-02-2020'),(38,'Rogelio Ompod','12','39','evelyn manlapaz','Yes','','52','','08-02-2020'),(39,'Rogelio Ompod','12','23','Gaudiosa Bajala','Yes','','52','','08-02-2020'),(40,'Rogelio Ompod','12','30','Jocelyn Galleon','Yes','','52','','08-02-2020'),(41,'Rogelio Ompod','12','35','Malou Balaod','Yes','','52','','08-02-2020'),(42,'Rogelio Ompod','12','37','Maria Delinda','Yes','','52','','08-02-2020'),(43,'Rogelio Ompod','12','24','Marilyn Duran','Yes','','52','','08-02-2020'),(44,'Rogelio Ompod','12','29','Mary Ann Bulaloc','Yes','','52','','08-02-2020'),(45,'Rogelio Ompod','12','25','Mary Villas','Yes','','52','','08-02-2020'),(46,'Rogelio Ompod','12','27','Nene Orce','Yes','','52','','08-02-2020'),(47,'Rogelio Ompod','12','28','sdf sdfaa','Yes','','52','','08-02-2020'),(48,'Rogelio Ompod','12','32','Vicustita Ocay','Yes','','52','','08-02-2020'),(49,'Rogelio Ompod','12','26','Yula Arsenio','Yes','','52','','08-02-2020'),(50,'Rogelio Ompod','12','40','Adelaidz ganzon','Yes','','53','','09-02-2020'),(51,'Rogelio Ompod','12','38','Carolina Castro','Yes','','53','','09-02-2020'),(52,'Rogelio Ompod','12','34','Dianna Dida','Yes','','53','','09-02-2020'),(53,'Rogelio Ompod','12','33','Diceiry Ann Real','Yes','','53','','09-02-2020'),(54,'Rogelio Ompod','12','23','Gaudiosa Bajala','Yes','','53','','09-02-2020'),(55,'Rogelio Ompod','12','42','Lottie Fuentes','Yes','','53','','09-02-2020'),(56,'Rogelio Ompod','12','35','Malou Balaod','Yes','','53','','09-02-2020'),(57,'Rogelio Ompod','12','24','Marilyn Duran','Yes','','53','','09-02-2020'),(58,'Rogelio Ompod','12','41','Marissa Sanchez','Yes','','53','','09-02-2020'),(59,'Rogelio Ompod','12','29','Mary Ann Bulaloc','Yes','','53','','09-02-2020'),(60,'Rogelio Ompod','12','25','Mary Villas','Yes','','53','','09-02-2020'),(61,'Rogelio Ompod','12','13','Ryan Duran','Yes','','42','','11-02-2020'),(62,'Rogelio Ompod','12','40','Adelaidz ganzon','Yes','','42','','11-02-2020'),(63,'Rogelio Ompod','12','38','Carolina Castro','Yes','','42','','11-02-2020'),(64,'Rogelio Ompod','12','39','evelyn manlapaz','Yes','','42','','11-02-2020'),(65,'Rogelio Ompod','12','35','Malou Balaod','Yes','','56','','21-02-2020'),(84,'Rogelio Ompod','12','24','Marilyn Duran','Yes','','56','','21-02-2020'),(83,'Rogelio Ompod','12','36','Dolley Casas','Yes','','56','','21-02-2020'),(82,'Rogelio Ompod','12','35','Malou Balaod','Yes','','56','','21-02-2020'),(81,'Rogelio Ompod','12','','','Yes','','56','','21-02-2020'),(80,'Rogelio Ompod','12','31','Alma Cabal','Yes','','56','','21-02-2020'),(72,'Rogelio Ompod','12','26','Yula Arsenio','Yes','','56','','21-02-2020'),(73,'Rogelio Ompod','12','35','Malou Balaod','Yes','','57','','21-02-2020'),(79,'Rogelio Ompod','12','23','Gaudiosa Bajala','Yes','','56','','21-02-2020');
/*!40000 ALTER TABLE `ecoboy_claimed_garbage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garbageprice`
--

DROP TABLE IF EXISTS `garbageprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garbageprice` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_value` double(10,2) NOT NULL,
  `price_status` enum('0','1') NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garbageprice`
--

LOCK TABLES `garbageprice` WRITE;
/*!40000 ALTER TABLE `garbageprice` DISABLE KEYS */;
INSERT INTO `garbageprice` VALUES (1,3.00,'1','09-09-2019'),(2,1.00,'0','09-09-2019'),(3,2.00,'0','09-09-2019'),(4,4.00,'0','09-09-2019'),(5,5.00,'0','09-09-2019'),(6,6.00,'0','09-09-2019'),(7,9.00,'0','25-10-2019'),(8,5.00,'0','21-02-2020');
/*!40000 ALTER TABLE `garbageprice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garbagetruck`
--

DROP TABLE IF EXISTS `garbagetruck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garbagetruck` (
  `garbage_truck_id` int(11) NOT NULL AUTO_INCREMENT,
  `Garbage_truck_weight` varchar(20) NOT NULL,
  `Plate_number` varchar(30) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`garbage_truck_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garbagetruck`
--

LOCK TABLES `garbagetruck` WRITE;
/*!40000 ALTER TABLE `garbagetruck` DISABLE KEYS */;
INSERT INTO `garbagetruck` VALUES (1,'2000','P#1001','2019-10-28'),(2,'2050','P#1002','2019-10-28'),(3,'1900','P#1003','2019-10-30');
/*!40000 ALTER TABLE `garbagetruck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marker`
--

DROP TABLE IF EXISTS `marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marker` (
  `marker_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `img` varchar(20) NOT NULL,
  `lat` varchar(60) NOT NULL,
  `lng` varchar(60) NOT NULL,
  `status` varchar(3) NOT NULL,
  PRIMARY KEY (`marker_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marker`
--

LOCK TABLES `marker` WRITE;
/*!40000 ALTER TABLE `marker` DISABLE KEYS */;
INSERT INTO `marker` VALUES (1,'Jojie Avergonzado','Koala.jpg','7.045549','125.498328','yes'),(2,'James Aversulo','user.png','7.046166','125.499358','yes'),(3,'Fumi Yam','user.png','7.045554','125.499841','yes'),(4,'isa pa','user.png','7.0464','125.500796','yes');
/*!40000 ALTER TABLE `marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `rateid` int(11) NOT NULL AUTO_INCREMENT,
  `ecoboyname` varchar(50) DEFAULT NULL,
  `score` varchar(10) DEFAULT NULL,
  `residentname` varchar(50) DEFAULT NULL,
  `ecoboyid` varchar(5) DEFAULT NULL,
  `resid` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`rateid`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (18,'Rogelio Ompod','5','Ryan Duran','12','13'),(19,'sdf sdf','4','ACT PBA','18','13'),(20,'derick dayrit','4','ACT PBA','20','13'),(21,'sdf sdf','4','rrr rrr','18','21'),(22,'good Jv','5','rrr rrr','12','21'),(23,'Rogelio Ompod','3','Malou Balaod','12','35'),(24,'Rogelio Ompod','3','Gaudiosa Bajala','12','23'),(25,'Rogelio Ompod','4','Alma Cabal','12','31');
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_bin`
--

DROP TABLE IF EXISTS `report_bin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_bin` (
  `bin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `res_id` varchar(5) NOT NULL,
  `num_bin` int(5) NOT NULL,
  `purok` varchar(50) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  `confirmation` enum('0','1') NOT NULL,
  `request` enum('0','1') NOT NULL,
  PRIMARY KEY (`bin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_bin`
--

LOCK TABLES `report_bin` WRITE;
/*!40000 ALTER TABLE `report_bin` DISABLE KEYS */;
INSERT INTO `report_bin` VALUES (1,'ACT PBA','13',2,'RV3 8-A','2019-12-11','1','1'),(2,'ACT PBA','13',5,'3','2019-12-18','1','1'),(3,'ACT PBA','13',3,'RV3 7-B','2020-01-07','1','1'),(4,'Malou Balaod','35',2,'RV3 7-B','2020-02-21','1','1'),(5,'Malou Balaod','35',2,'RV3 7-B','2020-02-21','0','0');
/*!40000 ALTER TABLE `report_bin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resident`
--

DROP TABLE IF EXISTS `resident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Brgy_ID` varchar(15) NOT NULL,
  `Precinct_No` varchar(10) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `suffix` varchar(5) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `capability` varchar(25) DEFAULT NULL,
  `assistedby` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` varchar(3) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` int(5) NOT NULL,
  `province` varchar(50) NOT NULL,
  `citymuni` varchar(50) NOT NULL,
  `barangay` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `noofyears` varchar(50) NOT NULL,
  `noofmonths` varchar(50) NOT NULL,
  `numofyears` varchar(50) NOT NULL,
  `citizenship` varchar(25) NOT NULL,
  `citizenshipby` varchar(25) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `cmuni` varchar(50) NOT NULL,
  `cprovince` varchar(50) NOT NULL,
  `don` varchar(20) NOT NULL,
  `cnoa` varchar(25) NOT NULL,
  `rdon` varchar(20) NOT NULL,
  `cnooa` varchar(25) NOT NULL,
  `occupation` varchar(25) NOT NULL,
  `tin` varchar(15) NOT NULL,
  `civilstatus` varchar(20) NOT NULL,
  `spouse` varchar(25) NOT NULL,
  `flname` varchar(50) NOT NULL,
  `ffname` varchar(50) NOT NULL,
  `fmname` varchar(50) NOT NULL,
  `mlname` varchar(50) NOT NULL,
  `mfname` varchar(50) NOT NULL,
  `mmname` varchar(50) NOT NULL,
  `hotf` varchar(50) NOT NULL,
  `hotfname` varchar(50) DEFAULT NULL,
  `family_verification` varchar(5) DEFAULT NULL,
  `names` varchar(500) NOT NULL,
  `Purok_No` varchar(15) NOT NULL,
  `purokleader` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `re_pass` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `cat` varchar(50) DEFAULT NULL,
  `photo` varchar(100) NOT NULL,
  `position` varchar(20) DEFAULT NULL,
  `rate` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resident`
--

LOCK TABLES `resident` WRITE;
/*!40000 ALTER TABLE `resident` DISABLE KEYS */;
INSERT INTO `resident` VALUES (9,'19-000','0000-0','Mantos','Jimmy','S','','Jims','','','Male','50','52',50,'province','city','barangay','house','6','6','6','Filipino','','1970-05-12','','','0000-00-00','','0000-00-00','','none','154_282_264',' Single','','lastname','firstname','middlename','lastname','firstname','middlename','Yes','9','head','add,adda,addaa','10','Vicentita Ocay','kagawad@gmail.com','kag','wer','active','info','87184758_614068436052210_5285909950193729536_n.jpg','Kagawad',NULL),(11,'asd','adsf','Liparanon','Ricky','E','','we','','','Female','52','4',3,'asdf','asdf','adsf','asdf','4','3','2','as','','1968-02-06','','','0000-00-00','','0000-00-00','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','','10','','','10','Vicentita Ocay','cap@gmail.com','cap','ty','active',NULL,'78289059_1024844617849004_325521876375830528_n.jpg','Captain',NULL),(12,'1545','452','Ompod','Rogelio','Amad','Sr.','Elio','','','Female','65','4',3,'asdf','asdf','adsf','asdf','4','3','2','as','','1955-05-08','','','2019-05-01','','2019-05-15','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','','10','','','10','Vicentita Ocay','eco@gmail.com','eco','jv','active',NULL,'78419981_2581280135451347_1544538392182652928_n.jpg','Ecoboy','3'),(13,'7895','658','Duran','Ryan','Ayuda','Jr.','Ryry','','','Male','23','4',3,'asdf','asdf','adsf','asdf','4','3','2','as','','1997-03-03','','','2019-05-01','','2019-05-15','','asdf','34344',' Single','','Duran','Roel','Monjes','Duran','Marilyn','Ayuda','No','9','Yes','','6','Ludivina Olaquer','res@gmail.com','res','res','active',NULL,'75210225_2847008438684321_7417487405856325632_o.jpg','',NULL),(14,'7895','658','wkol','HHHH','esx','','','','','Female','','4',3,'asdf','asdf','adsf','asdf','4','3','2','as','','2019-05-08','','','2019-05-01','','2019-05-15','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','No','9','Yes','','10','Vicentita Ocay','prk@gmail.com','prk','dec','active',NULL,'user.png','Purok Leader',NULL),(15,'19-000','000-0','asdf','sdfa','ss','Jr.','jie','','','Male','8','43',23,'No Selection','Davao City','sd','12 LOWER LUBOGAN','4','2','2','Philippines','ByBirth','2011-07-06','Davao City','No Selection','','','','','wala lagi','000-000-000',' Single','','dfs','asdf','asdf','asdf','asdf','asdf','Yes','15','head','dssd','RV3 8-A','Emma Castillano','jomari@gmail.com','password','password','active',NULL,'user.png','',NULL),(16,'19-0001','0000-1','o','s','d','Jr.','d','','','Male','49','3',3,'No Selection','Davao City','j','12 LOWER LUBOGAN','2','1','2','Philippines','','','','','','','','','','',' Single','','er','wer','wer','wer','wer','we','No','9','Yes','','2','Jessa Mutia','sample@gmail.com','password','password','active',NULL,'user.png','',NULL),(17,'12321312','12321312','sad','asfdafs','asfas','Jr.','asdas','Person with Disabilty','asdasd','Male','20','123',123,'asdas','asdas','asdas','asdasdas','123','12','12','asdasdsa','ByBirth','1999-12-12','sadasd','asdas','','','','','asdas123','12312213',' Single','sadsa','asdas','asdas','asdasd','asdas','asdas','asdas','Yes','',NULL,'','RV3 8-B','Virginia Avelino','jasongaleon1996@gmail.com','jgaleon1996','jgaleon1996','decline',NULL,'user.png','',NULL),(18,'13-4545','15478-8','Llamido','Cherry','Che','','','Illiterate','sadd','Female','22','112',50,'sdf','dsf','sdf','12','21','8','21','filipino','ByBirth','1998-01-17','dsaf','sdf','1998-01-17','23231','','','sdf','154-548-510','Married','sadsdf','sdf','dfd','cdfg','ewrer','werw','cdfgdf','Yes','18','head','erwe','5','Edwin Gallera','cherryllamido10@gmail.com','cherry123','cherry123','active',NULL,'70766343_2363153910446565_4466514753728020480_n.jpg','',NULL),(19,'123123121231','123123','duran','jomari','qwe','Jr.','acasa','Illiterate','asdasd','Male','807','1212',123,'sadasd','asdas','asdas','asdasd','12','122','12','asd','ByBirth','1212-12-12','asdasd','asdas','','','','','asdasd','11233','Married','asdsad','asdasd','asdasd','saddas','asdas','dasdas','dasd','Yes','19','head','','RV3 8-B','Virginia Avelino','dcjomari@gmail.com','jomari1234','jomari1234','active',NULL,'user.png','',NULL),(20,'12321312','123123','dayrit','derick','duran','Jr.','asd','Illiterate','fasfdaf','Male','23','12',32,'asdsa','asdas','asdas','das','12','12','123','asdas','ByBirth','1996-12-12','asdas','sadfas','','','','','asdasd','12312312',' Single','asd','asdasd','asdsad','sadasd','asdsa','asdas','asd','Yes','20','head','','RV3 8-B','Virginia Avelino','derickdayrit1993@gmail.com','012345678','012345678','active',NULL,'user.png','',NULL),(21,'12414131','213123123','rrr','rrr','rrr','Jr.','rrrr','Illiterate','12312asd','Male','0','2131',213123,'asdsad','213213asdasdasd','asdsadsa','dadasd','12','12','12','fdfdasd','ByBirth','2019-10-19','sad','asd','','','','','asdsad','12312312321',' Single','123213','adas','sadsa','dasdas','dasdas','dasdasd','asdasdasdasd','Yes','21','head','rrrrrrr','RV3 8-B','Virginia Avelino','duranryan3397@gmail.com','012345678','012345678','active',NULL,'user.png','',NULL),(22,'19-0003','2890-A','Dela Cruz','Jomari','Fuentes','','','','','Male','24','53',60,'Davao','awa','asdasd','asdasd','24','24','24','Filipino','ByBirth','1995-05-07','asda','asd','','','','','asdadsad','111-222-333',' Single','','adasd','asdsad','sadasd','ada','dsads','sadas','Yes','22','head','AdrianDelaCruz','9','Thelma Aninon','im.moron@yahoo.com','12345678','12345678','active',NULL,'user.png','',NULL),(23,'12312','3123213','Bajala','Gaudiosa','S','','asd','Illiterate','adad','Female','1','123',123,'ada','asdasd','asdas','adadsa','12','12','12','ada','ByBirth','2019-05-03','aada','asd','','','','','ada','123',' Single','adasd','ada','asdasd','asas','dasd','ada','dasdad','Yes','23','head','adsa','RV3 7-B','Marlyn Duran','gaudiosabajala123@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(24,'123123','123123','Duran','Marilyn','Ayuda','','adasd','Illiterate','asdasd','Female','0','123',123,'adsasd','asdas','dasdas','asdasd','12','12','12','adada','ByBirth','2020-06-26','asdasd','adasd','','','','','asd','123','Married','asdad','ada','dad','asdasd','asdas','dad','asdad','Yes','24','head','ads','RV3 7-B','Marlyn Duran','marilynduran456@gmail.com','12345678','12345678','active',NULL,'aw.jpg','',NULL),(25,'123','','Villas','Mary','asd','','asd','Illiterate','adad','Female','0','123',123,'ada','asda','adasd','asda','13','12','12','asd','ByBirth','2020-02-13','ad','asd','','','','','ad','123','Married','ad','ad','ad','adads','asd','aa','sdas','Yes','25','head','asd','RV3 7-B','Marlyn Duran','marygracevillas123@gmail.com','12345678','12345678','active',NULL,'84835966_4351381728209074_7578584615447691264_o.jpg','',NULL),(26,'12312','123','Arsenio','Yula','ads','','ad','Illiterate','asd','Female','0','123',123,'asd','asda','asd','asdas','1','1','1','ad','ByBirth','2020-01-29','asd','asd','','','','','213','213',' Single','ad','ad','asd','asd','adsa','dsasd','asd','Yes','26','head','ad','RV3 7-B','Marlyn Duran','yulaarsenio123@gmail.com','12345678','12345657','active',NULL,'user.png','',NULL),(27,'234234','23434','Orce','Nene','edfs','','neneng b','','','Female','33','5',54,'sdf','asdf','asdf','sadf','44','5','44','sdf','ByBirth','1987-02-03','sdfs','sdfasdf','','','','','sdfa','21312','Married','sdrtfsd','adfsd','sadfwe','werert','wersdf','dfgfg','ssdfs','Yes','27','head','werwerwsd','RV3 7-B','Marlyn Duran','orce.nene213@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(28,'213-1231','2143-123','Parba','Ramon','','','Mon','','','Male','32','5',54,'sd','asdsd','wqre','sdfsdf','45','2','45','Filipino','ByBirth','1988-08-12','asdas','fdfgdf','','','','','dfsgdsfg','2342434','Married','erwersfsdf','sadf','rdtewr','dfsgdfsg','sadf','qwersdfs','dsfgsdg','Yes','28','head','wereee','RV3 7-B','Marlyn Duran','evelynjayectin15@gmail.com','12345678','12345678','active','','87077590_196877498038632_5519030793516613632_n.jpg','Kagawad',NULL),(29,'213-0123','2324-234','Bulaloc','Mary Ann','dfdsa','','meme','','','Female','32','5',56,'gfsd','dsfgsdg','dsfg','sdfgsdgf','35','4','35','Filipino','ByBirth','1988-02-03','dsfa','sdfsd','','','','','sdfasf','321-123','Married','dsfsdfa','terw','wers','sdfsdf','terw','sdfasf','sdfasd','Yes','29','head','werwea','RV3 7-B','Marlyn Duran','bulalocmaryann16@gmail.com','12345678','12345678','active',NULL,'84061406_1040519306322262_2164621327720251392_n.jpg','',NULL),(30,'123-2131','323234-342','Galleon','Jocelyn','Busano','','Jocel','','','Female','32','5',54,'sdf','sdfs','sdf','asdf','45','7','45','Filipino','ByBirth','1988-05-12','fdsf','sdfgsg','','','','','ewrwe','2342-234-234','Married','dsfasdf','sdsad','asdg','gdsfsdf','sdsad','ewrw','dgrew','Yes','30','head','ewrerdf','RV3 7-B','Marlyn Duran','galleon.jocelyn17@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(31,'19-12378','987-11','Cabal','Alma','','','Alms','Illiterate','','Female','25','54',56,'Davao Del Sur','Davao','Baliok','20','50','50','','Filipino','ByBirth','1995-12-11','DAvao','Davao Del Sur','','','','','Housewife ','321-555-999',' Married','','Cabal','Jay','','Cabal','Elma','','Yes','31','head','','RV3 7-B','Marlyn Duran','almacabal123@gmail.com','12345678','12345678','active',NULL,'alma.jpg','',NULL),(32,'19-12344','','Ocay','Vicustita','Sato','','Vics','Illiterate','','Female','24','56',55,'Davao Del Sur','Davao','Baliok','16','12','12','3','Filipino','ByBirth','1996-12-31','Davao','Davao Del sur','','','','','Housewife ','111-222-333',' Married','','Ocay','Jayde','','Ocay','Elma','','Yes','32','head','','RV3 7-B','Marlyn Duran','vicustitaocay123@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(33,'19-43242','','Real','Diceiry Ann','','','Ann','Illiterate','','Female','26','45',54,'Davao Del Sur','Davao','Baliok','34','10','2','21','Filipino','ByBirth','1994-12-31','Davao','Davao Del Sur','','','','','Housewife ','999-666-210',' Married','','Real','Obama','','Real','Estate','','Yes','33','head','','RV3 7-B','Marlyn Duran','diceiryannreal123@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(34,'121312','','Dida','Dianna','','Sr.','Dian','Illiterate','','Female','29','45',45,'Davao Del Sur','Davao','Baliok','30','21','22','32','Filipino','ByBirth','1991-12-23','','','','','','','Housewife ','123123',' Single','','Dida','elsar','','Dida','jenny','','Yes','34','head',',','RV3 7-B','Marlyn Duran','didadianna123@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(35,'1943234','','Balaod','Malou','','','Mals','Illiterate','','Female','25','45',50,'Davao del sur','davao','baliok','22','23','32','23','Filipino','ByBirth','1995-12-21','davao ','davao del sur','','','','','Housewife ','999-666-222',' Married','','balaod','mars','','balaod','jed','','Yes','35','head','','RV3 7-B','Marlyn Duran','balaodmalou123@gmail.com','12345678','12345678','active',NULL,'61207122_2419611431423345_8697060198872776704_o.jpg','',NULL),(36,'194332','','Casas','Dolley','Tan','','Doll','Illiterate','','Female','29','55',65,'davao del sur','davao','baliok','18','3','34','43','Filipino','ByBirth','1991-12-31','davao ','davao del sur','','','','','Housewife ','221-334-221',' Married','','casas','jandi','','casas ','marsie','','Yes','36','head','','RV3 7-B','Marlyn Duran','dollycasas27@gmail.com','12345678','12345678','active',NULL,'doplly.jpg','',NULL),(37,'193242','','Delinda','Maria','','','Ars','','','Female','26','65',56,'davao del sur','davao','baliok','24','23','32','13','Filipino','ByBirth','1994-12-31','','','','','','','Housewife ','246-888-123',' Married','','Delinda','mario','','delinda','estes','','Yes','37','head','','RV3 7-B','Marlyn Duran','mariaadelinda123@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(38,'18-2321','','Castro','Carolina','','','CArs','','','Female','25','56',45,'davao del sur','davao','baliok','28','12','21','22','Filipino','ByBirth','1995-12-31','davao','','','','','','Housewife ','122-654-857',' Married','','castro','jude','','castro','jade','','Yes','38','head','','6','Ludivina Olaquer','cc1911824@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(39,'17-32423','','manlapaz','evelyn','','','eva','Illiterate','','Female','25','45',56,'davao del sur','davao','baliok','26','12','12','23','Filipino','ByBirth','1995-12-21','Davao','','','','','','Housewife ','112-655-474',' Married','','manlapaz','jake','','manlapaz','maria','','Yes','39','head','','6','Ludivina Olaquer','evelynmanlapaz123@gmail.com','12345678','12345678','active',NULL,'37114451_467187887038342_5502411808406765568_n.jpg','',NULL),(40,'19-2342213','','Bandal','Presencia','Roxas','','Encia','Illiterate','','Female','39','55',55,'davao del sur ','davao','baliok','32','12','21','30','Filipino','ByBirth','1981-01-02','davao','','','','','','Housewife ','999-666-211',' Married','','ganzon','jessie','','ganzon','malou','','Yes','40','head','','6','Ludivina Olaquer','adelaidz1234@gmail.com','12345678','12345678','active','','87015934_198570508168861_8129021254666551296_n.jpg','Kagawad',NULL),(41,'1234','','Sanchez','Marissa','','','Mars','','','Female','50','5',55,'Davao del Sur','Davao City','Baliok','38','50','50','50','Filipino','ByBirth','1970-02-20','','','','','','','dasda','123-213-123','Married','ASDas','asd','aasd','asdas','aw','sdsa','d','Yes','41','head','asdadasda','6','Ludivina Olaquer','sanchezmarissa136@gmail.com','12345678','12345678','active',NULL,'60114419_2352938168104177_7263536631003480064_o.jpg','',NULL),(42,'123123','','Fuentes','Lottie','','','Lotlot','','','Female','45','5',58,'Davao del Sur','Davao City','Baliok','39','45','45','45','Filipino','ByBirth','1975-05-06','Davao','Davao del Sur','','','','','Housewife','','Married','asdasda adsad','adasda','adsda','aaf','dads','as','fas','Yes','42','head','asdafstr','6','Ludivina Olaquer','sefuenteslottie20@gmail.com','12345678','12345678','active',NULL,'user.png','',NULL),(43,'10-9272','08272','Medina','Ramon','Hsjs','','ramon','','','Male','32','5',56,'Bznsnz','Bsjssj','Bsnsaj','Bshsjs','45','5','45','Bznzjzz','ByBirth','1988-02-13','Hsnshs','Hsbsjz','','','','','Hsnsnz','09282','Married','Hsbznzjz','Bznsba','Bznzz','Hzjz','Sbzbzj','BBJ','Hshsh','Yes','',NULL,'Bznzns','11-A','Beatriz Penones','m.ramon1332@gmail.com','12345678','12345678','inactive',NULL,'user.png','',NULL);
/*!40000 ALTER TABLE `resident` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resident_responded`
--

DROP TABLE IF EXISTS `resident_responded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resident_responded` (
  `respond_id` int(11) NOT NULL AUTO_INCREMENT,
  `sked_id` varchar(5) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `rest_id` varchar(5) NOT NULL,
  `res_name` varchar(50) NOT NULL,
  `garbage` varchar(5) NOT NULL,
  `reason` text,
  `week` varchar(10) DEFAULT NULL,
  `Collectiondate` varchar(20) DEFAULT NULL,
  `CollDateStart` varchar(20) NOT NULL,
  `CollDateEnd` varchar(20) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `img` text NOT NULL,
  `lat` text NOT NULL,
  `lng` text NOT NULL,
  `garbage_type` varchar(20) NOT NULL,
  `if_collected` varchar(5) NOT NULL,
  `date_collected` varchar(20) DEFAULT NULL,
  `seen` varchar(1) NOT NULL,
  PRIMARY KEY (`respond_id`)
) ENGINE=MyISAM AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resident_responded`
--

LOCK TABLES `resident_responded` WRITE;
/*!40000 ALTER TABLE `resident_responded` DISABLE KEYS */;
INSERT INTO `resident_responded` VALUES (47,'26','12','13','ACT PBA','Yes',NULL,'','2019-12-09','2019-12-09 06:00','2019-12-09 15:00','2019-12-08 07:51:19','user.png','','','rw','Yes','10-12-2019','1'),(48,'29','12','13','ACT PBA','Yes',NULL,'','2019-12-12','2019-12-12 05:00','2019-12-12 16:00','2019-12-08 23:39:55','user.png','','','rw','Yes','10-12-2019','1'),(49,'28','12','13','ACT PBA','Yes',NULL,'','2019-12-11','2019-12-11 06:30','2019-12-11 16:00','2019-12-11 04:50:54','user.png','','','rw','','',''),(50,'30','12','13','ACT PBA','Yes',NULL,'','2019-12-13','2019-12-13 05:00','2019-12-13 15:00','2019-12-11 06:47:04','user.png','','','rw','','',''),(51,'31','12','22','Jomari Dela Cruz','Yes',NULL,'','2019-12-14','2019-12-14 06:00','2019-12-14 16:00','2019-12-14 00:56:43','user.png','','','rw','','',''),(52,'32','12','13','ACT PBA','Yes',NULL,'','2019-12-17','2019-12-17 10:00','2019-12-17 17:00','2019-12-18 01:35:21','user.png','','','rw','Yes','18-12-2019','1'),(53,'34','12','13','ACT PBA','Yes',NULL,'','2019-12-19','2019-12-19 05:00','2019-12-19 18:00','2019-12-19 04:57:45','user.png','','','sw','','',''),(54,'34','12','22','Jomari Dela Cruz','Yes',NULL,'','2019-12-19','2019-12-19 05:00','2019-12-19 18:00','2019-12-19 05:00:26','user.png','','','rw','','',''),(55,'37','12','13','ACT PBA','Yes',NULL,'','2019-12-27','2019-12-27 05:00','2019-12-27 16:00','2019-12-27 09:23:09','user.png','','','sw','','',''),(56,'38','12','22','Jomari Dela Cruz','Yes',NULL,'','2019-12-28','2019-12-28 08:00','2019-12-28 18:00','2019-12-28 03:48:05','user.png','7.0057734','125.5054046','rw','Yes','28-12-2019','1'),(57,'40','12','13','ACT PBA','Yes',NULL,'','2020-01-07','2020-01-07 06:00','2020-01-07 20:00','2020-01-07 08:53:20','user.png','7.0145363','125.4995862','rw','Yes','07-01-2020','1'),(58,'40','12','22','Jomari Dela Cruz','Yes',NULL,'','2020-01-07','2020-01-07 06:00','2020-01-07 20:00','2020-01-07 08:58:46','user.png','7.0143841','125.4996537','sw','Yes','07-01-2020','1'),(59,'41','12','22','Jomari Dela Cruz','Yes',NULL,'','2020-01-09','2020-01-09 06:00','2020-01-09 19:00','2020-01-09 05:10:03','user.png','7.0143841','125.4996537','rw','','',''),(60,'43','12','13','ACT PBA','Yes',NULL,'','2020-01-17','2020-01-17 00:12','2020-01-17 15:00','2020-01-17 11:05:24','user.png','7.0746112','125.5596032','rw','','',''),(61,'44','12','13','ACT PBA','Yes',NULL,'','2020-01-25','2020-01-25 13:00','2020-01-25 17:00','2020-01-24 11:12:17','bg.png','7.0868991999999995','125.5981056','mw','','',''),(62,'45','12','13','ACT PBA','Yes',NULL,'','2020-01-24','2020-01-24 02:02','2020-01-24 13:02','2020-01-24 11:16:10','bg.png','7.0868991999999995','125.5981056','rw','','',''),(63,'47','12','13','ACT PBA','Yes',NULL,'','2020-01-31','2020-01-31 09:00','2020-01-31 14:00','2020-01-24 11:19:54','bg.png','','','sw','Yes','24-01-2020','1'),(64,'48','12','13','ACT PBA','Yes',NULL,'','2020-02-03','2020-02-03 09:00','2020-02-03 15:00','2020-01-24 11:26:32','bg.png','7.0197846','125.4965061','sw','','',''),(65,'48','12','22','Jomari Dela Cruz','Yes',NULL,'','2020-02-03','2020-02-03 09:00','2020-02-03 15:00','2020-01-24 11:28:56','user.png','7.0197846','125.4965061','sw','','',''),(66,'49','12','22','Jomari Dela Cruz','Yes',NULL,'','2020-02-05','2020-02-05 08:00','2020-02-05 17:00','2020-01-24 11:32:18','user.png','7.022336','125.4995864','rw','Yes','06-02-2020','1'),(67,'50','12','13','ACT PBA','Yes',NULL,'','2020-01-27','2020-01-27 05:00','2020-01-27 18:00','2020-01-27 09:26:45','75210225_2847008438684321_7417487405856325632_o.jpg','7.0143841','125.4996537','sw','Yes','27-01-2020','1'),(68,'49','12','13','Ryan Duran','Yes',NULL,'','2020-02-05','2020-02-05 08:00','2020-02-05 17:00','2020-02-06 00:59:55','75210225_2847008438684321_7417487405856325632_o.jpg','7.3041622','126.08934059999999','rw','Yes','06-02-2020','1'),(69,'51','12','13','Ryan Duran','Yes',NULL,'','2020-02-07','2020-02-07 09:00','2020-02-07 22:00','2020-02-07 13:35:39','75210225_2847008438684321_7417487405856325632_o.jpg','7.0041851','125.5043639','rw','Yes','07-02-2020','1'),(70,'52','12','23','Gaudiosa Bajala','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:19:00','user.png','7.0452581','125.5021781','sw','Yes','08-02-2020','0'),(71,'52','12','26','Yula Arsenio','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:21:25','user.png','7.0451409','125.5026781','mw','Yes','08-02-2020','0'),(72,'52','12','29','Mary Ann Bulaloc','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:24:20','user.png','7.0451639','125.5026831','sw','Yes','08-02-2020','0'),(73,'52','12','24','Marilyn Duran','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:25:34','user.png','7.045229','125.5023492','mw','Yes','08-02-2020','0'),(74,'52','12','30','Jocelyn Galleon','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:27:05','user.png','7.045229','125.5023492','sw','Yes','08-02-2020','0'),(75,'52','12','27','Nene Orce','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:28:22','user.png','7.045627','125.5026728','mw','Yes','08-02-2020','0'),(76,'52','12','25','Mary Villas','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:29:22','user.png','7.0456722','125.5028704','sw','Yes','08-02-2020','1'),(77,'52','12','28','sdf sdfaa','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:30:38','user.png','7.0460962','125.5028717','mw','Yes','08-02-2020','1'),(78,'52','12','35','Malou Balaod','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:32:04','user.png','7.0461283','125.502555','sw','Yes','08-02-2020','0'),(79,'52','12','36','Dolley Casas','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:33:29','user.png','7.0461283','125.502555','mw','Yes','08-02-2020','0'),(80,'52','12','31','Alma Cabal','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:36:36','user.png','7.0463348','125.502257','rw','Yes','08-02-2020','1'),(81,'52','12','37','Maria Delinda','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:39:34','user.png','7.0456249','125.5020123','sw','Yes','08-02-2020','0'),(82,'52','12','39','evelyn manlapaz','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:40:46','user.png','7.0520945','125.507458','rw','Yes','08-02-2020','0'),(83,'52','12','32','Vicustita Ocay','Yes',NULL,'','2020-02-08','2020-02-08 06:00','2020-02-08 17:00','2020-02-08 05:43:27','user.png','7.0457888','125.5008986','sw','Yes','08-02-2020','0'),(84,'53','12','25','Mary Villas','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 07:59:53','user.png','7.0457255','125.5026287','rw','Yes','09-02-2020','1'),(85,'53','12','38','Carolina Castro','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:01:22','user.png','7.0457255','125.5026287','sw','Yes','09-02-2020','1'),(86,'53','12','34','Dianna Dida','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:02:09','user.png','7.0457255','125.5026287','mw','Yes','09-02-2020','0'),(87,'53','12','42','Lottie Fuentes','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:03:02','user.png','7.0457255','125.5026287','rw','Yes','09-02-2020','0'),(88,'53','12','40','Adelaidz ganzon','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:23:36','user.png','7.0474659','125.5023245','sw','Yes','09-02-2020','1'),(89,'53','12','41','Marissa Sanchez','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:26:20','user.png','7.0449896','125.5026952','mw','Yes','09-02-2020','0'),(90,'53','12','33','Diceiry Ann Real','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:29:10','user.png','7.0440902','125.5029636','rw','Yes','09-02-2020','0'),(91,'53','12','24','Marilyn Duran','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:31:26','user.png','7.0441085','125.5029','mw','Yes','09-02-2020','0'),(92,'53','12','23','Gaudiosa Bajala','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:33:40','user.png','7.0434976','125.5032552','mw','Yes','09-02-2020','0'),(93,'53','12','35','Malou Balaod','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:35:48','user.png','7.0430158','125.5037663','sw','Yes','09-02-2020','0'),(94,'53','12','29','Mary Ann Bulaloc','Yes',NULL,'','2020-02-09','2020-02-09 06:00','2020-02-09 18:00','2020-02-09 08:37:47','user.png','7.0427564','125.5036982','sw','Yes','09-02-2020','0'),(95,'42','12','13','Ryan Duran','Yes',NULL,'','2020-02-27','2020-02-27 10:31','2020-02-27 15:31','2020-02-11 14:29:18','75210225_2847008438684321_7417487405856325632_o.jpg','12.879721','121.77401700000001','rw','Yes','11-02-2020','1'),(96,'42','12','42','Lottie Fuentes','Yes',NULL,'','2020-02-27','2020-02-27 10:31','2020-02-27 15:31','2020-02-11 14:39:03','user.png','12.879721','121.77401700000001','rw','','',''),(97,'42','12','41','Marissa Sanchez','Yes',NULL,'','2020-02-27','2020-02-27 10:31','2020-02-27 15:31','2020-02-11 14:44:01','60114419_2352938168104177_7263536631003480064_o.jpg','12.879721','121.77401700000001','sw','','',''),(98,'42','12','40','Adelaidz ganzon','Yes',NULL,'','2020-02-27','2020-02-27 10:31','2020-02-27 15:31','2020-02-11 14:44:57','user.png','12.879721','121.77401700000001','mw','Yes','11-02-2020','0'),(99,'42','12','39','evelyn manlapaz','Yes',NULL,'','2020-02-27','2020-02-27 10:31','2020-02-27 15:31','2020-02-11 14:47:14','37114451_467187887038342_5502411808406765568_n.jpg','12.879721','121.77401700000001','sw','Yes','11-02-2020','0'),(100,'42','12','38','Carolina Castro','Yes',NULL,'','2020-02-27','2020-02-27 10:31','2020-02-27 15:31','2020-02-11 14:48:42','user.png','12.879721','121.77401700000001','rw','Yes','11-02-2020','0'),(101,'56','12','35','Malou Balaod','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 02:04:50','61207122_2419611431423345_8697060198872776704_o.jpg','7.0143575','125.4996557','rw','Yes','21-02-2020','0'),(102,'56','12','26','Yula Arsenio','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 02:16:05','user.png','7.0143575','125.4996557','mw','Yes','21-02-2020','0'),(103,'57','12','35','Malou Balaod','Yes',NULL,'','2020-02-22','2020-02-22 04:00','2020-02-22 18:00','2020-02-21 02:36:42','61207122_2419611431423345_8697060198872776704_o.jpg','7.0240772','125.4891363','rw','Yes','21-02-2020','0'),(104,'56','12','23','Gaudiosa Bajala','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 02:39:21','user.png','7.0143575','125.4996557','mw','Yes','21-02-2020','0'),(105,'57','12','23','Gaudiosa Bajala','Yes',NULL,'','2020-02-22','2020-02-22 04:00','2020-02-22 18:00','2020-02-21 02:51:25','user.png','7.0240772','125.4891363','rw','','',''),(106,'56','12','31','Alma Cabal','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 03:07:05','alma.jpg','7.0240772','125.4891363','sw','Yes','21-02-2020','0'),(107,'56','12','36','Dolley Casas','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 03:12:39','doplly.jpg','7.0143575','125.4996557','rw','Yes','21-02-2020','0'),(108,'64','12','13','Ryan Duran','Yes',NULL,'','2020-02-29','2020-02-29 12:00','2020-02-29 15:58','2020-02-21 05:35:48','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','rw','','',''),(109,'65','12','13','Ryan Duran','Yes',NULL,'','2020-03-01','2020-03-01 12:00','2020-03-01 14:59','2020-02-21 05:42:07','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','rw','','',''),(110,'56','12','24','Marilyn Duran','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 05:47:31','aw.jpg','7.0142157','125.4992048','rw','Yes','21-02-2020','0'),(111,'66','12','13','Ryan Duran','Yes',NULL,'','2020-03-02','2020-03-02 12:00','2020-03-02 15:00','2020-02-21 05:49:31','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','rw','','',''),(112,'68','12','13','Ryan Duran','Yes',NULL,'','2020-03-05','2020-03-05 12:00','2020-03-05 14:58','2020-02-21 06:23:03','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','rw','','',''),(113,'69','12','13','Ryan Duran','Yes',NULL,'','2020-03-06','2020-03-06 12:00','2020-03-06 13:00','2020-02-21 06:32:43','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','sw','','',''),(114,'71','12','13','Ryan Duran','Yes',NULL,'','2020-03-01','2020-03-01 12:00','2020-03-01 13:00','2020-02-21 06:46:31','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','No garbage','','',''),(115,'73','12','13','Ryan Duran','Yes',NULL,'','2020-03-09','2020-03-09 12:12','2020-03-09 12:40','2020-02-21 07:09:29','75210225_2847008438684321_7417487405856325632_o.jpg','7.0240772','125.4891363','sw','','',''),(116,'56','12','30','Jocelyn Galleon','Yes',NULL,'','2020-02-21','2020-02-21 06:00','2020-02-21 17:00','2020-02-21 07:19:21','user.png','7.0240772','125.4891363','rw','','','');
/*!40000 ALTER TABLE `resident_responded` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `Sched_No` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `CollDateStart` varchar(50) NOT NULL,
  `CollDateEnd` varchar(50) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Purok_No` varchar(10) NOT NULL,
  `EcoboyName` varchar(100) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `status` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `week_no` varchar(10) NOT NULL,
  `days` varchar(20) NOT NULL,
  `plate_number` varchar(20) DEFAULT NULL,
  `driver` varchar(50) DEFAULT NULL,
  `weight_net_kgs` varchar(20) DEFAULT NULL,
  `current_price` double(10,2) DEFAULT NULL,
  `accomplish` varchar(5) DEFAULT NULL,
  `loading_cenro` varchar(20) DEFAULT NULL,
  `loading_date_ecoboy` varchar(20) DEFAULT NULL,
  `loading_date_cenro` varchar(20) DEFAULT NULL,
  `loading_ecoboy` varchar(20) DEFAULT NULL,
  `loading_landfill` varchar(20) DEFAULT NULL,
  `landfill_confirmation` varchar(1) DEFAULT NULL,
  `confirm_date_landfill` varchar(20) DEFAULT NULL,
  `receipt_kagawad` varchar(1) DEFAULT NULL,
  `garbage_truck_weight` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Sched_No`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (21,'TEsting','2019-11-21 13:00','2019-11-21 14:00','13:00:00','14:00:00','11-B','derick dayrit','20','inactive','Not yet review','2019-11-20 16:17:49','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'Garbage Schedule','2019-11-27 04:00','2019-11-27 17:00','04:00:00','17:00:00','RV3 7-A','derick dayrit','20','inactive','Not yet review','2019-11-26 08:58:17','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'Test jojie','2019-11-28 01:00','2019-11-28 02:00','01:00:00','02:00:00','4','derick dayrit','20','inactive','Not yet review','2019-11-27 07:08:31','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,'Garbage Collection','2019-12-08 04:00','2019-12-08 17:00','04:00:00','17:00:00','RV3 7-A','derick dayrit','20','inactive','Not yet review','2019-12-05 09:20:23','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,'Garbage collection','2020-01-08 06:00','2020-01-08 19:00','06:00:00','19:00:00','RV3 7-A','good Jv','12','inactive','Not yet review','2020-01-07 08:49:32','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,'collect','2020-01-29 09:00','2020-01-29 15:00','09:00:00','15:00:00','2','good Jv','12','denied','Not available','2020-01-24 11:17:44','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,'ae','2020-02-05 08:00','2020-02-05 17:00','08:00:00','17:00:00','4','good Jv','12','Approve','Approved','2020-02-06 01:05:23','','','P#1001','Ryan Llamido Cabahog','895',3.00,'ok','load','06-02-2020, 9:04 am','06-02-2020, 9:05 am','load','load',NULL,NULL,NULL,'2000'),(51,'Garbage Collection','2020-02-07 09:00','2020-02-07 22:00','09:00:00','22:00:00','RV3 7-B','Rogelio Ompod','12','Approve','Approved','2020-02-07 13:38:48','','','P#1001','Ryan Llamido Cabahog','900',3.00,'ok','load','07-02-2020, 9:38 pm','07-02-2020, 9:38 pm','load','load',NULL,NULL,NULL,'2000'),(55,'Ecollect','2020-03-04 08:00','2020-03-04 17:00','08:00:00','17:00:00','5','Rogelio Ompod','12','Approve','Approved','2020-02-21 02:35:24','','',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,'Garbage Collection','2020-02-21 06:00','2020-02-21 17:00','06:00:00','17:00:00','RV3 7-B','Rogelio Ompod','12','Approve','Approved','2020-02-22 07:58:11','','','P#1001','Ryan Llamido Cabahog','850',3.00,'ok','load','22-02-2020, 3:57 pm','22-02-2020, 3:57 pm','load','load',NULL,NULL,NULL,'2000');
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-22 10:07:08
