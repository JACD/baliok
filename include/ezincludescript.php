    <!-- Bootstrap core JavaScript-->

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="../../vendor/jquery/jquery.js"></script>
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Leaflet scripts -->
    <script>
var greenIcon = L.icon({
    iconUrl: '../../components/img/images/leaf-green.png',
    shadowUrl: '../../components/img/images/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});
var redIcon = L.icon({
    iconUrl: '../../components/img/images/leaf-red.png',
    shadowUrl: '../../components/img/images/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});
var orangeIcon = L.icon({
    iconUrl: '../../components/img/images/leaf-orange.png',
    shadowUrl: '../../components/img/images/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var mymap = L.map('mapid').setView([7.0464953, 125.4998873], 15);
var markers;
$(document).ready(function() {
    $.ajax({
        async: false,
        type: "GET",
        url: '../../php_function/getdatafrommarker.php',
        success: function(data) {

            if ((data) !== null) {
                markers = data;
            } else {
                L.marker([7.045016, 125.499787]).addTo(mymap)
                    .bindPopup('No resident responded.')
                    .openPopup();
            }
        }
    });
    markers = JSON.parse(markers);

    for (var i = 0; i < markers.length; ++i) {
        if (markers[i].garbage_type === 'rw') {
            if (markers[i].if_collected === '') {
                L.marker([markers[i].lat, markers[i].lng], {
                        icon: greenIcon
                    }).addTo(mymap)
                    .bindPopup("<div><img src='../../components/img/profile/" + markers[i].img +
                        "' style='width:150px;'><br/>" + markers[i].res_name +
                        "</b> <br/><input type='hidden' id='ayde' value='" + markers[i].respond_id +
                        "'/> <button class='speciallink' style='margin-top:5%; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collect</button></div>"
                    );
            } else {
                L.marker([markers[i].lat, markers[i].lng], {
                        icon: greenIcon
                    }).addTo(mymap)
                    .bindPopup("<div><img src='../../components/img/profile/" + markers[i].img +
                        "' style='width:150px;'><br/>" + markers[i].res_name +
                        "</b> <br/> <br/>  <span style='margin-top:30% !important; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collected</span></div>"
                    );
            }
        } else if (markers[i].garbage_type === 'sw') {
            if (markers[i].if_collected === '') {
                L.marker([markers[i].lat, markers[i].lng], {
                        icon: orangeIcon
                    }).addTo(mymap)
                    .bindPopup("<div><img src='../../components/img/profile/" + markers[i].img +
                        "' style='width:150px;'><br/>" + markers[i].res_name +
                        "</b> <br/><input type='hidden' id='ayde' value='" + markers[i].respond_id +
                        "'/> <button class='speciallink' style='margin-top:5%; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collect</button></div>"
                    );
            } else {
                L.marker([markers[i].lat, markers[i].lng], {
                        icon: orangeIcon
                    }).addTo(mymap)
                    .bindPopup("<div><img src='../../components/img/profile/" + markers[i].img +
                        "' style='width:150px;'><br/>" + markers[i].res_name +
                        "</b> <br/>  <br/> <span style='margin-top:5%; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collected</span></div>"
                    );
            }
        } else if (markers[i].garbage_type === 'mw') {
            if (markers[i].if_collected === '') {
                L.marker([markers[i].lat, markers[i].lng], {
                        icon: redIcon
                    }).addTo(mymap)
                    .bindPopup("<div><img src='../../components/img/profile/" + markers[i].img +
                        "' style='width:150px;'><br/>" + markers[i].res_name +
                        "</b> <br/><input type='hidden' id='ayde' value='" + markers[i].respond_id +
                        "'/> <button class='speciallink' style='margin-top:5%; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collect</button></div>"
                    );
            } else {
                L.marker([markers[i].lat, markers[i].lng], {
                        icon: redIcon
                    }).addTo(mymap)
                    .bindPopup("<div><img src='../../components/img/profile/" + markers[i].img +
                        "' style='width:150px;'><br/>" + markers[i].res_name +
                        "</b> <br/>  <br/> <span style='margin-top:5%; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collected</span></div>"
                    );
            }
        }
    }
});

mymap.on('popupopen', function() {
    $('.speciallink').click(function() {
        var ayde = document.getElementById("ayde").value;

        $.ajax({
            url: "../../php_function/collected.php",
            type: "POST",
            data: {
                id: ayde
            },
            success: function(data) {
                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Your schedule has been saved!',
                    allowOutsideClick: false
                })
                window.location.href = window.location.href;
            }
        })
    });
});

googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
    maxZoom: 30,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
}).addTo(mymap);

var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
}

mymap.on('click', onMapClick);

$('.leaflet-control-attribution').text(function(i, oldText) {
    return oldText === 'Leaflet' ? 'Barangay Baliok' : oldText;
});
window.onload = function() {
    document.getElementById("sidebarToggleTop").click();
};
    </script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../../components/js/sb-admin-2.min.js"></script>
    <!-- Page level plugins -->