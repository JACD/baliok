orig


var markers, mymap = L.map("mapid").setView([7.0464953, 125.4998873], 15);
$(document).ready(function() {
    $.ajax({
        async: !1,
        type: "GET",
        url: "../../php_function/getdatafrommarker.php",
        success: function(t) {
            markers = t
        }
    }), markers = JSON.parse(markers);
    for (var t = 0; t < markers.length; ++t) L.marker([markers[t].lat, markers[t].lng]).addTo(mymap).bindPopup("<div><img src='../../components/img/profile/" + markers[t].img + "' style='width:150px; margin-bottom: 5%;'><br/><b style='margin-top:5%; font-size: 15px;'>" + markers[t].res_name + "</b> <br/><button type='submit' id='collectid' style='margin-top:5%; background:#1cc88a; font-size: .8rem;border-radius: 10rem;padding: 5% 5% 5% 5%;'>Collect</button></div>")
}), googleHybrid = L.tileLayer("https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}", {
    maxZoom: 30,
    subdomains: ["mt0", "mt1", "mt2", "mt3"]
}).addTo(mymap);
var popup = L.popup();

function onMapClick(t) {
    popup.setLatLng(t.latlng).setContent("You clicked the map at " + t.latlng.toString()).openOn(mymap)
}
mymap.on("click", onMapClick);
var myLines = [{
        type: "Polygon",
        coordinates: [
            [
                [125.5053663253784, 7.048402209103975],
                [125.50441145896912, 7.049988722118666],
                [125.49607515335083, 7.057165229880231],
                [125.49192309379578, 7.0520224321982345],
                [125.49508810043334, 7.05154328664842],
                [125.49519002437592, 7.049408421008491],
                [125.49299597740172, 7.048822795298085],
                [125.4919284582138, 7.0466293542420875],
                [125.49054980278015, 7.046645325937797],
                [125.49050152301787, 7.044856492599409],
                [125.49337685108185, 7.0447340424458496],
                [125.49298524856567, 7.042790807419691],
                [125.5003237724304, 7.037189985853799],
                [125.50086021423341, 7.040804972528961],
                [125.50582766532898, 7.035752501737085],
                [125.50712049007416, 7.037812894255181],
                [125.51000118255614, 7.03723257790787],
                [125.50325274467468, 7.046011781586565],
                [125.5053663253784, 7.048402209103975]
            ]
        ]
    }],
    myStyle = {
        color: "#ff7800",
        stroke: !0,
        fill: !1
    };
L.geoJSON(myLines, {
    style: myStyle
}).addTo(mymap), $(".leaflet-control-attribution").text(function(t, a) {
    return "Leaflet" === a ? "Barangay Baliok" : a
});