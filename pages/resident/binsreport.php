<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-success">Report bin</h6>
    </div>
    <div class="card-body">



        <!-- DREA E SULOD ANG CONTENT -->


        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-8 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Purok #</th>
                                <th>Number of garbage bin</th>
                                <th>Date submit</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Purok #</th>
                                <th>Number of garbage bin</th>
                                <th>Date submit</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php
					$cap = mysqli_query($db,"SELECT * FROM report_bin where res_id='".$_SESSION[session_user]."';");
                    while($row = mysqli_fetch_array($cap))
                    {   
                      ?>
                                <td>
                                    <?php echo $row['purok'];?>
                                </td>
                                <td>
                                    <?php echo $row['num_bin'];?>
                                </td>
                                <td>
                                    <?php echo $row['date_created'];?>
                                </td>
                                <td>
                                    <button class="btn btn-success btn-circle" data-toggle="modal"
                                        data-target="#editModal<?php echo $row['bin_id'];?>">
                                        <i class="fas fa-edit"> </i>
                                    </button>
                                    <a href="../../php_function/function_for_personnel.php?idremove=<?php echo $row['bin_id'];?>&click=remove"
                                        onclick="return confirm('Are you sure you want to remove this position?');"
                                        class="btn btn-danger btn-circle">
                                        <i class="fas fa-sync"> </i>
                                    </a>

                                </td>
                            </tr>

                            <?php
									}
									?>
                        </tbody>
                    </table>

                </div>
            </div>

            <!-- Donut Chart -->
            <div class="col-xl-4 col-lg-3">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Insert Here </h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="chart-area">
                            <form action="../../php_function/function_for_personnel.php" method="POST">
                                <div class="form-group">
                                    <label>Purok</label>
                                    <select class="form-control select2" id="purok" name="Purok_No">
                                        <option></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="RV3 7-A">RV3 7-A</option>
                                        <option value="RV3 7-B">RV3 7-B</option>
                                        <option value="RV3 8-A">RV3 8-A</option>
                                        <option value="RV3 8-B">RV3 8-B</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11-A">11-A</option>
                                        <option value="11-B">11-B</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Number of garbage bin</label>
                                    <input type="number" class="form-control" name="number">
                                    <input type="hidden" class="form-control" value="<?php echo date('Y-m-d');?>"
                                        name="yearcreated" required>
                                </div>
                                <div>
                                    <button type="submit" onclick="return confirm('Do you want to submit?');"
                                        name="btn-bin" class="btn btn-success btn-user btn-block">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- /.container-fluid -->
<?php
			$cap = mysqli_query($db,"SELECT * FROM brgyposition;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<!-- update Modal-->
<div class="modal fade" id="editModal<?php echo $row['pos_id'];?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update record</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../../php_function/updatepersonnel.php" method="POST">

                    <div class="form-group">
                        <label>Position</label>

                        <input type="text" class="form-control" name="position" value="<?php echo $row['position'];?>">

                    </div>
                    <div class="form-group">
                        <label> Description</label>
                        <textarea class="form-control" name="desc"
                            value="<?php echo $row['description'];?>"><?php echo $row['description'];?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Year Created</label>
                        <input type="date" class="form-control" name="year" value="<?php echo $row['date_created'];?>">
                    </div>
                    <input type="hidden" class="form-control" name="idfor" value="<?php echo $row['pos_id'];?>"
                        required>

                    <div>
                    </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" onclick="return confirm('Do you want to update?');" name="btn-post-up"
                    class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>