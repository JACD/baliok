<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-success-800">Schedule</h1>
</div>


<!-- DREA E SULOD ANG CONTENT -->

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-gray-600">Garbage Collection Schedule</h6>
        </center>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Collection Date</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Ecoboy Name</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Collection Date</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Ecoboy Name</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <?php 
			            	$result = mysqli_query($db,"SELECT * FROM schedule where status='Approve';");

                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                        <td><?php echo date("F j, Y",strtotime($row['CollDate'])); ?></td>
                        <td><?php echo $row['StartTime'];?></td>
                        <td><?php echo $row['EndTime'];?></td>
                        <td><?php echo $row['EcoboyName'];?></td>

                    </tr>

                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal" id="openModal<?php echo $row['Sched_No'];?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Do you want to decline this schedule please state the reason.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <textarea style="width: 100%; height: 100%;" placeholder="Enter here" name="reason"></textarea>
                        <input type="hidden" value="<?php echo $row['Sched_No'];?>" name="idsked" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="decline-sked" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>