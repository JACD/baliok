<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Generate report</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="gen_wednesday.php">WEDNESDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="gen_saturday.php">SATURDAY</a>
                </li>
                <li class="nav-item border-bottom-info">
                    <a class="nav-link active" href="gen_sunday.php">SUNDAY</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <form action="" method="GET">
                    <div class="form-group">
                        <label>Week</label>
                        <select class="form-control" name="week_number" required>
                            <option />
                            <option value="week1">
                                WEEK 1
                            </option>
                            <option value="week2">
                                WEEK 2
                            </option>
                            <option value="week3">
                                WEEK 3
                            </option>
                            <option value="week4">
                                WEEK 4
                            </option>
                        </select>
                    </div>
            </div>
            <div class="col-xl-3 col-lg-8">
                <div class="form-group" style="margin-top:12%;">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                </form>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Collection Date</th>
                            <th>Ecoboy</th>
                            <th>Plate number</th>
                            <th>Driver</th>
                            <th>Weight/net/kgs</th>
                            <th>validate</th>
                            <th>Preview</th>

                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Collection Date</th>
                            <th>Ecoboy</th>
                            <th>Plate number</th>
                            <th>Driver</th>
                            <th>Weight/net/kgs</th>
                            <th>validate</th>
                            <th>Preview</th>

                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <?php 
                            if(isset($_GET['week_number']))
                            { 
                                $cweek = $_GET['week_number'];
					$result = mysqli_query($db,"SELECT * FROM schedule where week_no='$cweek' and days='saturday' and status='Approve';");
                            }
                            else
                             {
					$result = mysqli_query($db,"SELECT * FROM schedule where days='saturday' and status='Approve';");
                             }
                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                            <td><?php echo date("F j, Y  "."("."l".")",strtotime($row['CollDate'])); ?></td>
                            <td><?php echo $row['EcoboyName'];?></td>
                            <td><?php echo $row['plate_number'];?></td>
                            <td><?php echo $row['driver'];?></td>
                            <td><?php echo $row['weight_net_kgs'];?></td>
                            <?php
                      if($row['plate_number']!='' && $row['driver']!='')
                      { 
                      ?>
                            <?php if($row['accomplish']=='ok') { ?>
                            <td style="color:green;"> Validated </td>
                            <?php } else { ?>
                            <td> </td>
                            <?php }?>
                            <?php } else { ?>
                            <td style="color:red;">Ecoboy not reported the plate number or the driver </td>
                            <?php } ?>
                            <?php
                      if($row['accomplish']=='ok')
                      { 
                      ?>
                            <td> <a href="../../php_function/report.php" target="_blank"
                                    class="btn btn-primary btn-circle">
                                    <i class="fas fa-file"> </i>
                                </a></td>
                            <?php } else { ?>
                            <td> </td>
                            <?php } ?>

                        </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>