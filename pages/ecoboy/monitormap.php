<?php  include('../../include/ezincludetop.php');?>


<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php  include('../../include/ezincludesidebar.php');?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include '../../include/ezincludenav.php'; ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <center>
                                <h6 class="m-0 font-weight-bold text-success">Map Responder </h6>
                            </center>
                        </div>
                        <div class="card-body">
                            <div id="mapid" style="width: 100%; height: 500px;"></div>
                            <!--h6 class="m-0 font-weight-bold text-success">Under Development </h6-->
                        </div>
                    </div>

                    <!-- /.container-fluid -->

                    <!-- DREA LANG TAMAN E SULOD ANG CONTENT -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Barangay Baliok <?php echo date("Y");?>
                        </span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
        </div>
    </div>
    <!-- End of Main Content -->


    <?php include '../../include/ezincludescript.php'; ?>

</body>

</html>