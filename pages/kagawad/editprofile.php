<?php include '../../include/mainincludetop.php';?>

<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-success-800">Profile</h1>
</div>

<!------========DREA E BUTANG ANG MGA CONTENT=====----->
<?php
      $profile = mysqli_query($db,"SELECT * FROM resident where id='".$_SESSION['session_user']."';");
      while($row = mysqli_fetch_array($profile))
       {
           $familyhead=$row['hotfname'];   
      ?>
<!-- Content Row -->

<form action="../../php_function/editprofile.php" method="POST">
    <div class="row">
        <!-- Pending Requests Card Example -->
        <div class="col-xl-2 col-md-6 mb-4"></div>

        <!-- Earnings (Monthly) Card Example -->


        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">

        </div>
        <div class="col-xl-2 col-md-6 mb-4">

        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-1 col-md-6 mb-4">

        </div>
        <div class="col-xl-2 col-md-6 mb-4">
            <button type="submit" name="btn-edit" class="form-control btn btn-success">
                SAVE
            </button>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-1"></div>
        <div class="col-xl-10">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Account Overview</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>First Name: <span class="redcolor">*</span></th>
                                <th>
                                    <input type="hidden" class="form-control" name="aydes" placeholder="First Name"
                                        value="<?php echo $_SESSION['session_user'];?>" required="required" />
                                    <input type="text" class="form-control" name="fname" placeholder="First Name"
                                        value="<?php echo $row['fname'];?>" required="required" />
                                </th>
                            </tr>

                            <tr>
                                <th>Last Name: <span class="redcolor">*</span></th>
                                <th>
                                    <input type="text" class="form-control" name="lname" placeholder="Last Name"
                                        value="<?php echo $row['lname'];?>" required="required" />

                                </th>
                            </tr>
                            <tr>
                                <th>Middle Name:</th>
                                <th>
                                    <input type="text" class="form-control" name="mname" placeholder="Middle Name"
                                        value="<?php echo $row['mname'];?>" />

                                </th>
                            </tr>
                            <tr>
                                <th>Nick Name:</th>
                                <th>
                                    <input type="text" class="form-control" name="nname" placeholder="Nick Name"
                                        value="<?php echo $row['nickname'];?>" />

                                </th>
                            </tr>
                            <tr>
                                <th>Gender:</th>
                                <th>
                                    <select class="form-control" name="gender" required="required">
                                        <option value="<?php echo $row['gender'];?>"><?php echo $row['gender'];?>
                                        </option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>Suffix:</th>
                                <th>
                                    <input type="text" class="form-control" name="suffix" placeholder="Suffix"
                                        value="<?php echo $row['suffix'];?>" />

                                </th>
                            </tr>
                            <tr>
                                <th>Email: <span class="redcolor">*</span></th>
                                <th>
                                    <input type="text" class="form-control" name="email" placeholder="Email"
                                        value="<?php echo $row['email'];?>" required="required" />
                                </th>
                            </tr>
                            <tr>
                                <th class="active">Birthday: <span class="redcolor">*</span></th>
                                <th>
                                    <input type="date" class="form-control" name="dob" placeholder="Birthday"
                                        value="<?php echo $row['dob'];?>" required="required" />
                                </th>
                            </tr>
                            <tr>
                                <th class="active">Weight:</th>
                                <th>
                                    <input type="text" class="form-control" name="weight" placeholder="Weight"
                                        value="<?php echo $row[ 'weight'];?>" />
                                </th>
                            </tr>
                            <tr>
                                <th class="active">Height:</th>
                                <th>
                                    <input type="text" class="form-control" name="height" placeholder="Height"
                                        value="<?php echo $row['height'];?>" />
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie Chart -->

        <div class="col-xl-1"></div>
    </div>

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-1"></div>
        <div class="col-xl-10">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Personal Background</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="form-group row">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home">Parent's Name</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu1">Address</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu2">Other info</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div id="home" class="tab-pane active">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Mother's Name:</th>
                                        <th></th>
                                        <th>Father's Name:</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td>First Name: <span class="redcolor">*</span></td>
                                        <td>
                                            <input type="text" class="form-control" name="mfname"
                                                placeholder="First Name" value="<?php echo $row['mfname'];?>"
                                                required="required" />

                                        </td>
                                        <td>First Name: <span class="redcolor">*</span></td>
                                        <td>
                                            <input type="text" class="form-control" name="ffname"
                                                placeholder="First Name" value="<?php echo $row['ffname'];?>"
                                                required="required" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Middle Name:</td>
                                        <td>
                                            <input type="text" class="form-control" name="mmname"
                                                placeholder="Middle Name" value="<?php echo $row['mmname'];?>" />

                                        </td>
                                        <td>Middle Name:</td>
                                        <td>
                                            <input type="text" class="form-control" name="fmname"
                                                placeholder="Middle Name" value="<?php echo $row['fmname'];?>" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="active">Last Name: <span class="redcolor">*</span></td>
                                        <td class="success">
                                            <input type="text" class="form-control" name="mlname"
                                                placeholder="Last Name" value="<?php echo $row['mlname'];?>"
                                                required="required" />

                                        </td>
                                        <td class="active">Last Name: <span class="redcolor">*</span></td>
                                        <td class="success">
                                            <input type="text" class="form-control" name="flname"
                                                placeholder="Last Name" value="<?php echo $row['flname'];?>"
                                                required="required" />

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="menu1">
                            <div class="table-responsive">
                                <table class="table">

                                    <tr>
                                        <th>Province: <span class="redcolor">*</span></th>
                                        <td>
                                            <input type="text" class="form-control" name="province"
                                                placeholder="Province" value="<?php echo $row['province'];?>"
                                                required="required" />
                                        </td>
                                        <th>Purok: <span class="redcolor">*</span></th>
                                        <td>
                                            <select class="form-control select2" id="purokno" name="Purok_No"
                                                onchange="selectPurok()" required>
                                                <option value="<?php echo $row['Purok_No'];?>">
                                                    <?php echo $row['Purok_No'];?></option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="RV3 7-A">RV3 7-A</option>
                                                <option value="RV3 7-B">RV3 7-B</option>
                                                <option value="RV3 8-A">RV3 8-A</option>
                                                <option value="RV3 8-B">RV3 8-B</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11-A">11-A</option>
                                                <option value="11-B">11-B</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <th>Municipality: <span class="redcolor">*</span></th>
                                    <td>
                                        <input type="text" class="form-control" name="citymuni"
                                            placeholder="Municipality" value="<?php echo $row['citymuni'];?>"
                                            required="required" />

                                    </td>
                                    <th>Purok Leader: <span class="redcolor">*</span></th>
                                    <td>
                                        <input type="text" class="form-control" id="purok" name="purokleader"
                                            placeholder="Purok Leader" value="<?php echo $row['purokleader'];?>"
                                            required="required" />

                                    </td>
                                    </tr>
                                    <tr>
                                        <th>Barangay: <span class="redcolor">*</span></th>
                                        <td>
                                            <input type="text" class="form-control" name="barangay"
                                                placeholder="Barangay" value="<?php echo $row['barangay'];?>"
                                                required="required" />

                                        </td>
                                        <th>House No./Street: </th>
                                        <td>
                                            <input type="text" class="form-control" name="street"
                                                placeholder="House No./Street" value="<?php echo $row['street'];?>" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>PERIOD OF RESIDENCY:</th>
                                        <th></th>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="active">Number of years in City:</th>
                                        <td class="success">
                                            <input type="text" class="form-control" name="noofyears"
                                                placeholder="Number of years in City"
                                                value="<?php echo $row['noofyears'];?>" />

                                        </td>
                                        <th class="active">Number of years in Philippines:</th>
                                        <td class="success">
                                            <input type="text" class="form-control" name="numofyears"
                                                value="<?php echo $row['numofyears'];?>" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="active">Number of months in City:</th>
                                        <td class="success">
                                            <input type="text" class="form-control" name="noofmonths"
                                                value="<?php echo $row['noofmonths'];?>" />

                                        </td>
                                        <th class="active">Citizenship: <span class="redcolor">*</span></th>
                                        <td class="success">
                                            <input type="text" class="form-control" name="citizenship"
                                                value="<?php echo $row['citizenship'];?>" required="required" />

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="menu2">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Spouse Name:</th>
                                        <td>
                                            <input type="text" class="form-control" name="spouse"
                                                value="<?php echo $row['spouse'];?>" />

                                        </td>
                                        <th>Civil status: <span class="redcolor">*</span></th>
                                        <td>
                                            <input type="text" class="form-control" name="civilstatus"
                                                value="<?php echo $row['civilstatus'];?>" required="required" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Occupation:</th>
                                        <td>
                                            <input type="text" class="form-control" name="occupation"
                                                value="<?php echo $row['occupation'];?>" />

                                        </td>
                                        <th>Tin number:</th>
                                        <td>
                                            <input type="text" class="form-control" name="tin"
                                                value="<?php echo $row['tin'];?>" />

                                        </td>
                                        <?php } ?>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-1"></div>
    </div>
</form>

<!------========DREA RA TAMAN E BUTANG ANG MGA CONTENT=====----->
<script>
function selectPurok() {

    switch ($("#purokno").val()) {

        case "1":
            document.getElementById("purok").value = "Gemma Ronquillo";
            break;
        case "2":
            document.getElementById("purok").value = "Jessa Mutia";
            break;
        case "3":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
        case "4":
            document.getElementById("purok").value = "Fortune Agustin II";
            break;
        case "5":
            document.getElementById("purok").value = "Edwin Gallera";
            break;
        case "6":
            document.getElementById("purok").value = "Ludivina Olaquer";
            break;
        case "RV3 7-A":
            document.getElementById("purok").value = "Flordelina Bermudez";
            break;
        case "RV3 7-B":
            document.getElementById("purok").value = "Marlyn Duran";
            break;
        case "RV3 8-A":
            document.getElementById("purok").value = "Emma Castillano";
            break;
        case "RV3 8-B":
            document.getElementById("purok").value = "Virginia Avelino";
            break;
        case "9":
            document.getElementById("purok").value = "Thelma Aninon";
            break;
        case "10":
            document.getElementById("purok").value = "Vicentita Ocay";
            break;
        case "11-A":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
        case "11-B":
            document.getElementById("purok").value = "Beatriz Penones";
            break;
    }
}
</script>
<?php include '../../include/mainincludebottom.php';?>