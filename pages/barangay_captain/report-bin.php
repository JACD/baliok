<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Generate report</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Resident Name</th>
                                <th width='150px'>Number of garbage bin damage</th>
                                <th>Purok</th>
                                <th>Date reported</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Resident Name</th>
                                <th width='150px'>Number of garbage bin damage</th>
                                <th>Purok</th>
                                <th>Date reported</th>
                                <th>Action</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php 
					$result = mysqli_query($db,"SELECT * FROM report_bin;");
                         
                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                                <td>
                                    <?php echo $row['name'];?>
                                </td>
                                <td>
                                    <?php echo $row['num_bin'];?>
                                </td>
                                <td>
                                    <?php echo $row['purok'];?>
                                </td>
                                <td>
                                    <?php echo date("F j, Y",strtotime($row['date_created'])); ?>
                                </td>
                                <td>
                                    <!-- <button class="btn btn-success" data-toggle="modal"
                                    data-target="#editModal<php echo $row['bin_id'];?>">
                                    Confirm
                                </button> -->
                                    <?php
                                if($row['confirmation'] === '0')
                                {
                                ?>
                                    <a href="../../php_function/function_for_captain.php?confirm=<?php echo $row['bin_id'];?>&click=confirm"
                                        onclick="return confirm('Are you sure you want to confirm this request?');"
                                        class="btn btn-primary">
                                        Confirm
                                    </a>
                                    <?php 
                                } else if($row['request'] === '0'){
                                ?>
                                    <a href="../../php_function/function_for_captain.php?request=<?php echo $row['bin_id'];?>&click=request"
                                        onclick="return confirm('Are you sure you to send a request of this?');"
                                        class="btn btn-success">
                                        Request
                                    </a>
                                    <?php }else { ?>
                                    <form action='../../php_function/garbagebinreport.php' method='POST'
                                        target="_blank">
                                        <input type="hidden" value="<?php echo $row['bin_id'];?>" name="ripayde" />
                                        <button type="submit" class="btn btn-primary">
                                            Preview
                                        </button>
                                    </form>
                                    <?php } ?>


                                </td>
                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>