<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Ecoboy pledge recieved</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="sked_wednesday.php">WEDNESDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sked_saturday.php">SATURDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sked_sunday.php">SUNDAY</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <form action="" method="GET">
                    <div class="form-group">
                        <label>Week</label>
                        <select class="form-control" name="week_number" required>
                            <option />
                            <option value="week1">
                                WEEK 1
                            </option>
                            <option value="week2">
                                WEEK 2
                            </option>
                            <option value="week3">
                                WEEK 3
                            </option>
                            <option value="week4">
                                WEEK 4
                            </option>
                        </select>
                    </div>
            </div>
            <div class="col-xl-3 col-lg-8">
                <div class="form-group" style="margin-top:12%;">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                </form>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Collection Date</th>
                                <th>Ecoboy Name</th>
                                <th>Pledge sender</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Collection Date</th>
                                <th>Ecoboy Name</th>
                                <th>Pledge sender</th>
                            </tr>
                        </tfoot>
                        <tbody>

                            <?php 
                                   $resulta = mysqli_query($db,"SELECT ecoboy_id from schedule;");
                                    
                                    while($rowes = mysqli_fetch_array($resulta))
                                    { 
                                        $echoid=$rowes['ecoboy_id'];

                                    if(isset($_GET['week_number']))
                                    {
                                    $week=$_GET['week_number'];
                                 
                                    $result = mysqli_query($db,"SELECT SUM(`amount`),ecoboyname,cdate,sked_id,confirmation,pledge_id,day from pledge where week='$week' and day='wednesday' and ecoboy_id='$echoid';");

                                    }
                                    else 
                                    {
                                        
                                    $result = mysqli_query($db,"SELECT SUM(`amount`),ecoboyname,cdate,sked_id,confirmation,pledge_id,day from pledge where day='wednesday' and ecoboy_id='$echoid';");
                                    }
                                    while($row = mysqli_fetch_array($result))
                                    { 
                                          if(isset($row['cdate'])){ 
                                    ?>
                            <tr>
                                <td> <?php echo  $dates= date("F j, Y  "."("."l".")",strtotime($row['cdate']));?> </td>

                                <td><?php echo $row['ecoboyname'];?></td>

                                <td>
                                    <form action="pledgelist.php" method="POST">
                                        <input type="hidden" name="days" value="<?php echo $row['day']; ?>">
                                        <input type="hidden" name="skide" value="<?php echo $row['sked_id']; ?>">
                                        <input type="hidden" name="pledgeid" value="<?php echo $row['pledge_id']; ?>">
                                        <input type="hidden" name="ecoboy" value="<?php echo $echoid; ?>">
                                        <button class="btn btn-primary" name="btn-pledge">
                                            View
                                        </button>
                                    </form>
                                </td>
                                <?php }else { 
                                    }?>
                            </tr>

                            <?php 
                                    
                                    }
                    }
                    ?>
                        </tbody>
                    </table>
                </div>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
    $idhere=$row['Sched_No'];
  ?>
<div class="modal" id="openModal<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Giving a pledge are not mandatory.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/send_pledge.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <?php 
                $pledges = mysqli_query($db, "SELECT * FROM pledge where sked_id='$idhere';");
                while ($rows = mysqli_fetch_array($pledges)) {
                ?>
                    <span style="color:red;">NOTE: <br /> You are already give a pledge with the amount of PHP
                        <?php echo $rows['amount']; ?> </span>
                    <?php } ?>
                    <br />
                    <br />
                    <div class="form-group">
                        <input type="number" class="form-control" placeholder="optional" name="tip" />
                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="pledge" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>