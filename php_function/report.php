<?php
include("config.php");
require ('../fpdf/fpdf.php');
error_reporting(E_ALL);

$pdf = new FPDF('L');
///var_dump (get_class_methods($pdf));
$pdf->SetTitle('Report');
$pdf->AddPage();
$pdf->SetLeftMargin(3.81);
$pdf->SetFont('times','B',10);
$pdf->Image('../components/img/logo/logo.png',90,10,20,"C");
$pdf->Cell(0,10,'Republic of the Philippines',0,0,"C");
$pdf-> SetFont('times','',10);


$pdf->Ln(10);
$pdf->Cell(0,0,'City of Davao',0,0,"C");

$pdf->Ln(5);
$pdf-> SetFont('times','',10);
$pdf->Cell(0,0,'OFFICE OF THE PUNONG BARANGAY',0,0,"C");

$pdf->Ln(5);
$pdf->Cell(0,0,'Barangay Baliok',0,0,"C");

$pdf->Ln(5);
$pdf->Cell(0,0,'Talomo District',0,0,"C");

$pdf->Ln(5,0,'',0);
$pdf->Cell(0,0,'Tel.No.296-1067',0,0,"C");
 
$pdf->Ln(10,0,'',0);
$pdf->SetFont('times','B',12);
$pdf->Cell(0,0,'SOLID WASTE ACCOMPLISHMENT REPORT',0,0,"C");
 
$pdf-> Ln(5);
$pdf-> SetFont('Arial','B',11); 
$pdf-> Cell (25,8,'DATE',1);
$pdf-> Cell (65,8,'PLATE NO./DRIVER',1);
$pdf-> Cell (60,8,'DESTINATION',1);
$pdf-> Cell (30,8,'COMMODITY',1);  
$pdf-> Cell (50,8,'WEIGHT/NET/SW/KGS.X',1); 
$pdf-> Cell (55,8,'TOTAL SOLID WASTE KGS',1); 

$result = mysqli_query($db,"SELECT * FROM accomplishment_report;");

while($row = mysqli_fetch_array($result))
{   
$timbang = $row['weight_net_kgs'];
$cur_price = $row['cur_price'];
$total = $timbang * $cur_price;
$pdf-> Ln(8);
$pdf-> SetFont ('Arial','',10);	 
$pdf-> Cell (25,8,$row['Date'],1);
$pdf-> Cell (65,8,$row['Plate_No'],1);
$pdf-> Cell (60,8,$row['Destination'],1);
$pdf-> Cell (30,8,$row['Commodity'],1);  
$pdf->Cell( 50, 8,number_format($timbang).' X P'.$cur_price, 1, 0, 'R' );  
$pdf->Cell( 55, 8, 'P'.number_format($total,2), 1, 0, 'R' );
}	

$pdf->Output();
?>