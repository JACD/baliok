<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success"> Ecoboy Collected Garbage</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <a href="../../php_function/print_garbage_collected.php" target="_blank"
                    class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-print"></i>
                    Print</a>
                <br />
                <br />
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Ecoboy Name</th>
                                <th>Collected the garbage?</th>
                                <th>Reason</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Date</th>
                                <th>Ecoboy Name</th>
                                <th>Collected the garbage?</th>
                                <th>Reason</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php  
					$result = mysqli_query($db,"SELECT * FROM `ecoboy_claimed_garbage` ORDER by garbage_id DESC;"); 
                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                                <td>
                                    <?php echo date("F j, Y" ,strtotime($row['date_collected'])); ?>
                                </td>
                                <td>
                                    <?php echo ucfirst($row['Ecoboy_nym']); ?>
                                </td>
                                <?php if($row['claim']=='No') { ?>
                                <td>
                                    Not Collected
                                </td>
                                <?php } else { ?>
                                <td>
                                    Collected
                                </td>
                                <?php } ?>

                                <td>
                                    <?php echo $row['reason']; ?>
                                </td>
                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<?php
			$cap = mysqli_query($db,"SELECT * FROM resident;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal" id="openModal<?php echo $row['id'];?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Rate</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="../../php_function/function_resident_rate.php" method="POST">
                    <div class="form-group">
                        <label>Select</label>
                        <select class="form-control" name="rates" required>
                            <option />
                            <option value="5">
                                Excellent
                            </option>
                            <option value="4">
                                Best
                            </option>
                            <option value="3">
                                Better
                            </option>
                            <option value="2">
                                Good
                            </option>
                            <option value="1">
                                Bad
                            </option>
                        </select>
                    </div>

                    <input type="hidden" value="<?php echo $row['id'];?>" name="ecoboyid" />


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button type="submit" name="btn-rate" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>



<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>