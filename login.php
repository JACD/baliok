<?php include 'php_function/login.php';?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="components/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="components/img/logo/favicon-16x16.png">
    <script src="components/js/checkemail.js"></script>
    <style>
    div.registration-form fieldset {
        display: none;
    }
    </style>
</head>

<body class="bg-gradient-success">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block ">
                                <img style="margin:10% 10% 10% 10%; width:100%;height:80%;"
                                    src="components/img/logo/logo2.png">

                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="registration-form">
                                        <fieldset>
                                            <div class="text-center">
                                                <h1 class="h4 text-gray-900 mb-4">Login</h1>
                                            </div>
                                            <form class="user" role="form" method="POST" data-ajax="false">
                                                <div class="form-group">
                                                    <input type="email" class="form-control form-control-user"
                                                        id="exampleInputEmail" aria-describedby="emailHelp"
                                                        placeholder="Enter Email Address" name=" email" required="">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-user"
                                                        id="exampleInputPassword" placeholder="Password" name="password"
                                                        required="">
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox small">
                                                        <input type="checkbox" class="custom-control-input"
                                                            id="customCheck">
                                                        <label class="custom-control-label" for="customCheck">Remember
                                                            Me</label>
                                                    </div>
                                                </div>
                                                <button type="submit" name="btn-login"
                                                    class="btn btn-primary btn-user btn-block">
                                                    Login
                                                </button>
                                            </form>
                                            <hr>
                                            <!--div class="text-center">
                                                <a class="small" href="forgotpassword.php">Forgot Password?</a>
                                            </div-->
                                            <div class="text-center">
                                                <a class="small btn-next" href="#">Create an Account!</a>
                                            </div>
                                        </fieldset>
                                        <fieldset style="padding:10% 15% 20% 15%;">
                                            <div class="text-center">
                                                <h1 class="h4 text-gray-900 mb-4">Sign up</h1>
                                            </div>
                                            <form class="user" action="mail/sendmail.php" role="form" method="POST"
                                                data-ajax="false">
                                                <div class="form-group">
                                                    <input type="email" class="form-control form-control-user"
                                                        onkeyup="checkemail();" id="email" autocomplete="off"
                                                        placeholder="Enter Email Address" name="email"
                                                        required="required">
                                                    <span id="name_status"></span>
                                                </div>
                                                <button type="submit" name="btn-login"
                                                    class="btn btn-primary btn-user btn-block">
                                                    Submit
                                                </button>
                                            </form>
                                            <hr>
                                            <div class="text-center">
                                                <button type="button" class="btn btn-success btn-previous"
                                                    href="">Login</button>
                                            </div>
                                        </fieldset>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="regex/jquery.min.js"></script>
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="components/js/sb-admin-2.min.js"></script>
        <script type="text/Javascript">
            jQuery(document).ready(function() {

    $('.registration-form fieldset:first-child').fadeIn('slow'); 

    
    // next step

        $('.registration-form .btn-next').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).next().fadeIn();
    	});
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });

    
});
</script>
</body>

</html>