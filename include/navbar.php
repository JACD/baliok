 <!-- Topbar -->
 <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

     <!-- Sidebar Toggle (Topbar) -->
     <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
         <i class="fa fa-bars"></i>
     </button>

     <!-- Topbar Navbar -->
     <ul class="navbar-nav ml-auto">

         <?php if($position==''){ ?>


         <!-- Nav Item - Alerts -->
         <li class="nav-item dropdown no-arrow mx-1">
             <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">
                 <i class="fas fa-bell fa-fw"></i>
                 <!-- Counter - Alerts -->
                 <?php 
                $sql="SELECT * FROM `resident_responded` WHERE `rest_id`='".$_SESSION['session_user']."' AND `if_collected`='Yes' AND `seen`='0';";

                if ($result=mysqli_query($db,$sql))
                {
                // Return the number of rows in result set
                $rowcount=mysqli_num_rows($result);
             
                if($rowcount=='0')
                {
               
                }
                else {
                printf('<span class="badge badge-danger badge-counter">'.$rowcount.'</span>');
                
                }
                mysqli_free_result($result);
                }



                 ?>

             </a>
             <!-- Dropdown - Alerts -->
             <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="alertsDropdown">
                 <h6 class="dropdown-header">
                     Alerts
                 </h6>
                 <?php if($rowcount >= '1'){
                     
                     $notifier = mysqli_query($db, "SELECT * FROM `resident_responded` WHERE `rest_id`='".$_SESSION['session_user']."' AND `if_collected`='Yes' AND `seen`='0';");

                    while ($notif = mysqli_fetch_array($notifier)) {
                     
                     ?>

                 <a class="dropdown-item d-flex align-items-center"
                     href="../../php_function/resident_responded.php?respond=<?php echo $notif['respond_id'];?>">
                     <div class="mr-3">
                         <div class="icon-circle bg-primary">
                             <i class="fas fa-bell text-white"></i>
                         </div>
                     </div>
                     <div>
                         <div class="small text-gray-500">
                             <?php echo date("j F Y" ,strtotime($notif['date_collected']));?></div>
                         <span class="font-weight-bold">Your garbage was collected.</span>
                     </div>
                 </a>

                 <?php } } else { ?>
                 <a class="dropdown-item d-flex align-items-center" href="#">
                     <div>
                         <span class="font-weight-bold">No notification</span>
                     </div>
                 </a>
                 <?php } ?>


                 <!-- <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a> -->
             </div>
         </li>
         <?php } ?>
         <div class="topbar-divider d-none d-sm-block"></div>

         <!-- Nav Item - User Information -->
         <li class="nav-item dropdown no-arrow">
             <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">
                 <?php if($position=="admin"){ ?>
                 <span class="mr-2 d-none d-lg-inline text-gray-600 small"> <?php echo $fullname;?></span>
                 <img class="img-profile rounded-circle"
                     src="../../components/img/profile/<?php echo $profilepicture;?>">
                 <?php } else  if($position == "CENRO" or $position == "landfill") { ?>
                 <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $fullname;?></span>
                 <img class="img-profile rounded-circle" src="../../components/img/profile/<?php echo $puto;?>">
                 <?php } else { ?>
                 <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $fullname;?></span>
                 <img class="img-profile rounded-circle"
                     src="../../components/img/profile/<?php echo $profilepicture1;?>">
                 <?php } ?>

             </a>

             <!-- Dropdown - User Information -->
             <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                 <?php  if($position == "CENRO" or $position == "landfill") { ?>
                 <a class="dropdown-item" href="" data-toggle="modal" data-target="#profile">
                     <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                     Profile
                 </a>
                 <?php } else { ?>
                 <a class="dropdown-item" href="../../php_function/profile_k.php?profile-php=php">
                     <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                     Profile
                 </a>
                 <?php } ?>

                 <div class="dropdown-divider"></div>
                 <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">
                     <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                     Logout
                 </a>
             </div>
         </li>

     </ul>

 </nav>
 <!-- End of Topbar -->
 <!-- Logout Modal-->
 <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
             <div class="modal-footer">
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                 <a class="btn btn-primary" href="../../php_function/logout.php">Logout</a>
             </div>
         </div>
     </div>
 </div>
 <div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Profile</h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-xl-4">
                     </div>
                     <div class="col-xl-4">
                         <form action="../../../php_function/function_for_useragent.php" method="POST"
                             enctype="multipart/form-data">
                             <img src="../../components/img/profile/<?php echo $puto;?>"
                                 class="img-profile rounded-circle" width="150" height="150" />

                             <div style="color:blue; font-size:15px; margin-top:20%;">
                                 <input type="file" name="image" accept="image/*;capture=camera">
                             </div>
                     </div>
                     <div class="col-xl-4">
                     </div>
                 </div>
                 <br />
                 <div class="row">
                     <div class="col-xl-2">
                     </div>
                     <div class="col-xl-8">
                         <label class="control-label">First Name:</label>
                         <input type="text" class="form-control" name="fname" value="<?php echo $fname?>"
                             placeholder="First Name" />
                         <label class="control-label">Last Name:</label>
                         <input type="text" class="form-control" name="lname" value="<?php echo $lname?>"
                             placeholder="Last Name" />
                     </div>
                     <div class="col-xl-2">
                     </div>

                 </div>
             </div>
             <div class="modal-footer">
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                 <button type="submit" onclick="return confirm('Do you want to update?');" name="btn-uploads"
                     class="btn btn-success">Submit</button>
                 </form>
             </div>
         </div>
     </div>
 </div>