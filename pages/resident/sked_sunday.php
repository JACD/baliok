<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Garbage Collection Schedule</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="sked_wednesday.php">WEDNESDAY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sked_saturday.php">SATURDAY</a>
                </li>
                <li class="nav-item border-bottom-info">
                    <a class="nav-link active" href="sked_sunday.php">SUNDAY</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-8">
                <form action="" method="GET">
                    <div class="form-group">
                        <label>Week</label>
                        <select class="form-control" name="week_number" required>
                            <option />
                            <option value="week1">
                                WEEK 1
                            </option>
                            <option value="week2">
                                WEEK 2
                            </option>
                            <option value="week3">
                                WEEK 3
                            </option>
                            <option value="week4">
                                WEEK 4
                            </option>
                        </select>
                    </div>
            </div>
            <div class="col-xl-3 col-lg-8">
                <div class="form-group" style="margin-top:12%;">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                </form>
            </div>
        </div>
        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Collection Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Ecoboy Name</th>
                                <th>Do you have garbage?</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Collection Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Ecoboy Name</th>
                                <th>Do you have garbage?</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php 
                                    if(isset($_GET['week_number']))
                                    {
                                    $week=$_GET['week_number'];

                                    $result = mysqli_query($db,"SELECT * FROM schedule where status='Approve' and week_no='$week' and days='sunday';");

                                    }
                                    else 
                                    {
                                    $result = mysqli_query($db,"SELECT * FROM schedule where status='Approve' and days='sunday';");
                                    }
                                    while($row = mysqli_fetch_array($result))
                                    { 
                                       $idsaskedul= $row['Sched_No'];  
                                    ?>
                                <td>
                                    <?php echo date("F j, Y  "."("."l".")",strtotime($row['CollDate'])); ?>
                                </td>
                                <td>
                                    <?php echo $row['StartTime'];?>
                                </td>
                                <td>
                                    <?php echo $row['EndTime'];?>
                                </td>
                                <td>
                                    <?php echo $row['EcoboyName'];?>
                                </td>
                                <!-- <td>
                                    <button class="btn btn-primary" data-toggle="modal"
                                        data-target="#openModal<?php echo $row['Sched_No']; ?>">
                                        <i class="fas fa-eye"> </i>
                                    </button>
                                </td>-->
                                <?php 
                                $idsauser=$_SESSION['session_user'];
                               $queryforresponder = mysqli_query($db,"SELECT * FROM resident_responded where sked_id='$idsaskedul' and rest_id='$idsauser';");
                                  
                                    while($rowbert = mysqli_fetch_array($queryforresponder))
                                    {
                                        $idskedul=$rowbert[sked_id];
                                        $residentid=$rowbert[res_id];
                                        $garbage=$rowbert[garbage];
                                        $garbagetype=$rowbert[garbage_type];
                                    }   
                              if($garbage=='Yes' && $idskedul==$idsaskedul){
                               ?>
                                <td>
                                    <h5 class="success">Yes</h5>
                                    <h5>Garbage Type:</h5>
                                    <?php if($garbagetype=='rw') {?>
                                    <h5 style="color:green;">Recyclable waste</h5>
                                    <?php }else if($garbagetype=='sw') { ?>
                                    <h5 style="color:blue;">Sanitary waste</h5>
                                    <?php } else if($garbagetype=='mw') {?>
                                    <h5 style="color:red;">Mixed waste</h5>
                                    <?php } ?>
                                </td>
                                <?php } else if($garbage=='No' && $idskedul==$idsaskedul) { ?>
                                <td>
                                    <h3 class="success">No</h3>
                                </td>
                                <?php } else {?>
                                <td>
                                    <form action="../../php_function/resident_responded.php" role="form"
                                        id="submitrespond" method="POST">
                                        <div class="form-group row" style="text-align:center;">
                                            <div class="col-sm-12 mb-4 mb-sm-2">
                                                <label class="radio-inline">
                                                    <input type="checkbox" name="decis" id="yes" style="width: 30px;"
                                                        class="radiobtn form-control" value="Yes" />
                                                    <h5>Yes</h5>
                                                </label> &nbsp;
                                                <label class="radio-inline" id="hidewhenyes">
                                                    <input type="checkbox" name="decis" id="no" style="width: 30px;"
                                                        class="form-control" value="No"
                                                        onchange="submitresponders();" />
                                                    <h5>No</h5>
                                                </label>
                                                <div id="showYes" class="showhidediv" style="display:none;">
                                                    <div class="form-group">
                                                        <select class="control" name="garbagetype"
                                                            onchange="submitresponders();">
                                                            <option>Select Garbage type</option>
                                                            <option value="rw">Recyclable Waste</option>
                                                            <option value="sw">Sanitary Waste</option>
                                                            <option value="mw">Mixed Waste</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" value="<?php echo $row['Sched_No']; ?>"
                                                name="idsked" />
                                            <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                                        </div>
                                        <div id="result" hidden></div>
                                    </form>
                                </td>
                                <?php } ?>
                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>
                </div>

            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- /.container-fluid -->

<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
    $idhere=$row['Sched_No'];
  ?>
<div class="modal" id="openModal<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Do you have a garbage?</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/resident_responded.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group row" style="text-align:center;">
                        <div class="col-sm-12 mb-4 mb-sm-2">
                            <label class="radio-inline">
                                <input type="radio" name="decis" id="yes" style="width: 30px;"
                                    class="radiobtn form-control" value="Yes" />
                                <h5>Yes</h5>
                            </label> &nbsp;
                            <label class="radio-inline">
                                <input type="radio" name="decis" id="no" style="width: 30px;"
                                    class="radiobtn form-control" value="No" />
                                <h5>No</h5>
                            </label>
                        </div>
                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                        <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                    </div>
                    <!-- <div id="showNo" class="showhidediv" style="display:none;">
                        <div class="form-group">
                            <textarea type="text" class="form-control" width="5%" height="5%" name="reason"></textarea>
                           
                        </div>
                    </div>-->


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="accept" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
$(window).on("load", function() {
    if ("geolocation" in navigator) { //check geolocation available 
        //try to get user current location using getCurrentPosition() method
        navigator.geolocation.getCurrentPosition(function(position) {
            $("#result").html("Found your location <br />Lat : <input type='text' name='lat' value='" +
                position.coords.latitude + "'/> </br>Lang :<input type='text' name='lng' value='" +
                position.coords.longitude + "'/>");
        });
    } else {
        console.log("Browser doesn't support geolocation!");
    }
});

function submitresponders() {
    avar = confirm('Theres no undo! Do you want to continue?');
    if (avar == true) {
        document.getElementById('submitrespond').submit();
    }
    if (avar == false) {
        location.reload();
    }
}
</script>

<?php include '../../include/mainincludebottom.php';?>a