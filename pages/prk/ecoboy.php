<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-success-800">Donation</h1>
</div>


<!-- DREA E SULOD ANG CONTENT -->

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-gray-600">Ecoboy Donation</h6>
        </center>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Ecoboy Name</th>
                        <th>Donation sender</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Ecoboy Name</th>
                        <th>Donation sender</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <?php 
			            	$result = mysqli_query($db,"SELECT * FROM resident where position='Ecoboy';");

                    while($row = mysqli_fetch_array($result))
                    {   
                      ?>
                        <td><?php echo $row['fname'].' '.$row['lname'];?></td>
                        <td>
                            <form action="../prk/list_pledge.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                <button class="btn btn-success" name="btn-pledge">
                                    View
                                </button>
                            </form>
                        </td>

                    </tr>

                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>