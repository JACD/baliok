<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Receipt</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Weigh-In</th>
                                <th>Weight/net/kgs</th>
                                <th>Total Solid waste KGS</th>
                                <th>View Receipt</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Weigh-In</th>
                                <th>Weight/net/kgs</th>
                                <th>Total Solid waste KGS</th>
                                <th>View Receipt</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php 
                    $pricenow = mysqli_query($db,"SELECT * FROM garbageprice where price_status='1';"); 
                    while($row = mysqli_fetch_array($pricenow))
                    {
					$cur_price=$row['price_value'];	
					}
                    
                    $result = mysqli_query($db,"SELECT * FROM schedule WHERE loading_ecoboy='load';");
                             
                    while($row = mysqli_fetch_array($result))
                    {   
						$krpt = $row['receipt_kagawad'];
						$lfc = $row['landfill_confirmation'];
						$lde = $row['loading_date_ecoboy'];
						$ldc = $row['loading_date_cenro'];
						$lc = $row['loading_cenro'];
						$llf = $row['loading_landfill'];
						$skidnum = $row['Sched_No'];
						$cur_price = $row['current_price'];
						$timbang = $row['weight_net_kgs'];
						$total = $timbang * $cur_price;
                      ?>
                                <td>
                                    <?php if($ldc===null){  } else { echo date("F j, Y, g:i a",strtotime($ldc)); } ?>
                                </td>
                                <td>
                                    <?php echo number_format($timbang).' X P'.$cur_price;?>
                                </td>
                                <td>
                                    <?php echo 'Php'.number_format($total,2);?>
                                </td>
                                <td>

                                    <?php if($krpt=='1'){?>
                                    <form action="../../php_function/receipt.php" method="POST" target="_blank">
                                        <input type="hidden" value="<?php echo $row['Sched_No'];?>"
                                            name="idgetorposted">
                                        <button class="btn btn-primary">
                                            <i class="fas fa-receipt"> </i>
                                        </button>
                                    </form>

                                    <?php } ?>
                                </td>
                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>


<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>