<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-success">Barangay Functionaries</h6>
    </div>
    <div class="card-body">



        <!-- DREA E SULOD ANG CONTENT -->


        <div class="form-group row">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="functionaries_c.php">Barangay Captain</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="functionaries_k.php">Barangay Kagawad</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="functionaries_p.php">Barangay Personnel</a>
                </li>
            </ul>
        </div>

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-8 col-lg-8">

                <!-- Area Chart -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-success">Barangay Captain</h6>
                    </div>
                    <div class="card-body">
                        <div class="chart-area">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Year Declared</th>
                                            <th>Year End</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Year Declared</th>
                                            <th>Year End</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <?php
																			$cap = mysqli_query($db,"SELECT * FROM brgycaptain order by current DESC;");
																			while($row = mysqli_fetch_array($cap))
																			{   
																			?>
                                            <td>
                                                <?php echo $row['name'];?>
                                            </td>
                                            <td>
                                                <?php echo $row['yeardeclared'];?>
                                            </td>
                                            <td>
                                                <?php echo $row['yearend'];?>
                                            </td>
                                            <td>
                                                <?php echo $row['current'];?>
                                            </td>
                                            <td>
                                                <button class="btn btn-success" data-toggle="modal"
                                                    data-target="#editModal<?php echo $row['brgycaptain_id'];?>">
                                                    <i class="fas fa-edit"> </i>
                                                </button>
                                                <a href="../../php_function/function_for_captain.php?remove=<?php echo $row['brgycaptain_id'];?>&click-remove=remove"
                                                    onclick="return confirm('Are you sure you want to remove the Current Captain?');"
                                                    class="btn btn-danger">
                                                    <i class="fas fa-trash"> </i>
                                                </a>

                                            </td>
                                        </tr>

                                        <?php
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Donut Chart -->
            <div class="col-xl-4 col-lg-3">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Insert Barangay Captain</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <?php
										$cap = mysqli_query($db,"SELECT * FROM brgycaptain where current='Current Captain';");
										if($row = mysqli_fetch_array($cap))
										{   
										  ?>
                        <div class="chart-area">
                            <div class="form-group">
                                <label>Name</label>
                                <select class="form-control" name="name" disabled>
                                    <option> </option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Year Declared</label>
                                <input type="date" disabled class="form-control" id="yeardeclared" name="yeardeclared"
                                    required>
                            </div>
                            <div>
                                <button type="submit" disabled onclick="return confirm('Do you want to submit?');"
                                    name="btn-cap" class="btn btn-success btn-user btn-block">Submit</button>
                            </div>
                        </div>
                        <?php } else { ?>
                        <div class="chart-area">
                            <form action="../../php_function/function_for_captain.php" method="POST">
                                <div class="form-group">
                                    <label>Name</label>
                                    <select class="form-control" name="name" required>
                                        <option />
                                        <?php
														$cap = mysqli_query($db,"SELECT * FROM resident where status='active' and position='';");
														while($row = mysqli_fetch_array($cap))
														{    
														  ?>
                                        <option value="<?php echo $row['id'];?>">
                                            <?php echo $row['fname'].' '.$row['lname'];?>
                                        </option>
                                        <?php } ?>


                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Year Declared</label>
                                    <input type="date" class="form-control" id="yeardeclared" name="yeardeclared"
                                        required>
                                </div>
                                <div>
                                    <button type="submit" onclick="return confirm('Do you want to submit?');"
                                        name="btn-cap" class="btn btn-success btn-user btn-block">Submit</button>
                                </div>
                            </form>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- /.container-fluid -->
<?php
			$cap = mysqli_query($db,"SELECT * FROM brgycaptain;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<!-- update Modal-->
<div class="modal fade" id="editModal<?php echo $row['brgycaptain_id'];?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update record</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../../php_function/updatecaptain.php" method="POST">

                    <div class="form-group">
                        <label>Name</label>

                        <select class="form-control" name="name" required>
                            <option value="<?php echo $row['res_id'];?>"><?php echo $row['name'];?></option>
                            <?php
														$caps = mysqli_query($db,"SELECT * FROM resident where status='active' and position='';");
														while($rower = mysqli_fetch_array($caps))
														{    
														  ?>
                            <option value="<?php echo $rower['id'];?>">
                                <?php echo $rower['fname'].' '.$rower['lname'];?>
                            </option>
                            <?php } ?>


                        </select>
                    </div>
                    <div class="form-group">
                        <label>Year Declared</label>
                        <input type="date" class="form-control" id="yeardeclared"
                            value="<?php echo $row['yeardeclared'];?>" name="yeardeclared" required>
                    </div>
                    <input type="hidden" class="form-control" name="idfor" value="<?php echo $row['brgycaptain_id'];?>"
                        required>

                    <div>
                    </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" onclick="return confirm('Do you want to update?');" name="btn-ecap"
                    class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>