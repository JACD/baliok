<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Loading and Unloading</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Loading Date</th>
                                <th>Weight/Net/Kgs</th>
                                <th>Total Solid waste KGS</th>
                                <th>Action</th>
                                <th>Issue Receipt</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Loading date</th>
                                <th>Weight/Net/Kgs</th>
                                <th>Total Solid waste KGS</th>
                                <th>Action</th>
                                <th>Issue Receipt</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php 
                 
                    
                    $result = mysqli_query($db,"SELECT * FROM schedule WHERE loading_ecoboy='load';");
                             
                    while($row = mysqli_fetch_array($result))
                    {   
						$krpt = $row['receipt_kagawad'];
						$lfc = $row['landfill_confirmation'];
						$lde = $row['loading_date_ecoboy'];
						$ldc = $row['loading_date_cenro'];
						$lc = $row['loading_cenro'];
						$llf = $row['loading_landfill'];
						$skidnum = $row['Sched_No'];
						$cur_price = $row['current_price'];
						$timbangsabasura = $row['weight_net_kgs']; 
						$timbangsatruck = $row['garbage_truck_weight'];     
                        $timbangsaall = $timbangsatruck + $timbangsabasura;
						$total = $timbangsabasura * $cur_price;
                      ?>
                                <td>
                                    <?php if($ldc===null){  } else { echo date("F j, Y, g:i a",strtotime($ldc)); } ?>
                                </td>
                                <td>
                                    <?php echo number_format($timbangsabasura).' X P'.$cur_price;?>
                                </td>
                                <td>
                                    <?php echo 'P'.number_format($total,2);?>
                                </td>
                                <td>
                                    <?php if($lc =='load' && $llf =='load' && $lfc ==null){?>
                                    <a href="../../php_function/function_cenro.php?lf_id=<?php echo $skidnum;?>&clickbtncf=confirmed"
                                        onclick="return confirm('Do you want to confirm? There`s no undo!');"
                                        class="btn btn-success">
                                        Confirm
                                    </a>
                                    <?php }else { echo '<span style="color:#1cc88a;"> Confirmed </span>'; } ?>

                                </td>
                                <td>
                                    <?php if($lfc=='1' && $krpt=='' & $lc =='load' && $llf =='load'){?>
                                    <button class="btn btn-success btn-circle" data-toggle="modal"
                                        data-target="#editModal<?php echo $row['Sched_No'];?>">
                                        <i class="fas fa-receipt"> </i>
                                    </button>
                                    <?php } else if($krpt=='1'){?>
                                    <form action="../../php_function/receipt.php" method="POST" target="_blank">
                                        <input type="hidden" value="<?php echo $row['Sched_No'];?>"
                                            name="idgetorposted">
                                        <button class="btn btn-primary">
                                            <i class="fas fa-receipt"> </i>
                                        </button>
                                    </form>

                                    <?php } else { echo '';} ?>
                                </td>
                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <!-- Donut Chart -->

        </div>
    </div>
</div>

<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<form action="../../php_function/function_for_landfill.php" role="form" method="POST">
    <div class="modal" id="editModal<?php echo $row['Sched_No'];?>">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title">Issue Receipt</h4>
                    </center>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="form-group">
                                <?php 
                                $timbangsabasura = $row['weight_net_kgs']; 
                                $timbangsatruck = $row['garbage_truck_weight']; 
                                $timbangsaall = $timbangsatruck + $timbangsabasura;
                                ?>
                                <input type="hidden" class="form-control" value="<?php echo $row['current_price'];?>"
                                    name="curprice" required>
                                <input type="hidden" class="form-control" value="<?php echo $row['driver'];?>"
                                    name="driver" required>
                                <input type="hidden" class="form-control" value="<?php echo $row['plate_number'];?>"
                                    name="pletnumber" required>
                                <input type="hidden" class="form-control" value="<?php echo $row['Sched_No'];?>"
                                    name="idhere" required>
                                <input type="hidden" class="form-control" value="<?php echo $timbangsabasura;?>"
                                    name="weight_garbage" required>
                                <input type="hidden" class="form-control" value="<?php echo $timbangsatruck;?>"
                                    name="weight_truck" required>
                                <input type="hidden" class="form-control" value="<?php echo date('h:m a, d/m/Y');?>"
                                    name="datenow" required>
                                <input type="hidden" class="form-control" value="Solid waste" name="commodity" required>

                            </div>

                            <div class="form-group">
                                <label>Customer</label>
                                <input type="text" class="form-control" id="end" placeholder="Enter Customer"
                                    name="customer" required>
                            </div>
                            <div class="form-group">
                                <label>Area</label>
                                <input type="text" class="form-control" id="end" placeholder="Enter Area" name="area"
                                    required>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="add-sked" onclick="return confirm('Do you want to save?');"
                            class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>