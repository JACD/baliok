<?php include('../../php_function/session.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ECOLLECT</title>
    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../components/css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="../../components/img/logo/favicon-16x16.png">
    <style>
    @media screen and (min-width: 320px) and (max-width: 767px) and (orientation: portrait) {

        /* html {
            transform: rotate(-90deg);
            transform-origin: left top;
            width: 95vh;
            height: 100vw;
            overflow-x: hidden;
            position: absolute;
            top: 100%;
            left: 0;
        } */
        html {
            -ms-transform: rotate(-90deg);
            -webkit-transform: rotate(-90deg);
            transform: rotate(-90deg);
            width: 95vh;
            height: 100vw;
            overflow: scroll;
        }

        .modal-gamay {
            padding-right: 80px !important;
            padding-left: 80px !important;
        }
    }

    @media only screen and (max-width: 600px) {
        .fc-button-group {
            margin-right: 55px !important;
        }

        .formobile {
            margin-top: -25px;
            margin-bottom: 24%;
        }
    }

    .fc-day-grid-event {
        font-size: 15px;
        margin-top: 2px;
        padding: 10px;
        color: #f8f9fc !important;
    }

    body {
        overflow-x: hidden !important;
    }

    .mycolor {
        background-color: #00a65a !important;
        border: 1px solid #00a65a !important;
        color: #fff !important;
        cursor: pointer !important;
    }

    .mycolors {
        background-color: #e74a3b !important;
        border: 1px solid #e74a3b !important;
        color: #fff !important;
        cursor: pointer !important;
    }

    .myclr {
        background-color: #3a87ad !important;
        border: 1px solid #3a87ad !important;
        color: #fff !important;
        cursor: pointer !important;
    }

    .colored {
        color: #e74a3b;
        font-size: 1rem;
    }

    #calendar {
        margin-top: -2rem !important;
    }

    .showcancel {
        display: none !important;
    }

    .thiswillhide {
        display: none !important;
    }

    .hidethis {
        display: none !important;
    }

    .btnsave {
        display: none !important;
    }
    </style>


</head>
<?php include '../../include/includecalendarhead.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Garbage Collection Schedule</h6>
        </center>

        <div class="card-body">
            <!-- DREA E SULOD ANG CONTENT -->


            <!-- Content Row -->
            <div class="row">

                <div class="col-xl-12 col-lg-8">
                    <div class="formobile">
                        <div class="mt-4 text-left small">
                            <span class="mr-2">
                                <i class="fas fa-circle text-primary"></i> PENDING
                            </span>
                            <span class="mr-2">
                                <i class="fas fa-circle text-success"></i> APPROVED
                            </span>
                            <span class="mr-2">
                                <i class="fas fa-circle text-danger"></i> DENIED
                            </span>
                        </div>
                    </div>
                    <div id="calendar"></div>
                </div>

                <!-- Donut Chart -->

            </div>
        </div>
    </div>

    <!-- /.container-fluid -->




    <?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>

    <div class="modal modal-gamay" data-backdrop="false" id="approve<?php echo $row['Sched_No'];?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title">Garbage Collection Schedule</h4>
                    </center>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="form-group">
                                <h6><strong>Title</strong></h6>
                                <h5>
                                    <?php echo $row['title'];?>
                                </h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <h6><strong>Start Time</strong></h6>
                                <h5>
                                    <?php echo date('h:i A', strtotime($row['StartTime']));?>
                                </h5>
                            </div>
                            <div class="form-group">
                                <h6><strong>End Time</strong></h6>
                                <h5>
                                    <?php echo date('h:i A', strtotime($row['EndTime']));?>
                                </h5>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <h6><strong>Purok No.</strong></h6>
                                <h5>
                                    <?php echo $row['Purok_No'];?>
                                </h5>
                            </div>
                            <div class=" form-group ">
                                <h6><strong>Ecoboy Name</strong></h6>
                                <h5>
                                    <?php echo $row['EcoboyName'];?>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <div class="col-xl-12 col-lg-12">

                            <label style="margin-left: 8rem; font-weight: bold;">Do you have garbage?</label>
                            <div class="form-group" style="text-align:center !important;">
                                <?php 
                                $idsaskedul=$row['Sched_No'];
                                $idsauser=$_SESSION['session_user'];
                               $queryforresponder = mysqli_query($db,"SELECT * FROM resident_responded where sked_id='$idsaskedul' and rest_id='$idsauser';");
                                  
                                    while($rowbert = mysqli_fetch_array($queryforresponder))
                                    {
                                        $idskedul=$rowbert[sked_id];
                                        $residentid=$rowbert[res_id];
                                        $garbage=$rowbert[garbage];
                                        $garbagetype=$rowbert[garbage_type];
                                    }   
                              if($garbage=='Yes' && $idskedul==$idsaskedul){
                               ?>
                                <div>
                                    <h5 class="success">Yes</h5>
                                    <label>Garbage Type:</label>
                                    <?php if($garbagetype=='rw') {?>
                                    <h5 style="color:green;">Recyclable waste</h5>
                                    <?php }else if($garbagetype=='sw') { ?>
                                    <h5 style="color:blue;">Sanitary waste</h5>
                                    <?php } else if($garbagetype=='mw') {?>
                                    <h5 style="color:red;">Mixed waste</h5>
                                    <?php } ?>
                                </div>
                                <?php } else if($garbage=='No' && $idskedul==$idsaskedul) { ?>
                                <td>
                                    <h3 class="success">No</h3>
                                </td>
                                <?php } else {?>
                                <form action="../../php_function/responded_resident.php" role="form" id="submitrespond"
                                    method="POST">
                                    <div class="form-group row" style="text-align:center;">
                                        <div class="col-sm-12 mb-4 mb-sm-2">
                                            <label class="radio-inline" id="hidewheno">
                                                <input type="checkbox" name="decis" id="btnyes" style="width: 30px;"
                                                    class="radiobtn form-control" value="Yes" />
                                                <h5>Yes</h5>
                                            </label> &nbsp;
                                            <label class="radio-inline" id="hidewhenyes">
                                                <input type="checkbox" name="decis" id="no" style="width: 30px;"
                                                    class="radiono form-control" value="No" />
                                                <h5>No</h5>
                                            </label>
                                            <div id="showYes" class="showhidediv" style="display:none;">
                                                <div class="form-group">
                                                    <select class="control" name="garbagetype">
                                                        <option value="no">Select Garbage type</option>
                                                        <option value="rw">Recyclable Waste</option>
                                                        <option value="sw">Sanitary Waste</option>
                                                        <option value="mw">Mixed Waste</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="result<?php echo $row['Sched_No'];?>" hidden></div>
                                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                                    </div>
                                    <button type="submit" style="display:none;" class="btn btn-primary" id="showbtn"
                                        onclick="return confirm('Theres no undo! Do you want to continue?');">Submit</button>

                                </form>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>


    <!-- DREA LANG TAMAN E SULOD ANG CONTENT -->

</div>
<!-- /.container-fluid -->

</div>
<?php include '../../include/footer.php';?>
</div>
</div>
<!-- End of Main Content -->

<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>

<script>
$(window).on("load", function() {
    if ("geolocation" in navigator) { //check geolocation available 
        //try to get user current location using getCurrentPosition() method
        navigator.geolocation.getCurrentPosition(function(position) {
            $("#result" + "<?php echo $row['Sched_No']; ?>").html(
                "Found your location <br />Lat : <input type='text' name='lat' value='" +
                position.coords.latitude + "'/> </br>Lang :<input type='text' name='lng' value='" +
                position.coords.longitude + "'/>");
        });
    } else {
        console.log("Browser doesn't support geolocation!");
    }
});
</script>
<?php } ?>
<!-- Bootstrap core JavaScript-->
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js "></script>
<script>
$('.radiobtn').click(function() {
    $('div#showYes').show("slow");
    $('label#hidewhenyes').hide("slow");
    $('button#showbtn').show("slow");
});
$('.radiono').click(function() {
    $('label#hidewheno').hide("slow");
    $('button#showbtn').show("slow");
});
</script>
<script>
function submitresponders() {
    avar = confirm('Theres no undo! Do you want to continue?');
    if (avar == true) {
        document.getElementById('submitrespond').submit();
    }
    if (avar == false) {
        location.reload();
    }
}


$(document).ready(function() {
    if ($(window).width() >= 768) {
        $('#sidebarToggleTop').trigger('click');
    }
    var calendar = $('#calendar').fullCalendar({
        editable: false,
        header: {
            left: '',
            center: 'title',
            right: 'prev,today,next'
        },

        events: '../../php_function/loadresident.php',
        displayEventTime: true,
        timeFormat: 'h a',
        customRender: true,

        eventRender: function(event, eventElement) {
            if (event.status == "Approve") {
                eventElement.addClass('mycolor');
            } else
            if (event.status == "denied") {
                eventElement.addClass('mycolors');
            } else
            if (event.status == "inactive") {
                eventElement.addClass('myclr');
            }
        },
        validRange: function(nowDate) {
            return {
                start: nowDate.clone().add(-1, 'day'),
                end: nowDate.clone().add(99999, 'months')
            };
        },
        selectOverlap: function(event) {
            return event.rendering === 'background';
        },
        selectable: false,

        eventClick: function(event) {
            var id = event.id;
            var status = event.status;
            if (status == 'Approve') {
                $('#approve' + id).modal('show');
            } else if (status == 'denied') {
                $('#denied' + id).modal('show');
            } else if (status == 'inactive') {
                $('#inactive' + id).modal('show');
                //$('#updatemodal' + id).modal('show');
                //$('#updatemodal').modal('show');
            }
        }
    });

});
</script>
<!-- Core plugin JavaScript-->
<script src="../../vendor/jquery-easing/jquery.easing.min.js "></script>

<!-- Custom scripts for all pages-->
<script src="../../components/js/sb-admin-2.min.js "></script>

<!-- Page level plugins -->
<script src="../../vendor/datatables/jquery.dataTables.min.js "></script>
<script src="../../vendor/datatables/dataTables.bootstrap4.min.js "></script>

<!-- Page level custom scripts -->
<script src="../../components/js/demo/datatables-demo.js "></script>

<!-- Page level plugins -->

</body>

</html>