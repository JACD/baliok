<?php include('../../php_function/session.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ECOLLECT</title>
    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../components/css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="../../components/img/logo/favicon-16x16.png">

    <style>
    @media screen and (min-width: 320px) and (max-width: 767px) and (orientation: portrait) {

        /* html {
            transform: rotate(-90deg);
            transform-origin: left top;
            width: 95vh;
            height: 100vw;
            overflow-x: hidden;
            position: absolute;
            top: 100%;
            left: 0;
        } */
        html {
            -ms-transform: rotate(-90deg);
            -webkit-transform: rotate(-90deg);
            transform: rotate(-90deg);
            width: 95vh;
            height: 100vw;
            overflow: scroll;
        }

        .modal-gamay {
            padding-right: 80px !important;
            padding-left: 80px !important;
        }
    }


    @media only screen and (max-width: 600px) {
        .fc-button-group {
            margin-right: 55px !important;
        }

        .formobile {
            margin-top: -25px;
            margin-bottom: 24%;
        }
    }

    .fc-day-grid-event {
        font-size: 15px;
        margin-top: 2px;
        padding: 10px;
        color: #f8f9fc !important;
    }

    .mycolor {
        background-color: #00a65a !important;
        border: 1px solid #00a65a !important;
        color: #fff !important;
        cursor: pointer !important;
    }

    .mycolors {
        background-color: #e74a3b !important;
        border: 1px solid #e74a3b !important;
        color: #fff !important;
        cursor: pointer !important;
    }

    .myclr {
        background-color: #3a87ad !important;
        border: 1px solid #3a87ad !important;
        color: #fff !important;
        cursor: pointer !important;
    }

    .colored {
        color: #e74a3b;
        font-size: 1rem;
    }

    #calendar {
        margin-top: -2rem !important;
    }

    .hidecancel {
        display: none !important;
    }

    .thiswillhide {
        display: none !important;
    }

    .hidethis {
        display: none !important;
    }

    .btnsave {
        display: none !important;
    }
    </style>
    <script>
    $(function() {
        $('.akongmodal').on('hidden.bs.modal', function() {
            $(".addhere").removeClass("thiswillhide");
            $(".showhere").addClass("showcancel");
            $(".hidebtnsave").addClass("btnsave");
            $(".btnaccept").removeClass("hidethis");
            $(".btncancel").removeClass("hidethis");
        })
        $(".btncancel").click(function() {
            $("div.addhere").hide("thiswillhide");
            $("div.showhere").show("showcancel");
            $(".hidebtnsave").removeClass("btnsave");
            $(".btnaccept").addClass("hidethis");
            $(".btncancel").addClass("hidethis");
        });

    });

    $(document).ready(function() {

        if ($(window).width() >= 768) {
            $('#sidebarToggleTop').trigger('click');
        }
        var calendar = $('#calendar').fullCalendar({
            editable: false,
            header: {
                left: '',
                center: 'title',
                right: 'prev,today,next'
            },

            events: '../../php_function/loadecoboy.php',
            displayEventTime: true,
            timeFormat: 'h a',
            customRender: true,

            eventRender: function(event, eventElement) {
                if (event.status == "Approve") {
                    eventElement.addClass('mycolor');
                } else
                if (event.status == "denied") {
                    eventElement.addClass('mycolors');
                } else
                if (event.status == "inactive") {
                    eventElement.addClass('myclr');
                }
            },
            validRange: function(nowDate) {
                return {
                    start: nowDate.clone().add(-1, 'day'),
                    end: nowDate.clone().add(99999, 'months')
                };
            },
            selectOverlap: function(event) {
                return event.rendering === 'background';
            },
            selectable: false,

            eventClick: function(event) {
                var id = event.id;
                var status = event.status;
                if (status == 'Approve') {
                    $('#approve' + id).modal('show');
                } else if (status == 'denied') {
                    $('#denied' + id).modal('show');
                } else if (status == 'inactive') {
                    $('#inactive' + id).modal('show');
                    //$('#updatemodal' + id).modal('show');
                    //$('#updatemodal').modal('show');
                }
            }
        });

    });
    </script>
</head>
<?php include '../../include/includecalendarhead.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Garbage Collection Schedule</h6>
        </center>

        <div class="card-body">
            <!-- DREA E SULOD ANG CONTENT -->


            <!-- Content Row -->
            <div class="row">

                <div class="col-xl-12 col-lg-8">
                    <div class="formobile">
                        <div class="mt-4 text-left small">
                            <span class="mr-2">
                                <i class="fas fa-circle text-primary"></i> PENDING
                            </span>
                            <span class="mr-2">
                                <i class="fas fa-circle text-success"></i> APPROVED
                            </span>
                            <span class="mr-2">
                                <i class="fas fa-circle text-danger"></i> DENIED
                            </span>
                        </div>
                    </div>
                    <div id="calendar"></div>
                </div>

                <!-- Donut Chart -->

            </div>
        </div>
    </div>

    <!-- /.container-fluid -->
</div>


<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal modal-approve modal-gamay" data-backdrop="false" id="approve<?php echo $row['Sched_No'];?>"
    style="z-index: 999 !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title" id="hidethislabel">Garbage Collection Schedule</h4>
                    <h4 class="modal-title" id="showthislabel" style="display: none;">Please put the weight/Net/kgs,
                        plate number and the name of driver.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" id="thisinfohide">
                    <div class="col-xl-12 col-lg-12">
                        <?php $status = $row['status'];
                            if($status == 'Approve') { ?>
                        <h5 style="color: #00a65a;"> Approved</h5>
                        <?php } ?>
                        <div class="form-group">
                            <h6><strong>Title</strong></h6>
                            <h5>
                                <?php echo $row['title'];?>
                            </h5>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-8">
                        <div class="form-group">
                            <h6><strong>Start Time</strong></h6>
                            <h5>
                                <?php echo date('h:i A', strtotime($row['StartTime']));?>
                            </h5>
                        </div>
                        <div class="form-group">
                            <h6><strong>End Time</strong></h6>
                            <h5>
                                <?php echo date('h:i A', strtotime($row['EndTime']));?>
                            </h5>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-8">
                        <div class="form-group">
                            <h6><strong>Purok No.</strong></h6>
                            <h5>
                                <?php echo $row['Purok_No'];?>
                            </h5>
                        </div>
                        <div class=" form-group ">
                            <h6><strong>Ecoboy Name</strong></h6>
                            <h5>
                                <?php echo $row['EcoboyName'];?>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="row" id="thisdumpshow" style="display:none;">

                    <div class="col-xl-12 col-lg-12">

                        <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                            <div class="form-group">
                                <label>Weight / Net / Kgs.</label>
                                <input type="number" value="<?php echo $row['weight_net_kgs']; ?>" class=" form-control"
                                    name="weyt" />
                            </div>
                            <div class="form-group">
                                <label>Plate Number</label>
                                <select class="form-control select2" name="pletno" required>
                                    <option value="<?php echo $row['garbage_truck_id']; ?>">
                                        <?php echo $row['plate_number']; ?>
                                    </option>
                                    <?php      	 
                                        $query = mysqli_query($db, "Select garbage_truck_id,Plate_number from garbagetruck;") or die(mysqli_error());
                                        while($fetch = mysqli_fetch_array($query))
                                        {   

                                        echo "<option value=".$fetch['garbage_truck_id'].">".$fetch['Plate_number']."</option>";

                                        } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Driver</label>
                                <select class="form-control select2" name="driver" required>
                                    <option value="<?php echo $row['driver']; ?>"><?php echo $row['driver']; ?>
                                    </option>
                                    <?php      	 
                            $jequery = mysqli_query($db, "Select * from brgydriver where deleted='0';") or die(mysqli_error());
                            while($rows = mysqli_fetch_array($jequery))
                            {   

                            echo "<option value='".$rows['fname'].' '.$rows['mname'].' '.$rows['lname']."'>".$rows['fname'].' '.$rows['mname'].' '.$rows['lname']."</option>";

                            } ?>
                                </select>
                                <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                            </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer ">

                    <button class="btn btn-danger" style="display: none;" id="showthisbtncancel" type="button">
                        Cancel</button>
                    <button class="btn btn-success" name="btn-plet" id="showsubmitbtn" type="submit"
                        style="display: none;"> Submit</button>
                    </form>
                    <button class="btn btn-success" id="hidetheshow" type="button"> Dump <i class="fas fa-dumpster">
                        </i></button>
                    <form action="responded_resident.php" method="POST">
                        <input type="hidden" name="skidid" value="<?php echo $row['Sched_No']; ?>" />
                        <button class="btn btn-primary" id="hidethisbtn" name="btn-res">For Pick Up
                            <i class="icon-for-pick-up"><img src='../../components/img/images/pickup.png'
                                    style="width: 20px; height: 20px;" /> </i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
  ?>
<div class="modal modal-gamay" data-backdrop="false" id="opendriver<?php echo $row['Sched_No']; ?>"
    style="z-index: 999 !important;">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Please put the weight/Net/kgs, plate number and the name of driver.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label>Weight / Net / Kgs.</label>
                        <input type="number" value="<?php echo $row['weight_net_kgs']; ?>" class=" form-control"
                            name="weyt" />
                    </div>
                    <div class="form-group">
                        <label>Plate Number</label>

                        <select class="form-control select2" name="pletno" required>
                            <option value="<?php echo $row['garbage_truck_id']; ?>">
                                <?php echo $row['plate_number']; ?>
                            </option>
                            <?php      	 
                            $query = mysqli_query($db, "Select garbage_truck_id,Plate_number from garbagetruck;") or die(mysqli_error());
                            while($fetch = mysqli_fetch_array($query))
                            {   

                            echo "<option value=".$fetch['garbage_truck_id'].">".$fetch['Plate_number']."</option>";

                            } ?>
                        </select>

                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                        <input type="hidden" value="<?php echo $row['days']; ?>" name="days" />
                    </div>
                    <div class="form-group">
                        <label>Driver</label>
                        <input type="text" value="<?php echo $row['driver']; ?>" class=" form-control" name="driver" />
                        <select class="form-control select2" name="driver" required>
                            <option value="<?php echo $row['driver']; ?>"><?php echo $row['driver']; ?>
                            </option>
                            <?php      	 
                            $jequery = mysqli_query($db, "Select * from brgydriver where deleted='0';") or die(mysqli_error());
                            while($rows = mysqli_fetch_array($jequery))
                            {   

                            echo "<option value=".$rows['fname'].' '.$rows['mname'].' '.$rows['lname'].">".$rows['fname'].' '.$rows['mname'].' '.$rows['lname']."</option>";

                            } ?>
                        </select>
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-plet" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>
<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal akongmodal modal-gamay" data-backdrop="false" id="inactive<?php echo $row['Sched_No'];?>"
    style="z-index: 999 !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Garbage Collection Schedule</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- Modal body -->
            <form action="../../php_function/schedulefunction.php" id="formaad" role="form" method="POST">
                <div class="modal-body">
                    <div class="row addhere">
                        <div class="col-xl-12 col-lg-12">
                            <div class="form-group">
                                <?php $status = $row['status'];
                                        if($status == 'Approve') { ?>
                                <h5 style="color: #00a65a;"> Approved</h5>
                                <?php } else if($status == 'denied') {?>
                                <h5 style="color: #e74a3b;"> Denied</h5>
                                <?php } else if($status == 'inactive') { ?>
                                <h5 style="color: #3a87ad;"> Not yet review</h5>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="hidden" class="form-control" id="aydah" name="idhere"
                                    value="<?php echo $row['Sched_No'];?>">
                                <input type="text" class="form-control" id="" name="title"
                                    value="<?php echo $row['title'];?>" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <label>Start Time</label>
                                <input type="time" class="form-control" id="" name="StartTime"
                                    value="<?php echo $row['StartTime'];?>" disabled="disabled">
                            </div>
                            <div class="form-group">
                                <label>End Time</label>
                                <input type="time" class="form-control" id="" name="EndTime"
                                    value="<?php echo $row['EndTime'];?>" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8">
                            <div class="form-group">
                                <label>Purok No.:</label>
                                <select class="form-control select2" id="" name="Purok_No" disabled="disabled">
                                    <option><?php echo $row['Purok_No'];?></option>
                                </select>
                            </div>
                            <div class=" form-group ">
                                <label>Ecoboy Name:</label>
                                <select class="form-control select2 form-control " id="" name="ecoboy"
                                    disabled="disabled">
                                    <option><?php echo $row['EcoboyName'];?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row showhere showcancel" style="display: none;">
                        <div class="col-md-12 col-lg-12">
                            <textarea name="reason" class="form-control" placeholder="Enter reason"></textarea>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit"
                            onclick="return confirm('Are you sure? Your will not able to revert this.');"
                            class="btn btn-success hidebtnaccept btnaccept" name="btnaccept" id="btnaccept">Accept
                        </button>
                        <button type="button" class="btn btn-danger hidebtncancel btncancel" name="btncancel"
                            id="btncancel">Decline </button>
                        <button type="submit" class="btn btn-primary hidebtnsave btnsave" name="btnsave"
                            id="btnsave">Save </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php } ?>
<?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
<div class="modal modal-gamay" data-backdrop="false" id="denied<?php echo $row['Sched_No'];?>"
    style="z-index: 999 !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Garbage Collection Schedule</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="form-group">
                            <?php $status = $row['status'];
                                        if($status == 'Approve') { ?>
                            <h5 style="color: #00a65a;"> Approved</h5>
                            <?php } else if($status == 'denied') {?>
                            <h5 style="color: #e74a3b;"> Denied</h5>
                            <?php } else if($status == 'inactive') { ?>
                            <h5 style="color: #3a87ad;"> Not yet review</h5>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12">
                        <p>
                            <?php echo $row['reason'].'.';?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer ">
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->

</div>
<!-- /.container-fluid -->

</div>
<?php include '../../include/footer.php';?>
</div>
</div>
<!-- End of Main Content -->
<!-- Bootstrap core JavaScript-->
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js "></script>
<script>
$(document).bind("touchmove", function(event) {
    event.preventDefault();
});
$("button#hidetheshow").click(function() {
    $('div#thisdumpshow').show("slow");
    $('div#thisinfohide').hide("slow");
    $('h4#hidethislabel').hide("slow");
    $('h4#showthislabel').show("slow");
    $("button#showthisbtncancel").show("slow");
    $("button#hidethisbtn").hide("slow");
    $("button#hidetheshow").hide("slow");
    $("button#showsubmitbtn").show("slow");
});
$("button#showthisbtncancel").click(function() {
    $('div#thisdumpshow').hide("slow");
    $('div#thisinfohide').show("slow");
    $('h4#hidethislabel').show("slow");
    $('h4#showthislabel').hide("slow");
    $("button#showthisbtncancel").hide("slow");
    $("button#hidethisbtn").show("slow");
    $("button#hidetheshow").show("slow");
    $("button#showsubmitbtn").hide("slow");
});
$('.modal-approve').on('hidden.bs.modal', function() {
    $('div#thisdumpshow').hide("slow");
    $('div#thisinfohide').show("slow");
    $('h4#hidethislabel').show("slow");
    $('h4#showthislabel').hide("slow");
    $("button#showthisbtncancel").hide("slow");
    $("button#hidethisbtn").show("slow");
    $("button#hidetheshow").show("slow");
    $("button#showsubmitbtn").hide("slow");
});
$('html').on('touchmove', function(event) {
    event.preventDefault();
});
</script>
<!-- Core plugin JavaScript-->
<script src="../../vendor/jquery-easing/jquery.easing.min.js "></script>

<!-- Custom scripts for all pages-->
<script src="../../components/js/sb-admin-2.min.js "></script>

<!-- Page level plugins -->
<script src="../../vendor/datatables/jquery.dataTables.min.js "></script>
<script src="../../vendor/datatables/dataTables.bootstrap4.min.js "></script>

<!-- Page level custom scripts -->
<script src="../../components/js/demo/datatables-demo.js "></script>

<!-- Page level plugins -->

</body>

</html>