    <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <?PHP include 'logo.php';?>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="./">
                <i class="fas fa-fw fa-home"></i>
                <span>Home</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">


        <!-- Nav Item - Ecoboy -->
        <li class="nav-item">
            <a class="nav-link" href="../../pages/prk/ecoboy.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Ecoboy Donation</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/prk/att_wednesday.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Ecoboy Attendance</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../../pages/prk/donation.php">
                <i class="fas fa-fw fa-table"></i>
                <span> Donation Claimed</span></a>
        </li>
        <!--li class="nav-item">
            <a class="nav-link" href="../../pages/prk/rate.php">
                <i class="fas fa-fw fa-table"></i>
                <span>Ecoboy Rate</span></a>
        </li-->

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->