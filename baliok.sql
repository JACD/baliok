-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: barangaybaliok
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accomplishment_report`
--

DROP TABLE IF EXISTS `accomplishment_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accomplishment_report` (
  `Accomp_No` int(11) NOT NULL AUTO_INCREMENT,
  `Date` varchar(20) NOT NULL,
  `Plate_No` varchar(50) NOT NULL,
  `Destination` varchar(50) NOT NULL,
  `Commodity` varchar(50) NOT NULL,
  `weight_net_kgs` varchar(20) NOT NULL,
  PRIMARY KEY (`Accomp_No`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accomplishment_report`
--

LOCK TABLES `accomplishment_report` WRITE;
/*!40000 ALTER TABLE `accomplishment_report` DISABLE KEYS */;
INSERT INTO `accomplishment_report` VALUES (4,'2019-05-22','P#38607/cabanog','CENR SANITARY\r\nLANDFILL D.C','SOLID WASTE','1205'),(5,'2019-05-22','P#38607/cabanog','CENR SANITARY LANDFILL D.C','SOLID WASTE','1023'),(6,'2019-05-22','P#38607/cabanog','CENR SANITARY LANDFILL D.C','SOLID WASTE','1550'),(7,'2019-05-22','P#38607/cabanog','CENR SANITARY LANDFILL D.C','SOLID WASTE','1550'),(8,'2019-10-09','P#1000/Jomari','CENR SANITARY LANDFILL D.C','SOLID WASTE','100'),(9,'2019-09-25','P#38607/Lambanog','CENR SANITARY LANDFILL D.C','SOLID WASTE','4500'),(10,'2019-10-30','P#1001/Jomari','CENR SANITARY LANDFILL D.C','SOLID WASTE','1000'),(11,'2019-11-06','P#1001/Jason','CENR SANITARY LANDFILL D.C','SOLID WASTE','1000');
/*!40000 ALTER TABLE `accomplishment_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `ses_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(10) NOT NULL,
  `auth` varchar(10) NOT NULL,
  PRIMARY KEY (`ses_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,'jie@jie','jie','Jie','Jie','user.png','admin');
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgycaptain`
--

DROP TABLE IF EXISTS `brgycaptain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgycaptain` (
  `brgycaptain_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` varchar(50) NOT NULL,
  `yearend` varchar(50) DEFAULT NULL,
  `current` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brgycaptain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgycaptain`
--

LOCK TABLES `brgycaptain` WRITE;
/*!40000 ALTER TABLE `brgycaptain` DISABLE KEYS */;
INSERT INTO `brgycaptain` VALUES (16,'11','sd asd','2018-03-05','2019-10-08','former barangay captain','2019-10-08 09:29:22'),(17,'19','jomari duran','2019-10-08','2019-10-29','former barangay captain','2019-10-29 10:35:42'),(18,'11','sd asd','2019-10-02',NULL,'Current Captain','2019-10-29 10:35:56');
/*!40000 ALTER TABLE `brgycaptain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgykagawad`
--

DROP TABLE IF EXISTS `brgykagawad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgykagawad` (
  `brgykagawad_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` varchar(5) NOT NULL,
  `rank` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` varchar(50) DEFAULT NULL,
  `yearend` varchar(50) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brgykagawad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgykagawad`
--

LOCK TABLES `brgykagawad` WRITE;
/*!40000 ALTER TABLE `brgykagawad` DISABLE KEYS */;
INSERT INTO `brgykagawad` VALUES (8,'9','Rank 1','firstname lastname','2019-05-01',NULL,'2019-05-18 16:00:48');
/*!40000 ALTER TABLE `brgykagawad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgypersonnel`
--

DROP TABLE IF EXISTS `brgypersonnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgypersonnel` (
  `personnel_id` int(11) NOT NULL AUTO_INCREMENT,
  `resident_id` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `brgyposition` varchar(50) NOT NULL,
  `yeardeclared` varchar(15) NOT NULL,
  `yearend` varchar(15) NOT NULL,
  PRIMARY KEY (`personnel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgypersonnel`
--

LOCK TABLES `brgypersonnel` WRITE;
/*!40000 ALTER TABLE `brgypersonnel` DISABLE KEYS */;
INSERT INTO `brgypersonnel` VALUES (5,'12','good Jv','Ecoboy','2019-05-13',''),(8,'14','HHHH wkol','Purok Leader','2018-10-18',''),(9,'18','sdf sdf','Ecoboy','2019-10-08',''),(10,'20','derick dayrit','Ecoboy','2019-10-30','');
/*!40000 ALTER TABLE `brgypersonnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgyposition`
--

DROP TABLE IF EXISTS `brgyposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgyposition` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `position` varchar(50) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgyposition`
--

LOCK TABLES `brgyposition` WRITE;
/*!40000 ALTER TABLE `brgyposition` DISABLE KEYS */;
INSERT INTO `brgyposition` VALUES (2,'This is the description of the purok leader','Purok Leader','2018-03-07'),(4,'This is the description of the ecoboy','Ecoboy','07-13-2019');
/*!40000 ALTER TABLE `brgyposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgyreceipt`
--

DROP TABLE IF EXISTS `brgyreceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgyreceipt` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `idsked` varchar(5) DEFAULT NULL,
  `weigh_in` varchar(20) NOT NULL,
  `plate_no` varchar(20) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `driver` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL,
  `gross_tare` varchar(20) NOT NULL,
  `total` varchar(20) NOT NULL,
  `commodity` varchar(20) NOT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgyreceipt`
--

LOCK TABLES `brgyreceipt` WRITE;
/*!40000 ALTER TABLE `brgyreceipt` DISABLE KEYS */;
INSERT INTO `brgyreceipt` VALUES (1,'12','02:10 pm, 08/10/2019','P#1000','joy','Jomari','toril','100','300','Solid Waste'),(2,'14','06:10 pm, 09/10/2019','P#10010','Jomari','ryan','Toril','150','450','Solid waste'),(3,'16','03:10 pm, 28/10/2019','P#1001','Cherry','Jomari','Toril','1000','3000','Solid waste'),(4,'18','02:10 pm, 30/10/2019','P#1001','Cherry','Jason','Daliao','3000','12000','Solid waste');
/*!40000 ALTER TABLE `brgyreceipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brgyuseragent`
--

DROP TABLE IF EXISTS `brgyuseragent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brgyuseragent` (
  `useragent_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  `auth` varchar(20) NOT NULL,
  PRIMARY KEY (`useragent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brgyuseragent`
--

LOCK TABLES `brgyuseragent` WRITE;
/*!40000 ALTER TABLE `brgyuseragent` DISABLE KEYS */;
INSERT INTO `brgyuseragent` VALUES (2,'User1','Agent1','user1@gmail.com','012345678','09-02-2019','landfill'),(3,'User2','Agent2','user2@gmail.com','012345678','09-03-2019','CENRO');
/*!40000 ALTER TABLE `brgyuseragent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation`
--

DROP TABLE IF EXISTS `donation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation` (
  `pledge_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `res_id` varchar(5) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `ecoboyname` varchar(50) NOT NULL,
  `econickname` varchar(20) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `confirmation` varchar(10) DEFAULT NULL,
  `new` varchar(5) NOT NULL,
  `date` varchar(20) DEFAULT NULL,
  `reset` varchar(10) NOT NULL,
  PRIMARY KEY (`pledge_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation`
--

LOCK TABLES `donation` WRITE;
/*!40000 ALTER TABLE `donation` DISABLE KEYS */;
INSERT INTO `donation` VALUES (22,'ACT PBA','13','12','good Jv','Boyet',25.00,'Recieved','old','10-04-2019','reset'),(23,'ACT PBA','13','18','sdf sdf','juan',20.00,'Recieved','old','10-08-2019','reset'),(24,'ACT PBA','13','18','sdf sdf','juan',25.00,'Recieved','old','10-28-2019',''),(25,'ACT PBA','13','20','derick dayrit','asd',30.00,'Recieved','old','10-30-2019',''),(26,'ACT PBA','13','18','sdf sdf','juan',60.00,'Recieved','old','10-30-2019',''),(27,'rrr rrr','21','18','sdf sdf','juan',35.00,'Recieved','old','10-30-2019',''),(28,'rrr rrr','21','12','good Jv','Boyet',100.00,'Recieved','old','10-30-2019',''),(29,'ACT PBA','13','12','good Jv','Boyet',50.00,'Recieved','old','10-30-2019','');
/*!40000 ALTER TABLE `donation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donation_history`
--

DROP TABLE IF EXISTS `donation_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donation_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `eco_id` varchar(5) NOT NULL,
  `eco_name` varchar(50) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donation_history`
--

LOCK TABLES `donation_history` WRITE;
/*!40000 ALTER TABLE `donation_history` DISABLE KEYS */;
INSERT INTO `donation_history` VALUES (3,'12','good Jv','5000.00','30-09-2019'),(4,'12','good Jv','25.00','10-04-2019'),(5,'18','sdf sdf','20.00','10-08-2019');
/*!40000 ALTER TABLE `donation_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecoboy_claimed_garbage`
--

DROP TABLE IF EXISTS `ecoboy_claimed_garbage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecoboy_claimed_garbage` (
  `garbage_id` int(5) NOT NULL AUTO_INCREMENT,
  `Ecoboy_nym` varchar(50) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `resid` varchar(5) NOT NULL,
  `resname` varchar(50) NOT NULL,
  `claim` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `skid` varchar(5) NOT NULL,
  `cdate` varchar(50) DEFAULT NULL,
  `date_collected` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`garbage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecoboy_claimed_garbage`
--

LOCK TABLES `ecoboy_claimed_garbage` WRITE;
/*!40000 ALTER TABLE `ecoboy_claimed_garbage` DISABLE KEYS */;
INSERT INTO `ecoboy_claimed_garbage` VALUES (10,'good Jv','12','','','Yes','','8','',NULL),(7,'good Jv','12','13','ACT PBA','No','asdfl','8','wednesday',NULL),(9,'good Jv','12','','','Yes','','','',NULL),(11,'good Jv','12','134','ACT PBA','No','','8','wednesday',NULL),(12,'good Jv','12','','','Yes','','11','',NULL),(13,'good Jv','12','','','Yes','','11','',NULL),(14,'sdf sdf','18','','','Yes','','12','',NULL),(15,'good Jv','12','','','Yes','','14','',NULL),(16,'good Jv','12','','','Yes','','14','',NULL),(17,'good Jv','12','','','Yes','','8','',NULL),(18,'good Jv','12','','','Yes','','11','',NULL),(19,'good Jv','12','','','Yes','','14','',NULL),(20,'good Jv','12','13','ACT PBA','Yes','','16','wednesday','28-10-2019'),(21,'good Jv','12','13','ACT PBA','Yes','','8','wednesday','28-10-2019'),(22,'good Jv','12','13','ACT PBA','Yes','','11','saturday','28-10-2019'),(23,'good Jv','12','13','ACT PBA','Yes','','14','sunday','28-10-2019'),(24,'good Jv','12','13','ACT PBA','Yes','','18','wednesday','30-10-2019');
/*!40000 ALTER TABLE `ecoboy_claimed_garbage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garbageprice`
--

DROP TABLE IF EXISTS `garbageprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garbageprice` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_value` double(10,2) NOT NULL,
  `price_status` enum('0','1') NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garbageprice`
--

LOCK TABLES `garbageprice` WRITE;
/*!40000 ALTER TABLE `garbageprice` DISABLE KEYS */;
INSERT INTO `garbageprice` VALUES (1,3.00,'0','09-09-2019'),(2,1.00,'0','09-09-2019'),(3,2.00,'0','09-09-2019'),(4,4.00,'1','09-09-2019'),(5,5.00,'0','09-09-2019'),(6,6.00,'0','09-09-2019'),(7,9.00,'0','25-10-2019');
/*!40000 ALTER TABLE `garbageprice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garbagetruck`
--

DROP TABLE IF EXISTS `garbagetruck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garbagetruck` (
  `garbage_truck_id` int(11) NOT NULL AUTO_INCREMENT,
  `Garbage_truck_weight` varchar(20) NOT NULL,
  `Plate_number` varchar(30) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`garbage_truck_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garbagetruck`
--

LOCK TABLES `garbagetruck` WRITE;
/*!40000 ALTER TABLE `garbagetruck` DISABLE KEYS */;
INSERT INTO `garbagetruck` VALUES (1,'2000','P#1001','2019-10-28'),(2,'2050','P#1002','2019-10-28'),(3,'1900','P#1003','2019-10-30');
/*!40000 ALTER TABLE `garbagetruck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marker`
--

DROP TABLE IF EXISTS `marker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marker` (
  `marker_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `img` varchar(20) NOT NULL,
  `lat` varchar(60) NOT NULL,
  `lng` varchar(60) NOT NULL,
  `status` varchar(3) NOT NULL,
  PRIMARY KEY (`marker_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marker`
--

LOCK TABLES `marker` WRITE;
/*!40000 ALTER TABLE `marker` DISABLE KEYS */;
INSERT INTO `marker` VALUES (1,'Jojie Avergonzado','Koala.jpg','7.045549','125.498328','yes'),(2,'James Aversulo','user.png','7.046166','125.499358','yes'),(3,'Fumi Yam','user.png','7.045554','125.499841','yes'),(4,'isa pa','user.png','7.0464','125.500796','yes');
/*!40000 ALTER TABLE `marker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `rateid` int(11) NOT NULL AUTO_INCREMENT,
  `ecoboyname` varchar(50) DEFAULT NULL,
  `score` varchar(10) DEFAULT NULL,
  `residentname` varchar(50) DEFAULT NULL,
  `ecoboyid` varchar(5) DEFAULT NULL,
  `resid` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`rateid`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (18,'good Jv','2','ACT PBA','12','13'),(19,'sdf sdf','4','ACT PBA','18','13'),(20,'derick dayrit','4','ACT PBA','20','13'),(21,'sdf sdf','4','rrr rrr','18','21'),(22,'good Jv','5','rrr rrr','12','21');
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resident`
--

DROP TABLE IF EXISTS `resident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Brgy_ID` varchar(15) NOT NULL,
  `Precinct_No` varchar(10) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `suffix` varchar(5) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `capability` varchar(25) DEFAULT NULL,
  `assistedby` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` varchar(3) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` int(5) NOT NULL,
  `province` varchar(50) NOT NULL,
  `citymuni` varchar(50) NOT NULL,
  `barangay` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `noofyears` int(5) NOT NULL,
  `noofmonths` int(5) NOT NULL,
  `numofyears` int(5) NOT NULL,
  `citizenship` varchar(25) NOT NULL,
  `citizenshipby` varchar(25) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `cmuni` varchar(50) NOT NULL,
  `cprovince` varchar(50) NOT NULL,
  `don` varchar(20) NOT NULL,
  `cnoa` varchar(25) NOT NULL,
  `rdon` varchar(20) NOT NULL,
  `cnooa` varchar(25) NOT NULL,
  `occupation` varchar(25) NOT NULL,
  `tin` varchar(15) NOT NULL,
  `civilstatus` varchar(20) NOT NULL,
  `spouse` varchar(25) NOT NULL,
  `flname` varchar(50) NOT NULL,
  `ffname` varchar(50) NOT NULL,
  `fmname` varchar(50) NOT NULL,
  `mlname` varchar(50) NOT NULL,
  `mfname` varchar(50) NOT NULL,
  `mmname` varchar(50) NOT NULL,
  `hotf` varchar(50) NOT NULL,
  `hotfname` varchar(50) DEFAULT NULL,
  `family_verification` varchar(5) DEFAULT NULL,
  `names` varchar(500) NOT NULL,
  `Purok_No` varchar(15) NOT NULL,
  `purokleader` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `re_pass` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `position` varchar(20) DEFAULT NULL,
  `rate` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resident`
--

LOCK TABLES `resident` WRITE;
/*!40000 ALTER TABLE `resident` DISABLE KEYS */;
INSERT INTO `resident` VALUES (9,'19-000','0000-0','lastname','firstname','middlename','Jr.','nickname','','','Male','21','52',50,'province','city','barangay','house',6,6,6,'Filipino','','0000-00-00','','','0000-00-00','','0000-00-00','','none','154_282_264',' Single','','lastname','firstname','middlename','lastname','firstname','middlename','Yes','9','head','add,adda,addaa','10','Vicentita Ocay','kagawad@gmail.com','kag','wer','active','Koala.jpg','Kagawad',NULL),(11,'asd','adsf','asd','sd','w','Jr.','we','','','Female','','4',3,'asdf','asdf','adsf','asdf',4,3,2,'as','','0000-00-00','','','0000-00-00','','0000-00-00','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','','10','','','10','Vicentita Ocay','cap@gmail.com','cap','ty','active','user.png','Captain',NULL),(12,'1545','452','Jv','good','is','Jr.','Boyet','','','Female','','4',3,'asdf','asdf','adsf','asdf',4,3,2,'as','','2019-05-08','','','2019-05-01','','2019-05-15','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','','10','','','10','Vicentita Ocay','eco@gmail.com','eco','jv','active','user.png','Ecoboy','3'),(13,'7895','658','PBA','ACT','cis','Jr.','the','','','Female','','4',3,'asdf','asdf','adsf','asdf',4,3,2,'as','','2019-05-08','','','2019-05-01','','2019-05-15','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','No','9','Yes','','10','Vicentita Ocay','res@gmail.com','res','res','active','user.png','',NULL),(14,'7895','658','wkol','HHHH','esx','','','','','Female','','4',3,'asdf','asdf','adsf','asdf',4,3,2,'as','','2019-05-08','','','2019-05-01','','2019-05-15','','asdf','34344',' Single','','asdf','asdf','','w','aw','xc','No','9','Yes','','10','Vicentita Ocay','prk@gmail.com','prk','dec','active','user.png','Purok Leader',NULL),(15,'19-000','000-0','asdf','sdfa','ss','Jr.','jie','','','Male','8','43',23,'No Selection','Davao City','sd','12 LOWER LUBOGAN',4,2,2,'Philippines','ByBirth','2011-07-06','Davao City','No Selection','','','','','wala lagi','000-000-000',' Single','','dfs','asdf','asdf','asdf','asdf','asdf','Yes','15','head','dssd','RV3 8-A','Emma Castillano','jomari@gmail.com','password','password','active','user.png','',NULL),(16,'19-0001','0000-1','o','s','d','Jr.','d','','','Male','49','3',3,'No Selection','Davao City','j','12 LOWER LUBOGAN',2,1,2,'Philippines','','','','','','','','','','',' Single','','er','wer','wer','wer','wer','we','No','9','Yes','','2','Jessa Mutia','sample@gmail.com','password','password','active','user.png','',NULL),(17,'12321312','12321312','sad','asfdafs','asfas','Jr.','asdas','Person with Disabilty','asdasd','Male','20','123',123,'asdas','asdas','asdas','asdasdas',123,12,12,'asdasdsa','ByBirth','1999-12-12','sadasd','asdas','','','','','asdas123','12312213',' Single','sadsa','asdas','asdas','asdasd','asdas','asdas','asdas','Yes','',NULL,'','RV3 8-B','Virginia Avelino','jasongaleon1996@gmail.com','jgaleon1996','jgaleon1996','decline','user.png','',NULL),(18,'13-4545','15478-8','sdf','sdf','swedrfs','Jr.','juan','Illiterate','sadd','Female','21','112',50,'sdf','dsf','sdf','12',21,8,21,'filipino','ByBirth','1998-01-17','dsaf','sdf','1998-01-17','23231','','','sdf','154-548-510','Married','sadsdf','sdf','dfd','cdfg','ewrer','werw','cdfgdf','Yes','18','head','erwe','5','Edwin Gallera','cherryllamido10@gmail.com','cherry123','cherry123','active','Penguins.jpg','Ecoboy',NULL),(19,'123123121231','123123','duran','jomari','qwe','Jr.','acasa','Illiterate','asdasd','Male','807','1212',123,'sadasd','asdas','asdas','asdasd',12,122,12,'asd','ByBirth','1212-12-12','asdasd','asdas','','','','','asdasd','11233','Married','asdsad','asdasd','asdasd','saddas','asdas','dasdas','dasd','Yes','19','head','','RV3 8-B','Virginia Avelino','dcjomari@gmail.com','jomari1234','jomari1234','active','user.png','',NULL),(20,'12321312','123123','dayrit','derick','duran','Jr.','asd','Illiterate','fasfdaf','Male','23','12',32,'asdsa','asdas','asdas','das',12,12,123,'asdas','ByBirth','1996-12-12','asdas','sadfas','','','','','asdasd','12312312',' Single','asd','asdasd','asdsad','sadasd','asdsa','asdas','asd','Yes','20','head','','RV3 8-B','Virginia Avelino','derickdayrit1993@gmail.com','012345678','012345678','active','user.png','Ecoboy',NULL),(21,'12414131','213123123','rrr','rrr','rrr','Jr.','rrrr','Illiterate','12312asd','Male','0','2131',213123,'asdsad','213213asdasdasd','asdsadsa','dadasd',12,12,12,'fdfdasd','ByBirth','2019-10-19','sad','asd','','','','','asdsad','12312312321',' Single','123213','adas','sadsa','dasdas','dasdas','dasdasd','asdasdasdasd','Yes','21','head','rrrrrrr','RV3 8-B','Virginia Avelino','duranryan3397@gmail.com','012345678','012345678','active','user.png','',NULL);
/*!40000 ALTER TABLE `resident` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resident_responded`
--

DROP TABLE IF EXISTS `resident_responded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resident_responded` (
  `respond_id` int(11) NOT NULL AUTO_INCREMENT,
  `sked_id` varchar(5) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `rest_id` varchar(5) NOT NULL,
  `res_name` varchar(50) NOT NULL,
  `garbage` varchar(5) NOT NULL,
  `reason` text,
  `week` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `days` varchar(20) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `img` text NOT NULL,
  `lat` text NOT NULL,
  `lng` text NOT NULL,
  `garbage_type` varchar(20) NOT NULL,
  `if_collected` varchar(5) DEFAULT NULL,
  `date_collected` varchar(20) DEFAULT NULL,
  `seen` varchar(1) NOT NULL,
  PRIMARY KEY (`respond_id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resident_responded`
--

LOCK TABLES `resident_responded` WRITE;
/*!40000 ALTER TABLE `resident_responded` DISABLE KEYS */;
INSERT INTO `resident_responded` VALUES (30,'8','12','134','ACT PBA','Yes',NULL,'week1','2019-05-22','wednesday','2019-08-18 07:39:28','user.png','7.0264475','125.499279','',NULL,NULL,''),(39,'8','12','13','ACT PBA','Yes',NULL,'week1','2019-05-22','wednesday','2019-09-12 14:08:52','user.png','7.0264475','125.499279','sw','Yes','28-10-2019','1'),(40,'11','12','13','ACT PBA','Yes',NULL,'week1','2019-10-05','saturday','2019-10-04 02:00:02','user.png','6.6420736','124.6527488','sw','Yes','28-10-2019','1'),(41,'12','18','13','ACT PBA','Yes',NULL,'week2','2019-10-09','wednesday','2019-10-08 05:56:01','user.png','','','mw',NULL,NULL,''),(42,'14','12','13','ACT PBA','Yes',NULL,'week2','2019-10-13','sunday','2019-10-08 09:45:36','user.png','7.0242181','125.4891363','rw','Yes','28-10-2019','0'),(43,'15','18','13','ACT PBA','Yes',NULL,'week3','2019-10-23','wednesday','2019-10-28 06:58:27','user.png','7.0264475','125.499279','sw','','',''),(44,'16','12','13','ACT PBA','Yes',NULL,'week4','2019-10-30','wednesday','2019-10-28 06:58:38','user.png','7.0264475','125.499279','mw','Yes','28-10-2019','0'),(45,'9','12','13','ACT PBA','Yes',NULL,'week1','2019-09-25','wednesday','2019-10-28 08:01:26','user.png','7.0264475','125.499279','sw','','',''),(46,'18','12','13','ACT PBA','Yes',NULL,'week1','2019-11-06','wednesday','2019-10-30 05:53:54','user.png','7.0264475','125.499279','sw','Yes','30-10-2019','0');
/*!40000 ALTER TABLE `resident_responded` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `schedule` (
  `Sched_No` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `CollDateStart` varchar(50) NOT NULL,
  `CollDateEnd` varchar(50) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Purok_No` varchar(10) NOT NULL,
  `EcoboyName` varchar(100) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `status` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `week_no` varchar(10) NOT NULL,
  `days` varchar(20) NOT NULL,
  `plate_number` varchar(20) DEFAULT NULL,
  `driver` varchar(50) DEFAULT NULL,
  `weight_net_kgs` varchar(20) DEFAULT NULL,
  `accomplish` varchar(5) DEFAULT NULL,
  `loading_cenro` varchar(20) DEFAULT NULL,
  `loading_date_ecoboy` varchar(20) DEFAULT NULL,
  `loading_date_cenro` varchar(20) DEFAULT NULL,
  `loading_ecoboy` varchar(20) DEFAULT NULL,
  `loading_landfill` varchar(20) DEFAULT NULL,
  `landfill_confirmation` varchar(1) DEFAULT NULL,
  `confirm_date_landfill` varchar(20) DEFAULT NULL,
  `receipt_kagawad` varchar(1) DEFAULT NULL,
  `garbage_truck_weight` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Sched_No`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (8,'2019-05-22','02:34:00','15:54:00','RV3 7-B','good Jv','12','Approve','Approved','2019-10-13 02:25:07','week1','wednesday','','a','150','ok','load','08-10-2019, 5:50 pm','08-10-2019, 5:52 pm','load','load','1','08-10-2019, 5:52 pm','1',NULL),(9,'2019-09-25','14:01:00','14:00:00','6','good Jv','12','Approve','Approved','2019-10-09 10:07:31','week1','wednesday','P#38607','Lambanog','4500','ok','load','03-10-2019, 11:32 pm','03-10-2019, 11:38 pm','load','load','1','04-10-2019, 12:28 am','1',NULL),(10,'2019-10-26','01:00:00','17:00:00','4','good Jv','12','Approve','Approved','2019-10-30 07:04:44','week4','saturday',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'2019-10-05','03:04:00','16:05:00','RV3 7-B','good Jv','12','Approve','Approved','2019-10-08 06:31:32','week1','saturday','','','1500',NULL,'load','04-10-2019, 10:38 am','04-10-2019, 10:38 am','load','load','1','04-10-2019, 10:39 am','1',NULL),(12,'2019-10-09','08:00:00','10:00:00','RV3 7-A','sdf sdf','18','Approve','Approved','2019-10-08 06:31:39','week2','wednesday','P#1000','Jomari','100','ok','load','08-10-2019, 2:00 pm','08-10-2019, 2:01 pm','load','load','1','08-10-2019, 2:01 pm','1',NULL),(13,'2019-10-13','04:05:00','17:00:00','4','sdf sdf','18','denied','not feeling well','2019-10-08 09:42:31','week2','sunday',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'2019-10-13','04:05:00','17:00:00','2','good Jv','12','Approve','Approved','2019-10-09 10:06:52','week2','sunday','P#10010','ryan','150',NULL,'load','09-10-2019, 6:05 pm','09-10-2019, 6:06 pm','load','load','1','09-10-2019, 6:06 pm','1',NULL),(15,'2019-10-23','04:00:00','15:00:00','6','sdf sdf','18','Approve','Approved','2019-10-21 03:53:11','week3','wednesday',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'2019-10-30','04:00:00','17:00:00','2','good Jv','12','Approve','Approved','2019-10-28 07:06:07','week4','wednesday','P#1001','Jomari','1000','ok','load','28-10-2019, 3:04 pm','28-10-2019, 3:04 pm','load','load','1','28-10-2019, 3:04 pm','1','2000'),(17,'2019-11-24','01:00:00','12:00:00','3','sdf sdf','18','Approve','Approved','2019-10-29 11:22:12','week4','sunday',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'2019-11-06','05:04:00','15:00:00','1','good Jv','12','Approve','Approved','2019-10-30 06:25:18','week1','wednesday','P#1001','Jason','1000','ok','load','30-10-2019, 2:13 pm','30-10-2019, 2:13 pm','load','load','1','30-10-2019, 2:13 pm','1','2000'),(19,'2019-11-13','04:05:00','16:00:00','6','sdf sdf','18','Approve','Approved','2019-10-30 07:05:50','week2','wednesday',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'2019-11-20','04:00:00','16:00:00','RV3 8-B','sdf sdf','18','inactive','Not yet review','2019-10-30 07:07:20','week3','wednesday',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-20 15:34:08
