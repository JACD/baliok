<?php include '../../include/mainincludetop.php';?>

<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-success-800">Profile</h1>
</div>

<!------========DREA E BUTANG ANG MGA CONTENT=====----->
<?php
      $profile = mysqli_query($db,"SELECT * FROM resident where id='".$_GET['ayde']."';");
      while($row = mysqli_fetch_array($profile))
       {
           $familyhead=$row['hotfname'];   
      ?>
<!-- Content Row -->
<div class="row">
    <!-- Pending Requests Card Example -->
    <div class="col-xl-2 col-md-6 mb-4"></div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <a data-toggle="modal" data-target="<?php echo $row['id'];?>">
            <img src="../../components/img/profile/<?php echo $row['photo'];?>" class="img-profile rounded-circle"
                width="150" height="150" />
        </a>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
        <h4><?php echo $row['fname'].' '. $row['mname'].' '. $row['lname'].' ('. $row['nickname'].')'; ?> </h4>
        <hr />
        <h5><?php echo $row['Brgy_ID'];?></h5>
        <h5>Age: <?php echo $row['age'];?></h5>
        <h5>Gender: <?php echo $row['gender'];?></h5>
        <h5>Suffix: <?php echo $row['suffix'];?></h5>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4"></div>
</div>

<!-- Content Row -->

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-1 col-lg-3"></div>
    <div class="col-xl-6 col-lg-3">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Account Overview</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Email:</th>
                            <th><?php echo $row['email'];?></th>
                        </tr>
                        <tr>
                            <th class="active">Birthday:</th>
                            <th class="success"><?php echo $row['dob'];?></th>
                        </tr>
                        <tr>
                            <th class="active">Weight:</th>
                            <th class="success"><?php echo $row['weight'];?> kls.</th>
                        </tr>
                        <tr>
                            <th class="active">Height:</th>
                            <th class="success"><?php echo $row['height'];?> cm</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-3">
        <div class="card shadow mb-4" style="display: none;">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form action="../../php_function/changepassword.php" method="POST">
                    <input type="password" class="form-control" name="cpass" placeholder="Enter current password"
                        required />
                    <br />
                    <input type="password" class="form-control" name="newpass" placeholder="Enter new password"
                        pattern=".{8,12}" required title="8 to 12 characters" required />
                    <br />
                    <input type="password" class="form-control" name="rnewpass" placeholder="Retype new password"
                        pattern=".{8,12}" required title="8 to 12 characters" required />
                    <br />
                    <button type="submit" name="btn-pass" class="form-control btn btn-success">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-xl-1 col-lg-3"></div>
</div>

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-1 col-lg-3"></div>
    <div class="col-xl-10 col-lg-3">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Personal Background</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="form-group row">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">Parent's Name</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">Address</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu2">Other info</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div id="home" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Mother's Name:</th>
                                    <th></th>
                                    <th>Father's Name:</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>First Name:</td>
                                    <td><?php echo $row['mfname'];?></td>
                                    <td>First Name:</td>
                                    <td><?php echo $row['ffname'];?></td>
                                </tr>
                                <tr>
                                    <td>Middle Name:</td>
                                    <td><?php echo $row['mmname'];?></td>
                                    <td>Middle Name:</td>
                                    <td><?php echo $row['fmname'];?></td>
                                </tr>
                                <tr>
                                    <td class="active">Last Name:</td>
                                    <td class="success"><?php echo $row['mlname'];?></td>
                                    <td class="active">Last Name:</td>
                                    <td class="success"><?php echo $row['flname'];?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="menu1">
                        <div class="table-responsive">
                            <table class="table">

                                <tr>
                                    <th>Province:</th>
                                    <td><?php echo $row['province'];?></td>
                                    <th>Purok:</th>
                                    <td><?php echo $row['Purok_No'];?></td>
                                </tr>
                                <th>Municipality:</th>
                                <td><?php echo $row['citymuni'];?></td>
                                <th>Purok Leader:</th>
                                <td><?php echo $row['purokleader'];?></td>
                                </tr>
                                <tr>
                                    <th>Barangay:</th>
                                    <td><?php echo $row['barangay'];?></td>
                                    <th>House No./Street:</th>
                                    <td><?php echo $row['street'];?></td>
                                </tr>
                                <tr>
                                    <th>PERIOD OF RESIDENCEY:</th>
                                    <th></th>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th class="active">Number of years in City:</th>
                                    <td class="success"><?php echo $row['noofyears'];?></td>
                                    <th class="active">Number of years in Philippines:</th>
                                    <td class="success"><?php echo $row['numofyears'];?></td>
                                </tr>
                                <tr>
                                    <th class="active">Number of months in City:</th>
                                    <td class="success"><?php echo $row['noofmonths'];?></td>
                                    <th class="active">Citizenship:</th>
                                    <td class="success"><?php echo $row['citizenship'];?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="menu2">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Spouse Name:</th>
                                    <td><?php echo $row['spouse'];?></td>
                                    <th>Civil status:</th>
                                    <td><?php echo $row['civilstatus'];?></td>
                                </tr>
                                <tr>
                                    <th>Occupation:</th>
                                    <td><?php echo $row['occupation'];?></td>
                                    <th>Tin number:</th>
                                    <td><?php echo $row['tin']; } ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-1 col-lg-3"></div>
</div>

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-1 col-lg-3"></div>
    <div class="col-xl-10 col-lg-3">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Family Member</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>

                            <?php
          $family = mysqli_query($db,"SELECT * FROM resident where hotfname='$familyhead' and (hotf='' OR hotf='Yes');");
          while($rows = mysqli_fetch_array($family))
          {   
          ?>
                            <tr>
                                <td>
                                    <?php echo $rows['fname'].' '. $rows['lname'];?>
                                </td>


                                <?php if($rows['family_verification']==''){ ?>

                                <td>
                                </td>
                                <?php } else if($rows['family_verification']=='Yes'){ ?>
                                <td>
                                </td>
                                <?php } else if($rows['family_verification']=='No'){ ?>
                                <td>
                                </td>
                                <?php } else if($rows['family_verification']=='head'){ ?>
                                <td>
                                    FAMILY HEAD
                                </td>
                                <?php } ?>

                            </tr>

                            <?php
        									}
        									?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
			$photos = mysqli_query($db,"SELECT * FROM resident;");
			while($photorow = mysqli_fetch_array($photos))
			{   
			?>
<!-- update Modal-->
<div class="modal fade" id="editModal<?php echo $photorow['id'];?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Photo</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-6">
                        <form action="../../php_function/function_profile_photo.php" method="POST"
                            enctype="multipart/form-data">
                            <img src="../../components/img/profile/<?php echo $photorow['photo'];?>"
                                class="img-profile rounded-circle" width="150" height="150" />
                    </div>
                    <div class="col-xl-6">
                        <div style="color:blue; font-size:15px; margin-top:20%;">
                            <input type="file" name="image" accept="image/*;capture=camera">
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" onclick="return confirm('Do you want to update?');" name="btn-upload"
                    class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<!------========DREA RA TAMAN E BUTANG ANG MGA CONTENT=====----->

<script src="../../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="../../components/js/demo/chart-area-demo.js"></script>
<script src="../../components/js/demo/chart-pie-demo.js"></script>

<?php include '../../include/mainincludebottom.php';?>