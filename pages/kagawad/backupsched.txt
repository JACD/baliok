<?php include('../../php_function/session.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ECOLLECT</title>
    <!-- Custom fonts for this template -->
    <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../components/css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="../../components/img/logo/favicon-16x16.png">
    <style>
        .fc-day-grid-event {
            font-size: 15px;
            margin-top: 2px;
            padding: 10px;
            color: #f8f9fc !important;
        }
        
        body {
            overflow-x: hidden !important;
        }
        
        .mycolor {
            background-color: #00a65a !important;
            border: 1px solid #00a65a !important;
            color: #fff !important;
            cursor: pointer !important;
        }
        
        .mycolors {
            background-color: #e74a3b !important;
            border: 1px solid #e74a3b !important;
            color: #fff !important;
            cursor: pointer !important;
        }
        
        .myclr {
            background-color: #3a87ad !important;
            border: 1px solid #3a87ad !important;
            color: #fff !important;
            cursor: pointer !important;
        }
        
        .colored {
            color: #e74a3b;
            font-size: 1rem;
        }
        
        #calendar {
            margin-top: -2rem !important;
        }
    </style>
    <script>
        $(document).ready(function() {
            if ($(window).width() >= 768) {
                $('#sidebarToggleTop').trigger('click');
            }
            var calendar = $('#calendar').fullCalendar({
                editable: false,
                header: {
                    left: '',
                    center: 'title',
                    right: 'prev,today,next'
                },

                events: '../../php_function/load.php',
                displayEventTime: true,
                timeFormat: 'h a',
                customRender: true,

                eventRender: function(event, eventElement) {
                    if (event.status == "Approve") {
                        eventElement.addClass('mycolor');
                    } else
                    if (event.status == "denied") {
                        eventElement.addClass('mycolors');
                    } else
                    if (event.status == "inactive") {
                        eventElement.addClass('myclr');
                    }
                },
                validRange: function(nowDate) {
                    return {
                        start: nowDate.clone().add(-1, 'day'),
                        end: nowDate.clone().add(99999, 'months')
                    };
                },
                selectOverlap: function(event) {
                    return event.rendering === 'background';
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {

                    $('#myModal').modal('show');
                    $("#title").focus();
                    var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");

                    $("#myModal").off("click", "#btnsked").on('click', '#btnsked', function(e) {
                        e.preventDefault();
                        $("#btnsked").attr("disabled", "disabled");
                        var title = $('#title').val();
                        var starts = $('#start').val();
                        var ends = $('#end').val();
                        var purok = $('#purok').val();
                        var ecoboy = $('#ecoboy').val();

                        $(".colored").remove();
                        if (title.length < 1 && starts.length < 1 && ends.length < 1 && purok.length < 1 && ecoboy.length < 1) {
                            Swal.fire('All fields are required!');
                            $('#btnsked').prop("disabled", false);
                        } else
                        if (title.length < 1) {
                            $('#reqtitle').after('<label class="colored">Required</label>');
                            $('#btnsked').prop("disabled", false);
                        } else
                        if (starts.length < 1) {
                            $('#reqstart').after('<label class="colored">Required</label>');
                            $('#btnsked').prop("disabled", false);
                        } else
                        if (ends.length < 1) {
                            $('#reqend').after('<label class="colored">Required</label>');
                            $('#btnsked').prop("disabled", false);
                        } else
                        if (purok.length < 1) {
                            $('#reqprk').after('<label class="colored">Required</label>');
                            $('#btnsked').prop("disabled", false);
                        } else
                        if (ecoboy.length < 1) {
                            $('#reqeco').after('<label class="colored">Required</label>');
                            $('#btnsked').prop("disabled", false);
                        } else {

                            $('#myModal').modal('hide');
                            Swal.fire({
                                position: 'top',
                                title: 'Are you sure?',
                                text: "You want to save this schedule?",
                                icon: 'warning',
                                showCancelButton: true,
                                allowOutsideClick: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes'
                            }).then((result) => {
                                if (result.value) {
                                    $('.fc-event').remove();
                                    $.ajax({
                                        url: "../../php_function/insertsked.php",
                                        type: "POST",
                                        data: {
                                            title: title,
                                            start: start,
                                            StartTime: starts,
                                            EndTime: ends,
                                            Purok: purok,
                                            Ecoboy: ecoboy,
                                            end: end
                                        },
                                        success: function(data) {
                                            $('#myModal').modal('hide');
                                            $('#title').val('');
                                            $('#start').val('');
                                            $('#end').val('');
                                            $('#purok').val('');
                                            $('#ecoboy').val('');
                                            $('#btnsked').prop("disabled", false);
                                            calendar.fullCalendar('refetchEvents');
                                            Swal.fire({
                                                position: 'top',
                                                icon: 'success',
                                                title: 'Your schedule has been saved!',
                                                allowOutsideClick: false
                                            })
                                            window.location.href = window.location.href;
                                        }
                                    })
                                } else {
                                    $('#title').val('');
                                    $('#start').val('');
                                    $('#end').val('');
                                    $('#purok').val('');
                                    $('#ecoboy').val('');
                                    $('#btnsked').prop("disabled", false);
                                }
                            })

                        }
                    });
                },
                eventClick: function(event) {
                    var id = event.id;
                    var status = event.status;
                    if (status == 'Approve') {
                        $('#approve' + id).modal('show');
                    } else if (status == 'denied') {
                        $('#denied' + id).modal('show');
                    } else if (status == 'inactive') {
                        $('#inactive' + id).modal('show');
                        // $.ajax({
                        //     type: 'POST',
                        //     url: '../../php_function/selectmodal.php',
                        //     data: {
                        //         aydes: id
                        //     },
                        //     success: function(data) {
                        //         $('#updatemodal').modal('show');
                        //         $('.modal-body').html(data);
                        //     },
                        //     error: function(err) {
                        //         alert("error" + JSON.stringify(err));
                        //     }
                        // })

                        // $("#updatemodal").off("click", "#btnupsked").on('click', '#btnupsked', function(e) {
                        //     var ids = $('#aydes').val();
                        //     var title = $('#taytel').val();
                        //     var starts = $('#startyes').val();
                        //     var ends = $('#endyes').val();
                        //     var purok = $('#purokyes').val();
                        //     var ecoboy = $('#ecoboyes').val();
                        //     $('#updatemodal').modal('hide');

                        //     alert('ERROR');
                        //     Swal.fire({
                        //         position: 'top',
                        //         title: 'Are you sure?',
                        //         text: "You want to update this schedule?",
                        //         icon: 'warning',
                        //         showCancelButton: true,
                        //         allowOutsideClick: false,
                        //         confirmButtonColor: '#3085d6',
                        //         cancelButtonColor: '#d33',
                        //         confirmButtonText: 'Yes'
                        //     }).then((result) => {
                        //         if (result.value) {
                        //             $('.fc-event').remove();
                        //             $.ajax({
                        //                 url: "../../php_function/updateevent.php",
                        //                 type: "POST",
                        //                 data: {
                        //                     id: ids,
                        //                     title: title,
                        //                     StartTime: starts,
                        //                     EndTime: ends,
                        //                     Purok: purok,
                        //                     Ecoboy: ecoboy
                        //                 },
                        //                 success: function(data) {
                        //                     alert(data);
                        //                     calendar.fullCalendar('refetchEvents');
                        //                     // Swal.fire({
                        //                     //     position: 'top',
                        //                     //     icon: 'success',
                        //                     //     title: 'Successfully Update!',
                        //                     //     confirmButtonText: 'Ok',
                        //                     //     allowOutsideClick: false
                        //                     // })
                        //                 }
                        //             })
                        //         } else {}
                        //     })
                        // });

                    }
                }
            });


        });
    </script>
</head>
<?php include '../../include/includecalendarhead.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Garbage Collection Schedule</h6>
        </center>

        <div class="card-body">
            <!-- DREA E SULOD ANG CONTENT -->


            <!-- Content Row -->
            <div class="row">

                <div class="col-xl-12 col-lg-8">
                    <div>
                        <div class="mt-4 text-left small">
                            <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i> PENDING
                    </span>
                            <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> APPROVED
                    </span>
                            <span class="mr-2">
                      <i class="fas fa-circle text-danger"></i> DENIED
                    </span>
                        </div>
                    </div>
                    <div id="calendar"></div>
                </div>

                <!-- Donut Chart -->

            </div>
        </div>
    </div>

    <!-- /.container-fluid -->
    <div class="modal show" id="myModal" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title">Garbage Collection Schedule</h4>
                    </center>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="#" id="modalform">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <label id="reqtitle">Title * </label>
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="form-group">
                                    <label id="reqstart">Start Time * </label>
                                    <input type="time" class="form-control" id="start" name="StartTime">
                                </div>
                                <div class="form-group">
                                    <label id="reqend">End Time * </label>
                                    <input type="time" class="form-control" id="end" name="EndTime">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="form-group">
                                    <label id="reqprk">Purok No. * </label>
                                    <select class="form-control select2" id="purok" name="Purok_No">
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="RV3 7-A">RV3 7-A</option>
                                    <option value="RV3 7-B">RV3 7-B</option>
                                    <option value="RV3 8-A">RV3 8-A</option>
                                    <option value="RV3 8-B">RV3 8-B</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11-A">11-A</option>
                                    <option value="11-B">11-B</option>
                                </select>
                                </div>
                                <div class="form-group">
                                    <label id="reqeco">Ecoboy Name * </label>
                                    <select class="form-control select2 form-control" id="ecoboy" name="ecoboy">
                                    <option></option>
                                     <?php 
                            $query = mysqli_query($db, "Select * from resident WHERE position='Ecoboy';") or die(mysqli_error());
                    while($fetch = mysqli_fetch_array($query))
                    {
                    $fullname=$fetch['fname'].' '.$fetch['lname'];
                        ?>
                                        <option value="<?php echo $fetch['id']; ?>"><?php echo $fullname; ?></option>
                                        <?php
                        }
                        ?>
                                    
                                </select>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" id="btnsked" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal show" id="updatemodal" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <center>
                        <h4 class="modal-title">Garbage Collection Schedule</h4>
                    </center>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnupsked" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </div>


    <?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
        <div class="modal" id="approve<?php echo $row['Sched_No'];?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <center>
                            <h4 class="modal-title">Garbage Collection Schedule</h4>
                        </center>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <?php $status = $row['status'];
                                        if($status == 'Approve') { ?>
                                    <h5 style="color: #00a65a;"> Approved</h5>
                                    <?php } else if($status == 'denied') {?>
                                    <h5 style="color: #e74a3b;"> Denied</h5>
                                    <?php } else if($status == 'inactive') { ?>
                                    <h5 style="color: #3a87ad;"> Not yet review</h5>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" id="" name="title" value="<?php echo $row['title'];?>" disabled="disabled">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="form-group">
                                    <label>Start Time</label>
                                    <input type="time" class="form-control" id="" name="StartTime" value="<?php echo $row['StartTime'];?>" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>End Time</label>
                                    <input type="time" class="form-control" id="" name="EndTime" value="<?php echo $row['EndTime'];?>" disabled="disabled">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="form-group">
                                    <label>Purok No.:</label>
                                    <select class="form-control select2" id="" name="Purok_No" disabled="disabled">
                                    <option><?php echo $row['Purok_No'];?></option> 
                                </select>
                                </div>
                                <div class=" form-group ">
                                    <label>Ecoboy Name:</label>
                                    <select class="form-control select2 form-control " id="" name="ecoboy " disabled="disabled">
                                    <option><?php echo $row['EcoboyName'];?></option>                                      
                                </select>
                                </div>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
            <div class="modal akongmodal" id="inactive<?php echo $row['Sched_No'];?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <center>
                                <h4 class="modal-title">Garbage Collection Schedule</h4>
                            </center>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <!-- Modal body -->
                        <form action="../../php_function/schedulefunction.php" role="form" method="POST">
                            <div class="modal-body">
                                <div class="row addhere">
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <?php $status = $row['status'];
                                        if($status == 'Approve') { ?>
                                            <h5 style="color: #00a65a;"> Approved</h5>
                                            <?php } else if($status == 'denied') {?>
                                            <h5 style="color: #e74a3b;"> Denied</h5>
                                            <?php } else if($status == 'inactive') { ?>
                                            <h5 style="color: #3a87ad;"> Not yet review</h5>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="hidden" class="form-control" id="aydah" name="idhere" value="<?php echo $row['Sched_No'];?>">
                                            <input type="text" class="form-control" id="" name="title" value="<?php echo $row['title'];?>">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-8">
                                        <div class="form-group">
                                            <label>Start Time</label>
                                            <input type="time" class="form-control" id="" name="StartTime" value="<?php echo $row['StartTime'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>End Time</label>
                                            <input type="time" class="form-control" id="" name="EndTime" value="<?php echo $row['EndTime'];?>">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-8">
                                        <div class="form-group">
                                            <label>Purok No.:</label>
                                            <select class="form-control select2" id="" name="Purok_No"> 
                                    <option value="<?php echo $row['Purok_No'];?>"><?php echo $row['Purok_No'];?></option>
                                    <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="RV3 7-A">RV3 7-A</option>
                                        <option value="RV3 7-B">RV3 7-B</option>
                                        <option value="RV3 8-A">RV3 8-A</option>
                                        <option value="RV3 8-B">RV3 8-B</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11-A">11-A</option>
                                        <option value="11-B">11-B</option>
                                </select>
                                        </div>
                                        <div class=" form-group ">
                                            <label>Ecoboy Name:</label>
                                            <select class="form-control select2" id="" name="ecoboy">
                                    <option value="<?php echo $row['ecoboy_id'];?>"><?php echo $row['EcoboyName'];?></option>
                                                                        <?php 
                                    $query = mysqli_query($db, "Select * from resident WHERE position='Ecoboy';") or die(mysqli_error());
                                    while($fetch = mysqli_fetch_array($query))
                                    {
                                    $fullname=$fetch['fname'].' '.$fetch['lname'];
                                    ?>
                                    <option value="<?php echo $fetch['id']; ?>"><?php echo $fullname; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="submit" onclick="return confirm('Are you sure? Your will not able to revert this.');" class="btn btn-primary" name="btnupdate" id="btnupdate">Update </button>
                                </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php
			$cap = mysqli_query($db,"SELECT * FROM schedule;");
			while($row = mysqli_fetch_array($cap))
			{   
			?>
                <div class="modal" id="denied<?php echo $row['Sched_No'];?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <center>
                                    <h4 class="modal-title">Garbage Collection Schedule</h4>
                                </center>
                                <button type="button" class="close" data-dismiss="modal">×</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <?php $status = $row['status'];
                                        if($status == 'Approve') { ?>
                                            <h5 style="color: #00a65a;"> Approved</h5>
                                            <?php } else if($status == 'denied') {?>
                                            <h5 style="color: #e74a3b;"> Denied</h5>
                                            <?php } else if($status == 'inactive') { ?>
                                            <h5 style="color: #3a87ad;"> Not yet review</h5>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12">
                                        <p>
                                            <?php echo $row['reason'].'.';?>
                                        </p>
                                    </div>
                                    <!-- <div class="col-xl-12 col-lg-12">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" id="" name="title" value="<?php echo $row['title'];?>" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-8">
                                    <div class="form-group">
                                        <label>Start Time</label>
                                        <input type="time" class="form-control" id="" name="StartTime" value="<?php echo $row['StartTime'];?>" disabled="disabled">
                                    </div>
                                    <div class="form-group">
                                        <label>End Time</label>
                                        <input type="time" class="form-control" id="" name="EndTime" value="<?php echo $row['EndTime'];?>" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-8">
                                    <div class="form-group">
                                        <label>Purok No.:</label>
                                        <select class="form-control select2" id="" name="Purok_No" disabled="disabled">
                                    <option><?php echo $row['Purok_No'];?></option> 
                                </select>
                                    </div>
                                    <div class=" form-group ">
                                        <label>Ecoboy Name:</label>
                                        <select class="form-control select2 form-control " id="" name="ecoboy " disabled="disabled">
                                    <option><?php echo $row['EcoboyName'];?></option>                                      
                                </select>
                                    </div> -->
                                </div>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer ">
                            </div>
                        </div>
                    </div>
                </div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->

</div>
<!-- /.container-fluid -->

</div>
<?php include '../../include/footer.php';?>
</div>
</div>
<!-- End of Main Content -->
<!-- Bootstrap core JavaScript-->

<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js "></script>
<!-- Core plugin JavaScript-->
<script src="../../vendor/jquery-easing/jquery.easing.min.js "></script>

<!-- Custom scripts for all pages-->
<script src="../../components/js/sb-admin-2.min.js "></script>

<!-- Page level plugins -->
<script src="../../vendor/datatables/jquery.dataTables.min.js "></script>
<script src="../../vendor/datatables/dataTables.bootstrap4.min.js "></script>

<!-- Page level custom scripts -->
<script src="../../components/js/demo/datatables-demo.js "></script>
<!-- Page level plugins -->

</body>

</html>