<?php include '../../include/mainincludetop.php'; ?>
<?php include '../../php_function/session_name.php'; ?>

<!-- DREA E SULOD ANG CONTENT -->

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-gray-600">Garbage Collection Responded</h6>
        </center>
    </div>
    <div class="card-body">
        <?php 
          if(isset($_GET['skidid']))
                        {
   $skidids=$_GET['skidid'];
$result = mysqli_query($db, "SELECT * FROM schedule where Sched_No='$skidids';");
           
                        }
                        else
                        {
            $skidid=$_POST['skidid'];
            $result = mysqli_query($db, "SELECT * FROM schedule where Sched_No='$skidid';");
                        }
            while ($row = mysqli_fetch_array($result))
            {
               $dates= date("F j, Y  "."("."l".")",strtotime($row['CollDateStart']));
               $stime= date("h: i a  ",strtotime($row['StartTime']));
               $etime= date("h: i a  ",strtotime($row['EndTime']));
            }
            ?>
        <h5><?php echo $dates; ?></h5>
        <h6>Start Time: <?php echo $stime; ?></h6>
        <h6>End time: <?php echo $etime; ?></h6>
        <br />
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name of the respondent</th>
                        <th>Did I collected the garbage?</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name of the respondent</th>
                        <th>Did I collected the garbage?</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <?php
                        if(isset($_GET['skidid']))
                        {
            $skidids=$_GET['skidid'];
            $result = mysqli_query($db, "SELECT * FROM resident_responded where sked_id='$skidids' and garbage='Yes';");

                        }
                        else
                        { 
            $skidid=$_POST['skidid'];
            $result = mysqli_query($db, "SELECT * FROM resident_responded where sked_id='$skidid' and garbage='Yes';");
                        }
            while ($row = mysqli_fetch_array($result)) {
                    $restids=$row['rest_id'];
                 $newresult = mysqli_query($db, "SELECT * FROM resident where id='$restids';");

            while ($resrow = mysqli_fetch_array($newresult)) {
              ?>
                        <td><?php echo $resname = $resrow['fname'].' '.$resrow['lname']; ?></td>

                        <?php if ($row['if_collected']=='') { ?>
                        <td>
                            <div class="form-group">
                                <form action="../../php_function/garbage_confirm.php" id="submitconfirm" role="form"
                                    method="POST">
                                    <input type="hidden" value="<?php echo $row['respond_id']; ?>"
                                        name="responded_id" />
                                    <input type="hidden" value="<?php echo $row['sked_id']; ?>" name="idsked" />
                                    <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                                    <input type="hidden" value="<?php echo $restids; ?>" name="resid" />
                                    <input type="hidden" value="<?php echo $resname; ?>" name="nameres" />
                                    <input type="hidden" value="<?php echo $fullname; ?>" name="ecoboynym" />
                                    <input type="hidden" value="<?php echo $idres; ?>" name="ecoid" />

                                    <label class="radio-inline">
                                        <input type="checkbox" style="width: 85% !important;" class="form-control"
                                            name="btn-confirm-yes" value="yes" onchange="submitconfirm();" />
                                        <h6>Yes</h6>
                                    </label>
                                    &nbsp;
                                    <label class="radio-inline">
                                        <input type="checkbox" name="no" class="form-control" data-toggle="modal"
                                            data-target="#openmodalNo<?php echo $row['sked_id']; ?>" />
                                        <h6>No</h6>
                                    </label>
                                </form>
                            </div>
                        </td>
                        <?php } else if ($row['if_collected']=='Yes') { ?>
                        <td>
                            YES
                        </td>
                        <?php } else if ($row['if_collected']=='No') { ?>
                        <td>
                            No
                        </td>
                        <?php } ?>
                    </tr>

                    <?php
        }
        }
        ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
$cap = mysqli_query($db, "SELECT * FROM schedule;");
while ($row = mysqli_fetch_array($cap)) {
  ?>
<div class="modal" id="openModal<?php echo $row['Sched_No']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Do you want to decline this schedule please state the reason.</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/function_for_ecoboy.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <textarea style="width: 100%; height: 100%;" placeholder="Enter here" name="reason"></textarea>
                        <input type="hidden" value="<?php echo $row['Sched_No']; ?>" name="idsked" />
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="decline-sked" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->

<?php
$cap = mysqli_query($db, "SELECT * FROM resident_responded;");
while ($row = mysqli_fetch_array($cap)) {
    $idhere=$row['sked_id'];
  ?>
<div class="modal" id="openmodalYes<?php echo $row['sked_id']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">Yes</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/garbage_confirm.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">

                    <p>Please confirm form submission.</p>
                    <p>Do you want to continue?</p>

                    <input type="hidden" value="<?php echo $row['sked_id']; ?>" name="idsked" />
                    <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                    <input type="hidden" value="<?php echo $row['rest_id']; ?>" name="resid" />
                    <input type="hidden" value="<?php echo $row['res_name']; ?>" name="nameres" />
                    <input type="hidden" value="<?php echo $fullname; ?>" name="ecoboynym" />
                    <input type="hidden" value="<?php echo $idres; ?>" name="ecoid" />



                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-confirm-yes" class="btn btn-success">Continue</button>

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>

<?php
$cap = mysqli_query($db, "SELECT * FROM resident_responded;");
while ($row = mysqli_fetch_array($cap)) {
    $idhere=$row['sked_id'];
  ?>
<div class="modal" id="openmodalNo<?php echo $row['sked_id']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">No</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/garbage_confirm.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <input type="hidden" value="<?php echo $row['respond_id']; ?>" name="responded_id" />
                    <input type="hidden" value="<?php echo $row['sked_id']; ?>" name="idsked" />
                    <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                    <input type="hidden" value="<?php echo $row['rest_id']; ?>" name="resid" />
                    <input type="hidden" value="<?php echo $row['res_name']; ?>" name="nameres" />
                    <input type="hidden" value="<?php echo $fullname; ?>" name="ecoboynym" />
                    <input type="hidden" value="<?php echo $idres; ?>" name="ecoid" />
                    <div class="form-group">
                        <textarea type="text" class="form-control" placeholder="Why? State here" width="5%" height="5%"
                            name="reason" required="required"></textarea>
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-confirm-no" class="btn btn-success">Continue</button>

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<!----------acceeetpppttttt Garbage Collection Responded--->
<?php
$cap = mysqli_query($db, "SELECT * FROM resident_responded;");
while ($row = mysqli_fetch_array($cap)) {
    $idhere=$row['sked_id'];
  ?>
<div class="modal" id="openmodalYES<?php echo $row['sked_id']; ?>">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <center>
                    <h4 class="modal-title">No</h4>
                </center>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="../../php_function/garbage_confirm.php" role="form" method="POST">
                <!-- Modal body -->
                <div class="modal-body">
                    <input type="hidden" value="<?php echo $row['respond_id']; ?>" name="responded_id" />
                    <input type="hidden" value="<?php echo $row['sked_id']; ?>" name="idsked" />
                    <input type="hidden" value="<?php echo $row['days']; ?>" name="day" />
                    <input type="hidden" value="<?php echo $row['rest_id']; ?>" name="resid" />
                    <input type="hidden" value="<?php echo $row['res_name']; ?>" name="nameres" />
                    <input type="hidden" value="<?php echo $fullname; ?>" name="ecoboynym" />
                    <input type="hidden" value="<?php echo $idres; ?>" name="ecoid" />
                    <div class="form-group">
                        <textarea type="text" class="form-control" placeholder="Why? State here" width="5%" height="5%"
                            name="reason" required="required"></textarea>
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" name="btn-confirm-no" class="btn btn-success">Continue</button>

                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
function submitconfirm() {
    avar = confirm('Theres no undo! Do you want to continue?');
    if (avar == true) {
        document.getElementById('submitconfirm').submit();
    }
    if (avar == false) {
        location.reload();
    }
}
</script>


<?php include '../../include/mainincludebottom.php'; ?>