-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 02, 2019 at 04:13 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barangaybaliok`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomplishment_report`
--

DROP TABLE IF EXISTS `accomplishment_report`;
CREATE TABLE IF NOT EXISTS `accomplishment_report` (
  `Accomp_No` int(11) NOT NULL AUTO_INCREMENT,
  `Date` varchar(20) NOT NULL,
  `Plate_No` varchar(50) NOT NULL,
  `Destination` varchar(50) NOT NULL,
  `Commodity` varchar(50) NOT NULL,
  `weight_net_kgs` varchar(20) NOT NULL,
  PRIMARY KEY (`Accomp_No`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomplishment_report`
--

INSERT INTO `accomplishment_report` (`Accomp_No`, `Date`, `Plate_No`, `Destination`, `Commodity`, `weight_net_kgs`) VALUES
(4, '2019-05-22', 'P#38607/cabanog', 'CENR SANITARY\r\nLANDFILL D.C', 'SOLID WASTE', '1205'),
(5, '2019-05-22', 'P#38607/cabanog', 'CENR SANITARY LANDFILL D.C', 'SOLID WASTE', '1023'),
(6, '2019-05-22', 'P#38607/cabanog', 'CENR SANITARY LANDFILL D.C', 'SOLID WASTE', '1550'),
(7, '2019-05-22', 'P#38607/cabanog', 'CENR SANITARY LANDFILL D.C', 'SOLID WASTE', '1550');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
CREATE TABLE IF NOT EXISTS `administrator` (
  `ses_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(10) NOT NULL,
  `auth` varchar(10) NOT NULL,
  PRIMARY KEY (`ses_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`ses_id`, `username`, `password`, `firstname`, `lastname`, `photo`, `auth`) VALUES
(1, 'jie@jie', 'jie', 'Jie', 'Jie', 'user.png', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `brgycaptain`
--

DROP TABLE IF EXISTS `brgycaptain`;
CREATE TABLE IF NOT EXISTS `brgycaptain` (
  `brgycaptain_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` varchar(50) NOT NULL,
  `yearend` varchar(50) DEFAULT NULL,
  `current` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brgycaptain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgycaptain`
--

INSERT INTO `brgycaptain` (`brgycaptain_id`, `res_id`, `name`, `yeardeclared`, `yearend`, `current`, `date`) VALUES
(16, '11', 'sd asd', '2018-03-05', NULL, 'Current Captain', '2019-05-24 13:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `brgykagawad`
--

DROP TABLE IF EXISTS `brgykagawad`;
CREATE TABLE IF NOT EXISTS `brgykagawad` (
  `brgykagawad_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` varchar(5) NOT NULL,
  `rank` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `yeardeclared` varchar(50) DEFAULT NULL,
  `yearend` varchar(50) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brgykagawad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgykagawad`
--

INSERT INTO `brgykagawad` (`brgykagawad_id`, `res_id`, `rank`, `name`, `yeardeclared`, `yearend`, `date_created`) VALUES
(8, '9', 'Rank 1', 'firstname lastname', '2019-05-01', NULL, '2019-05-18 16:00:48');

-- --------------------------------------------------------

--
-- Table structure for table `brgypersonnel`
--

DROP TABLE IF EXISTS `brgypersonnel`;
CREATE TABLE IF NOT EXISTS `brgypersonnel` (
  `personnel_id` int(11) NOT NULL AUTO_INCREMENT,
  `resident_id` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `brgyposition` varchar(50) NOT NULL,
  `yeardeclared` varchar(15) NOT NULL,
  `yearend` varchar(15) NOT NULL,
  PRIMARY KEY (`personnel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgypersonnel`
--

INSERT INTO `brgypersonnel` (`personnel_id`, `resident_id`, `name`, `brgyposition`, `yeardeclared`, `yearend`) VALUES
(5, '12', 'good Jv', 'Ecoboy', '2019-05-13', ''),
(8, '14', 'HHHH wkol', 'Purok Leader', '2018-10-18', '');

-- --------------------------------------------------------

--
-- Table structure for table `brgyposition`
--

DROP TABLE IF EXISTS `brgyposition`;
CREATE TABLE IF NOT EXISTS `brgyposition` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `position` varchar(50) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgyposition`
--

INSERT INTO `brgyposition` (`pos_id`, `description`, `position`, `date_created`) VALUES
(2, 'This is the description of the purok leader', 'Purok Leader', '2018-03-07'),
(4, 'This is the description of the ecoboy', 'Ecoboy', '07-13-2019');

-- --------------------------------------------------------

--
-- Table structure for table `brgyuseragent`
--

DROP TABLE IF EXISTS `brgyuseragent`;
CREATE TABLE IF NOT EXISTS `brgyuseragent` (
  `useragent_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `date_created` varchar(20) NOT NULL,
  `auth` varchar(20) NOT NULL,
  PRIMARY KEY (`useragent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brgyuseragent`
--

INSERT INTO `brgyuseragent` (`useragent_id`, `firstname`, `lastname`, `username`, `password`, `date_created`, `auth`) VALUES
(2, 'User1', 'Agent1', 'user1@gmail.com', '012345678', '09-02-2019', 'landfill'),
(3, 'User2', 'Agent2', 'user2@gmail.com', '012345678', '09-03-2019', 'CENRO');

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

DROP TABLE IF EXISTS `donation`;
CREATE TABLE IF NOT EXISTS `donation` (
  `pledge_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `res_id` varchar(5) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `ecoboyname` varchar(50) NOT NULL,
  `econickname` varchar(20) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `confirmation` varchar(10) DEFAULT NULL,
  `new` varchar(5) NOT NULL,
  `date` varchar(20) DEFAULT NULL,
  `reset` varchar(10) NOT NULL,
  PRIMARY KEY (`pledge_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`pledge_id`, `name`, `res_id`, `ecoboy_id`, `ecoboyname`, `econickname`, `amount`, `confirmation`, `new`, `date`, `reset`) VALUES
(21, 'ACT PBA', '13', '12', 'good Jv', 'Boyet', 5000.00, 'Recieved', 'old', '09-30-2019', 'reset');

-- --------------------------------------------------------

--
-- Table structure for table `donation_history`
--

DROP TABLE IF EXISTS `donation_history`;
CREATE TABLE IF NOT EXISTS `donation_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `eco_id` varchar(5) NOT NULL,
  `eco_name` varchar(50) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation_history`
--

INSERT INTO `donation_history` (`history_id`, `eco_id`, `eco_name`, `amount`, `date`) VALUES
(3, '12', 'good Jv', '5000.00', '30-09-2019');

-- --------------------------------------------------------

--
-- Table structure for table `ecoboy_claimed_garbage`
--

DROP TABLE IF EXISTS `ecoboy_claimed_garbage`;
CREATE TABLE IF NOT EXISTS `ecoboy_claimed_garbage` (
  `garbage_id` int(5) NOT NULL AUTO_INCREMENT,
  `Ecoboy_nym` varchar(50) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `resid` varchar(5) NOT NULL,
  `resname` varchar(50) NOT NULL,
  `claim` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `skid` varchar(5) NOT NULL,
  `cdate` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`garbage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ecoboy_claimed_garbage`
--

INSERT INTO `ecoboy_claimed_garbage` (`garbage_id`, `Ecoboy_nym`, `ecoboy_id`, `resid`, `resname`, `claim`, `reason`, `skid`, `cdate`) VALUES
(10, 'good Jv', '12', '', '', 'Yes', '', '8', ''),
(7, 'good Jv', '12', '13', 'ACT PBA', 'No', 'asdfl', '8', 'wednesday'),
(9, 'good Jv', '12', '', '', 'Yes', '', '', ''),
(11, 'good Jv', '12', '134', 'ACT PBA', 'No', '', '8', 'wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `garbageprice`
--

DROP TABLE IF EXISTS `garbageprice`;
CREATE TABLE IF NOT EXISTS `garbageprice` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_value` double(10,2) NOT NULL,
  `price_status` enum('0','1') NOT NULL,
  `date_created` varchar(20) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `garbageprice`
--

INSERT INTO `garbageprice` (`price_id`, `price_value`, `price_status`, `date_created`) VALUES
(1, 3.00, '0', '09-09-2019'),
(2, 1.00, '0', '09-09-2019'),
(3, 2.00, '0', '09-09-2019'),
(4, 4.00, '1', '09-09-2019'),
(5, 5.00, '0', '09-09-2019'),
(6, 6.00, '0', '09-09-2019');

-- --------------------------------------------------------

--
-- Table structure for table `marker`
--

DROP TABLE IF EXISTS `marker`;
CREATE TABLE IF NOT EXISTS `marker` (
  `marker_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `img` varchar(20) NOT NULL,
  `lat` varchar(60) NOT NULL,
  `lng` varchar(60) NOT NULL,
  `status` varchar(3) NOT NULL,
  PRIMARY KEY (`marker_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marker`
--

INSERT INTO `marker` (`marker_id`, `name`, `img`, `lat`, `lng`, `status`) VALUES
(1, 'Jojie Avergonzado', 'Koala.jpg', '7.045549', '125.498328', 'yes'),
(2, 'James Aversulo', 'user.png', '7.046166', '125.499358', 'yes'),
(3, 'Fumi Yam', 'user.png', '7.045554', '125.499841', 'yes'),
(4, 'isa pa', 'user.png', '7.0464', '125.500796', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
CREATE TABLE IF NOT EXISTS `ratings` (
  `rateid` int(11) NOT NULL AUTO_INCREMENT,
  `ecoboyname` varchar(50) DEFAULT NULL,
  `score` varchar(10) DEFAULT NULL,
  `residentname` varchar(50) DEFAULT NULL,
  `ecoboyid` varchar(5) DEFAULT NULL,
  `resid` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`rateid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`rateid`, `ecoboyname`, `score`, `residentname`, `ecoboyid`, `resid`) VALUES
(17, 'good Jv', '3', 'ACT PBA', '12', '13');

-- --------------------------------------------------------

--
-- Table structure for table `resident`
--

DROP TABLE IF EXISTS `resident`;
CREATE TABLE IF NOT EXISTS `resident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Brgy_ID` varchar(15) NOT NULL,
  `Precinct_No` varchar(10) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `suffix` varchar(5) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `capability` varchar(25) DEFAULT NULL,
  `assistedby` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `age` varchar(3) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` int(5) NOT NULL,
  `province` varchar(50) NOT NULL,
  `citymuni` varchar(50) NOT NULL,
  `barangay` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `noofyears` int(5) NOT NULL,
  `noofmonths` int(5) NOT NULL,
  `numofyears` int(5) NOT NULL,
  `citizenship` varchar(25) NOT NULL,
  `citizenshipby` varchar(25) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `cmuni` varchar(50) NOT NULL,
  `cprovince` varchar(50) NOT NULL,
  `don` varchar(20) NOT NULL,
  `cnoa` varchar(25) NOT NULL,
  `rdon` varchar(20) NOT NULL,
  `cnooa` varchar(25) NOT NULL,
  `occupation` varchar(25) NOT NULL,
  `tin` varchar(15) NOT NULL,
  `civilstatus` varchar(20) NOT NULL,
  `spouse` varchar(25) NOT NULL,
  `flname` varchar(50) NOT NULL,
  `ffname` varchar(50) NOT NULL,
  `fmname` varchar(50) NOT NULL,
  `mlname` varchar(50) NOT NULL,
  `mfname` varchar(50) NOT NULL,
  `mmname` varchar(50) NOT NULL,
  `hotf` varchar(50) NOT NULL,
  `hotfname` varchar(50) DEFAULT NULL,
  `family_verification` varchar(5) DEFAULT NULL,
  `names` varchar(500) NOT NULL,
  `Purok_No` varchar(15) NOT NULL,
  `purokleader` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `re_pass` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `position` varchar(20) DEFAULT NULL,
  `rate` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resident`
--

INSERT INTO `resident` (`id`, `Brgy_ID`, `Precinct_No`, `lname`, `fname`, `mname`, `suffix`, `nickname`, `capability`, `assistedby`, `gender`, `age`, `height`, `weight`, `province`, `citymuni`, `barangay`, `street`, `noofyears`, `noofmonths`, `numofyears`, `citizenship`, `citizenshipby`, `dob`, `cmuni`, `cprovince`, `don`, `cnoa`, `rdon`, `cnooa`, `occupation`, `tin`, `civilstatus`, `spouse`, `flname`, `ffname`, `fmname`, `mlname`, `mfname`, `mmname`, `hotf`, `hotfname`, `family_verification`, `names`, `Purok_No`, `purokleader`, `email`, `password`, `re_pass`, `status`, `photo`, `position`, `rate`) VALUES
(9, '19-000', '0000-0', 'lastname', 'firstname', 'middlename', 'Jr.', 'nickname', '', '', 'Male', '21', '52', 50, 'province', 'city', 'barangay', 'house', 6, 6, 6, 'Filipino', '', '0000-00-00', '', '', '0000-00-00', '', '0000-00-00', '', 'none', '154_282_264', ' Single', '', 'lastname', 'firstname', 'middlename', 'lastname', 'firstname', 'middlename', 'Yes', '9', 'head', 'add,adda,addaa', '10', 'Vicentita Ocay', 'kagawad@gmail.com', 'kag', 'wer', 'active', 'Koala.jpg', 'Kagawad', NULL),
(11, 'asd', 'adsf', 'asd', 'sd', 'w', 'Jr.', 'we', '', '', 'Female', '', '4', 3, 'asdf', 'asdf', 'adsf', 'asdf', 4, 3, 2, 'as', '', '0000-00-00', '', '', '0000-00-00', '', '0000-00-00', '', 'asdf', '34344', ' Single', '', 'asdf', 'asdf', '', 'w', 'aw', 'xc', '', '10', '', '', '10', 'Vicentita Ocay', 'cap@gmail.com', 'cap', 'ty', 'active', 'user.png', 'Captain', NULL),
(12, '1545', '452', 'Jv', 'good', 'is', 'Jr.', 'Boyet', '', '', 'Female', '', '4', 3, 'asdf', 'asdf', 'adsf', 'asdf', 4, 3, 2, 'as', '', '2019-05-08', '', '', '2019-05-01', '', '2019-05-15', '', 'asdf', '34344', ' Single', '', 'asdf', 'asdf', '', 'w', 'aw', 'xc', '', '10', '', '', '10', 'Vicentita Ocay', 'eco@gmail.com', 'eco', 'jv', 'active', 'user.png', 'Ecoboy', '3'),
(13, '7895', '658', 'PBA', 'ACT', 'cis', 'Jr.', 'the', '', '', 'Female', '', '4', 3, 'asdf', 'asdf', 'adsf', 'asdf', 4, 3, 2, 'as', '', '2019-05-08', '', '', '2019-05-01', '', '2019-05-15', '', 'asdf', '34344', ' Single', '', 'asdf', 'asdf', '', 'w', 'aw', 'xc', 'No', '9', 'Yes', '', '10', 'Vicentita Ocay', 'res@gmail.com', 'res', 'res', 'active', 'user.png', '', NULL),
(14, '7895', '658', 'wkol', 'HHHH', 'esx', '', '', '', '', 'Female', '', '4', 3, 'asdf', 'asdf', 'adsf', 'asdf', 4, 3, 2, 'as', '', '2019-05-08', '', '', '2019-05-01', '', '2019-05-15', '', 'asdf', '34344', ' Single', '', 'asdf', 'asdf', '', 'w', 'aw', 'xc', 'No', '9', 'Yes', '', '10', 'Vicentita Ocay', 'prk@gmail.com', 'prk', 'dec', 'active', 'user.png', 'Purok Leader', NULL),
(15, '19-000', '000-0', 'asdf', 'sdfa', 'ss', 'Jr.', 'jie', '', '', 'Male', '8', '43', 23, 'No Selection', 'Davao City', 'sd', '12 LOWER LUBOGAN', 4, 2, 2, 'Philippines', 'ByBirth', '2011-07-06', 'Davao City', 'No Selection', '', '', '', '', 'wala lagi', '000-000-000', ' Single', '', 'dfs', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'Yes', '15', 'head', 'dssd', 'RV3 8-A', 'Emma Castillano', 'jomari@gmail.com', 'password', 'password', 'active', 'user.png', '', NULL),
(16, '19-0001', '0000-1', 'o', 's', 'd', 'Jr.', 'd', '', '', 'Male', '49', '3', 3, 'No Selection', 'Davao City', 'j', '12 LOWER LUBOGAN', 2, 1, 2, 'Philippines', '', '', '', '', '', '', '', '', '', '', ' Single', '', 'er', 'wer', 'wer', 'wer', 'wer', 'we', 'No', '9', 'Yes', '', '2', 'Jessa Mutia', 'sample@gmail.com', 'password', 'password', 'active', 'user.png', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resident_responded`
--

DROP TABLE IF EXISTS `resident_responded`;
CREATE TABLE IF NOT EXISTS `resident_responded` (
  `respond_id` int(11) NOT NULL AUTO_INCREMENT,
  `sked_id` varchar(5) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `rest_id` varchar(5) NOT NULL,
  `res_name` varchar(50) NOT NULL,
  `garbage` varchar(5) NOT NULL,
  `reason` text,
  `week` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `days` varchar(20) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `img` text NOT NULL,
  `lat` text NOT NULL,
  `lng` text NOT NULL,
  `garbage_type` varchar(20) NOT NULL,
  PRIMARY KEY (`respond_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resident_responded`
--

INSERT INTO `resident_responded` (`respond_id`, `sked_id`, `ecoboy_id`, `rest_id`, `res_name`, `garbage`, `reason`, `week`, `date`, `days`, `date_created`, `img`, `lat`, `lng`, `garbage_type`) VALUES
(30, '8', '12', '134', 'ACT PBA', 'Yes', NULL, 'week1', '2019-05-22', 'wednesday', '2019-08-18 07:39:28', 'user.png', '7.0264475', '125.499279', ''),
(39, '8', '12', '13', 'ACT PBA', 'Yes', NULL, 'week1', '2019-05-22', 'wednesday', '2019-09-12 14:08:52', 'user.png', '7.0264475', '125.499279', 'sw');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `Sched_No` int(11) NOT NULL AUTO_INCREMENT,
  `CollDate` date NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Purok_No` varchar(10) NOT NULL,
  `EcoboyName` varchar(100) NOT NULL,
  `ecoboy_id` varchar(5) NOT NULL,
  `status` varchar(10) NOT NULL,
  `reason` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `week_no` varchar(10) NOT NULL,
  `days` varchar(20) NOT NULL,
  `plate_number` varchar(20) DEFAULT NULL,
  `driver` varchar(50) DEFAULT NULL,
  `weight_net_kgs` varchar(20) DEFAULT NULL,
  `accomplish` varchar(5) DEFAULT NULL,
  `loading_cenro` varchar(20) DEFAULT NULL,
  `loading_date_ecoboy` varchar(20) DEFAULT NULL,
  `loading_date_cenro` varchar(20) DEFAULT NULL,
  `loading_ecoboy` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Sched_No`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`Sched_No`, `CollDate`, `StartTime`, `EndTime`, `Purok_No`, `EcoboyName`, `ecoboy_id`, `status`, `reason`, `date_created`, `week_no`, `days`, `plate_number`, `driver`, `weight_net_kgs`, `accomplish`, `loading_cenro`, `loading_date_ecoboy`, `loading_date_cenro`, `loading_ecoboy`) VALUES
(8, '2019-05-22', '02:34:00', '15:54:00', 'RV3 7-B', 'good Jv', '12', 'Approve', 'Approved', '2019-09-22 05:09:10', 'week1', 'wednesday', 'P#38607', 'cabanog', '1550', 'ok', NULL, NULL, NULL, NULL),
(9, '2019-09-25', '14:01:00', '14:00:00', '6', 'good Jv', '12', 'Approve', 'Approved', '2019-10-02 16:00:37', 'week1', 'wednesday', 'P#38607', 'Lambanog', '4500', NULL, NULL, '02-10-2019', NULL, 'load'),
(10, '2019-10-26', '01:00:00', '17:00:00', '4', 'good Jv', '12', 'inactive', 'Not yet review', '2019-10-02 15:05:28', 'week4', 'saturday', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
