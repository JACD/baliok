<?php include '../../include/mainincludetop.php';?>


<!-- Page Heading -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <center>
            <h6 class="m-0 font-weight-bold text-success">Loading and Unloading</h6>
        </center>
    </div>
    <div class="card-body">
        <!-- DREA E SULOD ANG CONTENT -->

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Loading Date</th>
                                <th>Unloading Date</th>
                                <th>Plate number/Driver</th>
                                <th>Weight/net/kgs</th>
                                <th>Total Solid waste KGS</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Loading date</th>
                                <th>Unloading date</th>
                                <th>Plate number/Driver</th>
                                <th>Weight/net/kgs</th>
                                <th>Total Solid waste KGS</th>
                                <th>Action</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <?php 
                   
                    
                    $result = mysqli_query($db,"SELECT * FROM schedule WHERE loading_ecoboy='load';");
                             
                    while($row = mysqli_fetch_array($result))
                    {   
						$lde = $row['loading_date_ecoboy'];
						$ldc = $row['loading_date_cenro'];
						$lc = $row['loading_cenro'];
						$llf = $row['loading_landfill'];
						$skidnum = $row['Sched_No'];
						$cur_price = $row['current_price'];
						$timbang = $row['weight_net_kgs'];
						$total = $timbang * $cur_price;
                      ?>
                                <td>
                                    <?php if($lde===null){  } else { echo date("F j, Y, g:i a",strtotime($lde)); } ?>
                                </td>
                                <td>
                                    <?php if($ldc===null){  } else { echo date("F j, Y, g:i a",strtotime($ldc)); } ?>
                                </td>
                                <td>
                                    <?php echo $row['plate_number'];?>
                                </td>
                                <td>
                                    <?php echo number_format($timbang).' X P'.$cur_price;?>
                                </td>
                                <td>
                                    <?php echo 'P'.number_format($total,2);?>
                                </td>
                                <td>
                                    <?php if($lc===null){?>
                                    <a href="../../php_function/function_cenro.php?cenro_id=<?php echo $skidnum;?>&clickbtn=trueload"
                                        onclick="return confirm('Do you want to load the garbage? There`s no undo!');"
                                        class="btn btn-success">
                                        Load
                                    </a>
                                    <?php }else if($lc=='load' && $llf==''){?>
                                    <a href="../../php_function/function_cenro.php?cenro_id=<?php echo $skidnum;?>&clickunbtn=trueunload"
                                        onclick="return confirm('Do you want to unload the garbage? There`s no undo!');"
                                        class="btn btn-danger">
                                        Unload
                                    </a>
                                    <?php }else if($lc=='load' && $llf=='load'){ echo '<span style="color:#1cc88a;">Done</span>'; } ?>

                                </td>
                            </tr>

                            <?php
                    }
                    ?>
                        </tbody>
                    </table>
                </div>
            </div>


            <!-- Donut Chart -->

        </div>
    </div>
</div>

<!-- DREA LANG TAMAN E SULOD ANG CONTENT -->



<?php include '../../include/mainincludebottom.php';?>